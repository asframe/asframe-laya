/**
 * @ASF.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2013-8-22
 */
declare namespace asf {
    /**
     * ASFrame框架的共享对象(ASFrameSession)，一些需要跨框架使用的实例，一般存放在这里。
     * 需要对框架的一些默认实现时，可以覆盖这里的接口对象。
     * 一般可以通过动态设置这里属性，达到调节框架性能的目的。
     * @author sodaChen
     * #Date:2013-8-22
     */
    class ASF {
        /**
         * 资源解析的时间，指的是在一帧的时间里，可以用多少时间来进行解析资源（默认是15毫秒）
         * 注意，解析时间消耗多，则会影响其他程序运行，有可能会掉帧。时间少，解析又慢。
         * 可以根据实际情况来动态调节这个值。
         **/
        static resParseTime: number;
        /**
         * 共享池的引用完毕之后的延迟销毁时间。默认是30秒
         * @see com.asframe.share.SharingPool
         */
        static sharingPoolDelay: number;
        /** 是否启动共享缓存策略（一般用于工具开发） **/
        static isResSharing: Boolean;
        /** 是否使用内存缓存二进制字节 **/
        static isCacheBytes: Boolean;
        /**时钟管理器，老的计时器，性能，可以抛弃了*/
        static timer: TimeMgr;
        static init(): void;
        /**
         * 获得当前的时间毫秒数
         */
        static currentTimeMillis(): number;
    }
}
declare namespace asf {
    class App {
        /**时钟管理器，老的计时器，性能，可以抛弃了*/
        static timer: TimeMgr;
        constructor();
        static init(): void;
    }
}
/**
 * @asframe.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2013-6-20
 */
declare namespace asf {
    /**
     * 获得当前的时间毫秒数
     */
    function currentTimeMillis(): number;
    function addTick(tick: Function | ITick): void;
}
declare namespace asf {
    /***
     * Action的相关常量
     */
    class ActionConst {
        /** 队列动作代理动作 */
        static QUEUE: string;
        /** 普通正常类型,可以多个动作同时存在 **/
        static SAME: string;
        /** 普通正常类型,可以多个动作同时存在 **/
        static REPEAT: string;
        /** 该类型动作只允许队列里只存在一个动作 **/
        static ALONE: string;
        /** 表演者注入 **/
        static INJECT_ACTOR: string;
        /** 参数注入 **/
        static INJECT_PARAM: string;
        /** 方法注入 **/
        static INJECT_SET_PARAMS: string;
        constructor();
    }
}
declare namespace asf {
    /***
     * 表演代理者，支持aciton的队列、并发、同名排斥模式
     */
    class ActorProxies {
        /** 表演者 **/
        private _actor;
        /** 动作集合 **/
        private _actionMap;
        /** 存放并发的id引用,使用Dictionary太消耗性能了，必须优化.改用hashcode实现机制 **/
        private _sameIdMap;
        /** 并发id增长值 **/
        private _sameIdCount;
        private _thisObj;
        constructor(actor: IActor);
        setActor(actor: IActor): void;
        act(action: IAction<any>, callBack?: Function, thisObj?: any, param?: any): boolean;
        delAction(action: IAction<any>, isDestory?: boolean): void;
    }
}
declare namespace asf {
    /***
     * 表演代理者，支持aciton的队列、并发、同名排斥模式
     */
    class BasicAction<T extends IActor> implements IAction<T> {
        static NAME: string;
        /** 附加的额外数据 **/
        extra: any;
        /** 动作类型 **/
        protected mType: string;
        /** 动作名称 **/
        protected mName: string;
        /**  **/
        protected mThisObj: any;
        protected mActor: T;
        /** 默认结束的时候调用的回调函数 **/
        protected mCallBack: Function;
        protected mParam: any;
        /** 是否已经启动了（防止重复调用start方法） **/
        protected mIsStart: boolean;
        /** 是否已经释放的标记 **/
        protected isDestory: boolean;
        constructor();
        /**
         * 初始化方法
         * @param callBack 销毁前的回调函数
         * @param param 参数
         *
         */
        initAction(callBack: Function, thisObj: any, param?: any): void;
        setName(name: string): void;
        setType(type: string): void;
        getType(): string;
        getName(): string;
        /**
         * 开始执行具体的动作逻辑，BasicAction做了一些基础处理，一般不建议子类重写。如果重写了，请调用super.start();
         *
         * @param actor 动作的服务对象(表演者)
         */
        start(actor: T): void;
        /**
         * 子类必须重写该方法
         */
        protected onStart(): void;
        destroy(o?: any): void;
        protected onDestory(): void;
    }
}
declare namespace asf {
    /**
     * 动作接口，在应用所有关于动作的都是该接口的实现类
     *
     * @author soda.chen
     *
     */
    interface IAction<T extends IActor> extends IDestroy {
        /**
         * 初始化方法
         * @param callBack 销毁前的回调函数
         * @param param 参数
         *
         */
        initAction(callBack: Function, thisObj: any, param?: any): void;
        /**
         * 开始执行该动作,绑定动作执行者，每个动作只能绑定一个动作执行者
         *
         * @param actor:动作执行者(IActor接口实现类)
         */
        start(actor: T): void;
        /**
         * 返回动作基本类型
         * @see ActionConstants
         */
        getType(): string;
        /**
         * @return 返回动作名称
         * @see ActionConstants
         */
        getName(): string;
    }
}
declare namespace asf {
    /**
     * 表演者，专门执行动作的接口，所有需要执行的对象，都必须实现中换个统一的接口
     * @author soda.chen
     *
     */
    interface IActor {
        /**
         * 执行动作action，返回是否成功启动动作。执行动作action，返回是否成功启动动作。
         * @param action 需要执行的动作实例
         * @param callBack acton被销毁前的回调函数
         * @param param action需要使用到的相关参数
         * @return
         *
         */
        act(action: IAction<any>, callBack: Function, thisObj: any, param?: any): boolean;
        /**
         * 删除一个动作
         * @param action 为动作名称或者IAction实例
         * @param isDestory 是否销毁action 默认是不销毁，只是从容器中删除
         */
        delAction(action: IAction<any>, isDestroy?: boolean): void;
    }
}
declare namespace asf {
    /**
     * 队列动作代理器，代理所有类型为ActionConstants.QUEUE类型的动作,进行队列播放<br>
     * 当代理器里的所有动作都播放完时，则该代理器从动作管理器中删除
     * 队列机制一般不建议使用，不太好掌控，因为是个action列表，很容易导致列表阻塞，缓存过多
     * @author soda.C
     */
    class QueueActionProxy<T extends IActor> extends BasicAction<T> {
        /** 队列里的action **/
        private actions;
        /** 当前执行的动作 **/
        private currAction;
        constructor();
        destroy(o?: any): void;
        protected onStart(): void;
        /**
         * 结束一个action，同时会调用action的overAction方法
         * @param action
         *
         */
        private overAction;
        /**
         * 添加一个新的action
         * @param action
         *
         */
        addAction(action: IAction<T>): void;
        /**
         * 选择一个新的action来处理
         */
        actAction(): void;
    }
}
declare namespace asf {
    /***
     * 基于帧的心跳机制的动作对象，具备每帧跳动的
     */
    class TickAction<T extends IActor> extends BasicAction<T> {
        private timerId;
        constructor();
        start(actor: T): void;
        destroy(o?: any): void;
        tick(): boolean;
    }
}
/**
 * @RgbColor.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame Color
 * <br>Date:2012-9-6
 */
declare namespace asf {
    /**
     *
     * @author sodaChen
     * Date:2012-9-6
     */
    class RgbColor {
        /**
         * 红色
         */
        red: number;
        /**
         * 绿色
         */
        green: number;
        /**
         * 蓝色
         */
        blue: number;
        /**
         * 构造
         */
        constructor(red?: number, green?: number, blue?: number);
        /**
         * 获取3通道16进制值
         * @return
         */
        getHex24(): number;
        clone(): RgbColor;
        /**
         * @return
         */
        toString(): String;
    }
}
/**
 * @RgbaColor.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame Color
 * <br>Date:2012-9-6
 */
declare namespace asf {
    /**
     *
     * @author sodaChen
     * Date:2012-9-6
     */
    class RgbaColor extends RgbColor {
        /** 透明通道 **/
        alpha: number;
        constructor(red?: number, green?: number, blue?: number, alpha?: number);
        /**
         * 获取4通道16进制值
         * @return
         */
        getHex32(): number;
        /**
         * 应用一个alpha值 , 并返回一个32位通道值
         * @param	a
         * @return
         */
        applyAlpha(alpha: number): number;
        /**
         * 克隆
         * @return
         */
        clone(): RgbaColor;
        /**
         * 输出文本
         * @return
         */
        toString(): String;
    }
}
/**
 * @CallBack.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/6
 */
declare namespace asf {
    /**
     * 回调函数对象，主要是增加了对this对象和参数的引用
     * @author sodaChen
     */
    class CallBack implements IPassivate {
        /** 池对象 **/
        static pool: Pool<CallBack>;
        /** 是否自动释放回池了 **/
        autoRelease: boolean;
        /**
         * 执行完是否自动清除自己的相关引用，必须是自动释放为false才行
         */
        autoClean: boolean;
        /**
         * 是否从池里来的
         */
        inPool: boolean;
        /** 回调函数 **/
        fun: Function;
        /** 执行回调函数时需要返回的参数 **/
        param: any;
        /** 回调函数需要的执行环境对象 **/
        thisObj: Object;
        /**
         * 通过对象池获得或者创建CallBack对象
         * @param fun 函数
         * @param thisObj this对象
         * @param param 可选参数z
         * @param autoRelease 是否执行完execute就主动放回对象池
         * @returns {CallBack} 实例
         */
        static create(fun: Function, thisObj: Object, param?: any, autoRelease?: boolean): CallBack;
        /**
         * 释放对象
         * @param callBack
         */
        static release(callBack: CallBack): void;
        /**
         *
         * @param fun 回调函数
         * @param thisObj 调函数需要的执行环境对象
         * @param param 执行回调函数时需要返回的参数。如果有参数。会是回调函数的第一个参数
         */
        constructor(fun?: Function, thisObj?: Object, param?: any);
        /**
         * 初始化，如果放入了对象池，可以使用这个方法来重新初始化
         * @param fun 回调函数
         * @param thisObj 调函数需要的执行环境对象
         * @param param 执行回调函数时需要返回的参数
         */
        init(fun: Function, thisObj: Object, param?: any): void;
        /**
         * 执行回调函数
         * @param args 不定参数，这里输入的参数必须和回调函数保持相同的顺序
         */
        execute(...args: any[]): any;
        passivate(): void;
        destroy(): void;
    }
}
/**
 * @ICallBack.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/7
 */
declare namespace asf {
    /**
     * 回调函数接口对象，主要是为了不用再额外绑定对象
     */
    interface ICallBack {
        /**
         * 执行回调函数接口
         * @param args 任意参数，根据需要指定
         */
        execute(...args: any[]): void;
    }
}
/**
 * @ASFrame.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012-present ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2012-1-7
 */
declare namespace asf {
    /**
     * @private
     * 哈希计数，全局使用，外部不可更改
     */
    let $hashCount: number;
    /**
     * ASFrame框架的顶级对象。框架内所有对象的基类，为对象实例提供唯一的hashCode值。
     */
    class ASFObject implements IHashObject, ICloneable {
        /**
         * 返回此对象唯一的哈希值,用于唯一确定一个对象。hashCode为大于等于1的整数。
         */
        hashCode: number;
        /**
         * 创建一个 HashObject 对象
         */
        constructor();
        clone(): any;
    }
}
/**
 * @Assert.as
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2012-1-7
 */
declare namespace asf {
    /**
     * 断言对象
     * @author sodaChen
     * Date:2012-1-7
     */
    class Assert {
        /**
         * 断言值是为<code>true</code>.
         * <pre class="code">Assert.isTrue(value, "该表达式必须是为真");</pre>
         * @param expression 一个布尔值表达式
         * @param message 当表达式不对时给出的提示信息
         * @param isThrowError 是否抛出异常，默认为false。传true时抛出异常
         */
        static isTrue(expression: boolean, message?: string): void;
        /**
         * 断言该对象不为 <code>null</code>.
         * <pre class="code">Assert.notNull(value, "参数不能为空");</pre>
         * @param object 检测对象
         * @param message 错误时的提示信息
         * @param isThrowError 是否抛出异常，默认为false。传true时抛出异常
         * @throws com.asframe.lang.error.IllegalArgumentError 如果表达式的值为<code>null</code>
         */
        static notNull(object: Object, message?: string): void;
        /**
         * 断言该对象是属于type类
         * <pre class="code">Assert.isInstanceOf(value, type, "vlaue必须type类或者他的子类");</pre>
         * @param object 检测对象
         * @param message 错误时的提示信息
         * @param isThrowError 是否抛出异常，默认为false。传true时抛出异常
         * @throws com.asframe.lang.error.IllegalArgumentError 如果value不属于type时抛出
         */
        static isInstanceOf(object: any, type: any, message?: string): void;
        /**
         * 检测对象是否为抽象类，抽象是不能进行实例化的
         * @param object
         * @param clazz
         *
         */
        static isAbstractClass(object: Object, clazz: string): void;
        /**
         * 是否索引越界了，小于等0或者i大于等于len
         * @param i
         * @param len
         * @param message
         *
         */
        static isIndexOutOfBounds(i: number, len: number, message?: string): void;
        private static isThrowIllegalArgumentError;
    }
}
/**
 * @Global.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2016-5-29
 */
declare namespace asf {
    /**
     * 全局对象，用于存放全局使用的东西，主要是提供一些公共的数据，以及做防止垃圾回收的措施
     * 游戏中最好不要直接在这里增加内容，这里维护的是属于框架通用的属性
     * @author sodaChen
     * Date:2016-5-29
     */
    class Global {
        /** 全局主题订阅和派发 **/
        static subject: Subject;
        /** 用于存放引用的公共集合 **/
        static refMap: HashMap<any, any>;
        /** 通用公共存放数据的集合 **/
        static dataMap: HashMap<any, any>;
        static addRef(target: Object): void;
        static removeRef(target: Object): void;
        static hasValue(key: any): boolean;
        static addValue(key: any, value: any): void;
        static getValue(key: any): any;
        static removeValue(key: any): any;
        static init(): void;
        /**
         * 创建一个object的any属性对象
         */
        static createAny(): any;
    }
}
/**
 * @IASFObject.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/19
 */
declare namespace asf {
    /**
     * 顶级对象。框架内所有对象的基类，为对象实例提供唯一的hashCode值。
     */
    interface IHashObject {
        /**
         * 返回此对象唯一的哈希值,用于唯一确定一个对象。hashCode为大于等于1的整数。
         * @readOnly
         */
        hashCode: number;
    }
    /**
     * 指示其他某个对象是否与此对象“相等”。
     * equals 方法在非空对象引用上实现相等关系：
     * 自反性：对于任何非空引用值 x，x.equals(x) 都应返回 true。
     * 对称性：对于任何非空引用值 x 和 y，当且仅当 y.equals(x) 返回 true 时，x.equals(y) 才应返回 true。
     * 传递性：对于任何非空引用值 x、y 和 z，如果 x.equals(y) 返回 true，并且 y.equals(z) 返回 true，那么 x.equals(z) 应返回 true。
     * 一致性：对于任何非空引用值 x 和 y，多次调用 x.equals(y) 始终返回 true 或始终返回 false，
     * 前提是对象上 equals 比较中所用的信息没有被修改。
     * 对于任何非空引用值 x，x.equals(null) 都应返回 false。
     * Object 类的 equals 方法实现对象上差别可能性最大的相等关系；即，对于任何非空引用值 x 和 y，当且仅当 x 和 y 引用同一个对象时，
     * 此方法才返回 true（x == y 具有值 true）。
     *（模仿java语言的equals功能）
     * @author sodaChen
     * Date:2011-5-4
     */
    interface IEquals {
        /**
         * 返回此对象唯一的哈希值,用于唯一确定一个对象。hashCode为大于等于1的整数。
         * @readOnly
         */
        equals(object: ASFObject): boolean;
    }
    /**
     * 复制一个新的对象
     */
    interface ICloneable {
        clone(): any;
    }
    interface IASFObject extends IHashObject, IEquals, ICloneable {
    }
}
declare namespace asf {
    interface IDestroy {
        /**
        * 释放对象相关资源
        * @param o:释放时需要的参数（不是必须的）
        *
        */
        destroy(o?: any): void;
    }
}
/**
 * Created by sodaChen on 2017/3/1.
 */
declare namespace asf {
    /**
     * 输出信息扩展类
     * @author sodaChen
     * Date:2017/2/15
     */
    class ConsolePlus {
        /** 调试级别，trace函数路径 **/
        static TRACE: number;
        /** 普通日志级别 **/
        static DEBUG: number;
        /** 普通日志级别 **/
        static LOG: number;
        /** 信息日志级别 console.info() **/
        static INFO: number;
        /** 警告日志级别 console.warn() **/
        static WARN: number;
        /** 错误日志级别 console.error() **/
        static ERROR: number;
        /** 默认是最初的日志级别，改变这个值。可以改变日志输出的结果 **/
        private static $level;
        /** 是否开启扩展模式 **/
        private static isPlusMode;
        /** 需要屏蔽覆盖的方法名 **/
        private static funNames;
        /** console最初的方法缓存 **/
        private static funList;
        /** 扩展后的方法级别 **/
        private static funPulsList;
        /** 空方法 **/
        private static emptyFun;
        static init(isPlusMode?: boolean): void;
        /**
         * 设置日志等级，所有大于等于这个日志级别的输出都可以正常输出，小于的则不能进行输出
         * @param level
         */
        static setLevel(level: number): void;
        /**
         * 设置输出模式
         * @param isPlusMode 是否为扩展模式
         */
        static setConsoleMode(isPlusMode: boolean): void;
        /**
         * 打开日志
         */
        static openConsole(): void;
        /**
         * 关闭所有的日志输出
         */
        static closeConsole(): void;
        /**
         * 开启日志输出的扩展模式
         */
        static openConsolePlus(): void;
        /**
         * 关闭日志输出的扩展模式
         */
        static closeConsolePlus(): void;
        /**
         * 创建扩展函数
         * @param oldFun 原有的函数
         * @param funName 函数名称
         */
        private static createPlusFun;
    }
}
/**
 * @Level.as.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-9-4
 */
declare namespace asf {
    /**
     * 日志级别对象
     * @author sodaChen
     * Date:2011-9-4
     */
    class Level {
        static TRACE: Level;
        static DEBUG: Level;
        static LOG: Level;
        static INFO: Level;
        static WARN: Level;
        static ERROR: Level;
        /** 级别数值 **/
        level: number;
        /** 级别名称 **/
        name: string;
        /** 日志显示的颜色 **/
        color: string;
        constructor(level: number, name: string, color: string);
        toString(): string;
    }
}
/**
 * @Logger.as.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-8-12
 */
declare namespace asf {
    /**
     * Log使用对象,最主要和最基础的使用对象，一般情况下都是使用这个来进行日志的输入和输出。
     * 带有默认的输出渠道，实际项目使用者。可以动态修改日志的输出格式
     * @author sodaChen
     * Date:2011-8-12
     */
    class Log {
        /** 是否开启日志系统 **/
        static isOpen: boolean;
        /** 默认是开启调试模式的 **/
        static level: Level;
        /**
         * 输出调试信息
         * @param target:日志输出的源头
         * @param arg0:输出参数
         * @param arg1:输出参数
         * @param arg2:输出参数
         *
         */
        static debug(...logs: any[]): void;
        /**
         * 输出提示信息
         * @param msg
         *
         */
        static info(...logs: any[]): void;
        /**
         * 输出提示信息
         * @param msg
         *
         */
        static log(...logs: any[]): void;
        /**
         * 输出警告的信息
         * @param target:日志输出的源头
         * @param arg0:输出参数
         * @param arg1:输出参数
         * @param arg2:输出参数
         *
         */
        static warn(...logs: any[]): void;
        /**
         * 输出错误的信息
         * @param	msg:需要输出信息的对象
         */
        static error(...logs: any[]): void;
        static formatLog(logLevel: Level, logs: any): void;
        /**
         * 设置日志等级
         * @param level:日志等级描述对象
         *
         */
        static setLevel(level: Level): void;
    }
}
/**
 * @Logger.as.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-8-12
 */
declare namespace asf {
    /**
     * Log使用对象,最主要和最基础的使用对象，一般情况下都是使用这个来进行日志的输入和输出。
     * 带有默认的输出渠道，实际项目使用者。可以动态修改日志的输出格式
     * @author sodaChen
     * Date:2011-8-12
     */
    class LogFactory {
        /** 是否开启日志系统 **/
        static isOpen: boolean;
        /** 默认是开启调试模式的 **/
        static level: Level;
        /**
         * 输出调试信息
         * @param target:日志输出的源头
         * @param arg0:输出参数
         * @param arg1:输出参数
         * @param arg2:输出参数
         *
         */
        static debug(...logs: any[]): void;
        /**
         * 输出提示信息
         * @param msg
         *
         */
        static info(...logs: any[]): void;
        /**
         * 输出提示信息
         * @param msg
         *
         */
        static log(...logs: any[]): void;
        /**
         * 输出警告的信息
         * @param target:日志输出的源头
         * @param arg0:输出参数
         * @param arg1:输出参数
         * @param arg2:输出参数
         *
         */
        static warn(...logs: any[]): void;
        /**
         * 输出错误的信息
         * @param	msg:需要输出信息的对象
         */
        static error(...logs: any[]): void;
        private static formatLog;
        /**
         * 设置日志等级
         * @param level:日志等级描述对象
         *
         */
        static setLevel(level: Level): void;
    }
}
/**
 * @Logger.as.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-8-12
 */
declare namespace asf {
    /**
     * Log使用对象,最主要和最基础的使用对象，一般情况下都是使用这个来进行日志的输入和输出。
     * 带有默认的输出渠道，实际项目使用者。可以动态修改日志的输出格式
     * @author sodaChen
     * Date:2011-8-12
     */
    class Logger {
        /** 日志头信息 **/
        logHead: string;
        constructor(path: string);
        /**
         * 输出调试信息
         * @param target:日志输出的源头
         * @param arg0:输出参数
         * @param arg1:输出参数
         * @param arg2:输出参数
         *
         */
        debug(...logs: any[]): void;
        /**
         * 输出提示信息
         * @param msg
         *
         */
        info(...logs: any[]): void;
        /**
         * 输出提示信息
         * @param msg
         *
         */
        log(...logs: any[]): void;
        /**
         * 输出警告的信息
         * @param target:日志输出的源头
         * @param arg0:输出参数
         * @param arg1:输出参数
         * @param arg2:输出参数
         *
         */
        warn(...logs: any[]): void;
        /**
         * 输出错误的信息
         * @param	msg:需要输出信息的对象
         */
        error(...logs: any[]): void;
        formatLog(logLevel: Level, logs: any[]): void;
        /**
         * 设置日志等级
         * @param level:日志等级描述对象
         *
         */
        setLevel(level: Level): void;
    }
}
/**
 * @Dictionary.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/19
 */
declare namespace asf {
    /**
     * 字典对象，支持key和value的绑定，支持任意对象作为key。内部使用了Array来实现
     * @author sodaChen
     * Date:2017/1/19
     */
    class Dictionary<K, V> extends ASFObject implements IMap<K, V> {
        /**
         * 所有的key组合
         */
        private $keys;
        /**
         * 所有的vaule组合
         */
        private $values;
        constructor();
        put(k: K, v: V): void;
        get(k: K): V;
        remove(k: K): V;
        indexOf(key: K): number;
        /**
         * 如果此映射未包含键-值映射关系，则返回 true.
         * @return 如果此映射未包含键-值映射关系，则返回 true.
         */
        isEmpty(): boolean;
        /**
         * 如果此映射包含指定键的映射关系，则返回 true.
         * @param key  测试在此映射中是否存在的键.
         * @return 如果此映射包含指定键的映射关系，则返回 true.
         */
        hasKey(key: K): boolean;
        /**
         * 如果该映射将一个或多个键映射到指定值，则返回 true.
         * @param key 测试在该映射中是否存在的值
         * @return 如果该映射将一个或多个键映射到指定值，则返回 true.
         */
        hasValue(value: V): boolean;
        size(): number;
        keys(copy?: boolean): Array<K>;
        values(copy?: boolean): Array<V>;
        forEach(fun: Function, thisObj: Object): void;
        forKeyValue(fun: Function, thisObj: Object): void;
        /**
         * @inheritDoc
         */
        getContainer(): Object;
        clear(): void;
        destroy(o?: any): void;
    }
}
/**
 * @HashMap.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012-present ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2012-1-7
 */
declare namespace asf {
    /**
     * 集合对象
     * @author sodaChen
     * Date:2012-1-7
     */
    class HashMap<K, V> extends ASFObject implements IMap<K, V> {
        /** 长度 **/
        private _length;
        private obj;
        constructor();
        /**
         * 获取源
         */
        getSource(): Object;
        /**
         * 将指定的值与此映射中的指定键相关联.
         * @param key 与指定值相关联的键.
         * @param value 与指定键相关联的值.
         */
        put(key: K, value: V): void;
        /**
         * 返回此映射中映射到指定键的值.
         * @param key 与指定值相关联的键.
         * @return 此映射中映射到指定值的键，如果此映射不包含该键的映射关系，则返回 null.
         */
        get(key: K): V;
        /**
         * 从此映射中移除所有映射关系
         */
        clear(): void;
        /**
         * 如果存在此键的映射关系，则将其从映射中移除
         * @param key 从映射中移除其映射关系的键
         * @return 以前与指定键相关联的值，如果没有该键的映射关系，则返回 null
         */
        remove(key: K): V;
        /**
         * 返回此映射中的键-值映射关系数.
         * @return 此映射中的键-值映射关系的个数.
         */
        size(): number;
        /**
         * 如果此映射未包含键-值映射关系，则返回 true.
         * @return 如果此映射未包含键-值映射关系，则返回 true.
         */
        isEmpty(): boolean;
        /**
         * 如果此映射包含指定键的映射关系，则返回 true.
         * @param key  测试在此映射中是否存在的键.
         * @return 如果此映射包含指定键的映射关系，则返回 true.
         */
        hasKey(key: K): boolean;
        /**
         * 如果该映射将一个或多个键映射到指定值，则返回 true.
         * @param key 测试在该映射中是否存在的值
         * @return 如果该映射将一个或多个键映射到指定值，则返回 true.
         */
        hasValue(value: V): boolean;
        /**
         * 返回此映射中包含的所有值.
         * @return 包含所有值的数组
         */
        values(): Array<V>;
        /**
         * 返回此映射中包含的所有key值.
         * @return 包含所有key的数组
         */
        keys<K>(): Array<K>;
        /**
         * @inheritDoc
         */
        getContainer(): Object;
        forEach(fun: Function, thisObj: Object): void;
        forKeyValue(fun: Function, thisObj: Object): void;
        destroy(o?: any): void;
    }
}
/**
 * @IMap.ts
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2012-1-4
 */
declare namespace asf {
    /**
     * 将键映射到值的对象。一个映射不能包含重复的键；每个键最多只能映射到一个值。
     * Map 接口提供三种collection 视图，允许以键集、值集或键-值映射关系集的形式查看某个映射的内容。
     * 映射顺序 定义为迭代器在映射的 collection 视图上返回其元素的顺序。
     * 某些映射实现可明确保证其顺序，如 TreeMap 类；另一些映射实现则不保证顺序，如 HashMap 类。
     * @author sodaChen
     * Date:2012-1-4
     */
    interface IMap<K, V> extends IDestroy {
        /**
         * 返回此映射中的键-值映射关系数.
         * @return 此映射中的键-值映射关系的个数.
         */
        size(): number;
        /**
         * 将指定的值与此映射中的指定键相关联.
         * @param key 与指定值相关联的键.
         * @param value 与指定键相关联的值.
         */
        put(key: K, value: V): void;
        /**
         * 返回此映射中映射到指定键的值.
         * @param key 与指定值相关联的键.
         * @return 此映射中映射到指定值的键，如果此映射不包含该键的映射关系，则返回 null.
         */
        get(key: K): V;
        /**
         * 从此映射中移除所有映射关系
         */
        clear(): void;
        /**
         * 如果存在此键的映射关系，则将其从映射中移除
         * @param key 从映射中移除其映射关系的键
         * @return 以前与指定键相关联的值，如果没有该键的映射关系，则返回 null
         */
        remove(key: K): V;
        /**
         * 如果此映射未包含键-值映射关系，则返回 true.
         * @return 如果此映射未包含键-值映射关系，则返回 true.
         */
        isEmpty(): boolean;
        /**
         * 如果此映射包含指定键的映射关系，则返回 true.
         * @param key  测试在此映射中是否存在的键.
         * @return 如果此映射包含指定键的映射关系，则返回 true.
         */
        hasKey(key: K): boolean;
        /**
         * 如果该映射将一个或多个键映射到指定值，则返回 true.
         * @param key 测试在该映射中是否存在的值
         * @return 如果该映射将一个或多个键映射到指定值，则返回 true.
         */
        hasValue(value: V): boolean;
        /**
         * 返回此映射中包含的所有值.
         * @return 包含所有值的数组
         */
        values(): Array<V>;
        /**
         * 返回此映射中包含的所有key值.
         * @return 包含所有key的数组
         */
        keys(): Array<K>;
        /**
         * 对每个值遍历一次
         * @param fun 接受参数是V
         */
        forEach(fun: Function, thisObj: Object): void;
        /**
         * 循环执行这个方法，每次传递是key和value
         * @param fun
         */
        forKeyValue(fun: Function, thisObj: Object): void;
        /**
         * 获取到IMap的对象容器，注意，该方法一般不要使用，也不要太多共同使用。
         * 除非是对性能要求非常高，而且需要确保不会直接修改该对象
         * @return
         *
         */
        getContainer(): Object;
    }
}
/**
 * @Singleton.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2010-2016 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-10-29
 */
declare namespace asf {
    /**
     * 所有单例的基类，做了单例的基础检查。所有子类最好都写一个getInstance的静态方法来获取
     * @author sodaChen
     * Date:2012-10-29
     */
    class Singleton implements IDestroy {
        /** 存放初始化过的构造函数 **/
        private static classMap;
        constructor();
        /**
         * 销毁方法。事实上单例是很少进行销毁的
         */
        destroy(o?: any): void;
        protected onDestroy(): void;
        /**
         * 删除单例的实例（不对单例本身做任何的销毁，只是删除他的引用）
         * @param clazz 单例的Class对象
         *
         */
        static removeInstance(clazz: any): void;
        /**
         * 获取单例类，若不存在则创建.所有的单例创建的时候，都必须使用这个方法来创建，这样可以做到统一管理单例
         * @param clazz	任意需要实现单例效果的类
         * @return
         *
         */
        static getInstanceOrCreate(clazz: any): any;
    }
}
/**
 * @IObserver.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-1-19
 */
declare namespace asf {
    /**
     * 观察者接口
     * @author sodaChen
     * Date:2012-1-19
     */
    interface IObserver {
        /**
         * 相应观察者
         * @param notice 触发此次相应的通知
         * @param args 相关参数数组
         *
         */
        update(args: Array<any>): void;
    }
}
/**
 * @INotifys.as
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:ASFrame
 * <br>Date:2016-9-19
 */
declare namespace asf {
    /**
     * 主题接口对象，设定主题的唯一字符串，然后添加相应的监听。
     * @author sodaChen
     * #Date:2016-9-19
     */
    interface ISubject {
        /**
         * 设置一个通知主题
         * @param notice
         */
        setNotice(notice: string): void;
        /**
         * 发起一个通知
         * @param param 参数是数字
         */
        notice(param?: Array<any>): void;
        /**
         * 发起一个通知
         * @param args 观察者接受的参数
         */
        send(...args: any[]): void;
        /**
         * 添加一个观察者通知
         * @param listener 通知监听函数
         *
         */
        on(listener: Function, thisObj: Object): void;
        /**
         * 添加一个观察者到指定通知的主题中
         * @param notcie 指定的通知
         * @param observer 观察者接口
         *
         */
        onObserver(observer: IObserver, isOnce: boolean): void;
        /**
         * 删除一个观察者通知
         * @param notice 通知名称
         * @param listener 删除指定监听函数
         *
         */
        off(listener: Function): void;
        /**
         * 根据指定通知删除一个观察者
         * @param notcie 指定的通知
         * @param observer 观察者接口
         *
         */
        offObserver(observer: IObserver): void;
        /**
         * 监听主题，只监听一次
         * @param notice
         * @param listener
         */
        once(listener: Function, thisObj: Object): void;
    }
}
/**
 * @NoticeData.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/23
 */
declare namespace asf {
    class NoticeData {
        /** 通知主题 **/
        notice: string | number;
        /** 监听函数 **/
        listener: Function;
        /** 执行域对象 **/
        thisObj: Object;
        /** 是否只执行一次就删除 **/
        isOnce: boolean;
        constructor(listener: Function, thisObj: Object, isOnce?: boolean);
    }
}
/**
 * @NoticeList.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2017/6/5
 */
declare namespace asf {
    /**
     * 一个主题的通知列表,主要是为了确保每个不同的通知可以互相独立出来，根据isNotice属性
     * @author sodaChen
     * Date:2017/6/5
     */
    class NoticeList {
        /** 通知主题 **/
        notice: string | number;
        /** 存放监听函数的集合 **/
        listeners: NoticeData[];
        onceList: NoticeData[];
        /** 是否正在发送通知 **/
        isNoticing: boolean;
        constructor();
    }
}
/**
 * @Subjects.as
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-7
 */
declare namespace asf {
    /**
     * 观察者主题，单一主题
     * @author sodaChen
     * #Date:2016-9-7
     */
    class Subject implements ISubject {
        /** 存放监听函数的集合 **/
        private listenerList;
        private noticeStr;
        constructor(notice?: string);
        private addNoticeData;
        private getObserverFun;
        /**
         * 设置一个通知主题
         * @param notice
         */
        setNotice(notice: string): void;
        notice(params: Array<Object>): void;
        /**
         * 发送一起通知
         * @param notice 通知
         * @param params 通知附带的参数
         *
         */
        send(...args: any[]): void;
        /**
         * 添加一个观察者通知
         * @param notice 通知名称
         * @param listener 通知监听函数
         *
         */
        on(listener: Function, thisObj: Object): void;
        onObserver(observer: IObserver, isOnce: boolean): void;
        /**
         * 删除一个观察者通知
         * @param notice 通知名称
         * @param listener 删除指定监听函数，为空则表示删除这个通知下的所有监听函数
         *
         */
        off(listener: Function): void;
        offObserver(observer: IObserver): void;
        once(listener: Function, thisObj: Object): void;
    }
}
/**
 * @INotifys.as
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:ASFrame
 * <br>Date:2016-9-19
 */
declare namespace asf {
    /**
     * 主题集合对象，发起通知器，可以代替AS3自带的事件派发器
     * @author sodaChen
     * #Date:2016-9-19
     */
    interface ISubjects {
        /**
         * 发起一个通知事件
         * @param notice 通知名称
         * @param param 参数数组
         */
        notice(notice: string | number, param?: Array<any>): void;
        /**
         * 发起一个通知
         * @param notice 通知
         * @param args 观察者接受的参数
         */
        send(notice: string | number, ...args: any[]): void;
        /**
         * 添加一个观察者通知
         * @param notice 通知名称
         * @param listener 通知监听函数
         *
         */
        on(notice: string | number, listener: Function, thisObj: Object): NoticeData;
        /**
         * 添加一个观察者到指定通知的主题中
         * @param notcie 指定的通知
         * @param observer 观察者接口
         * @param thisObj listener的类对象
         */
        onObserver(notice: string | number, observer: IObserver, isOnce: boolean): NoticeData;
        /**
         * 删除一个观察者通知
         * @param notice 通知名称
         * @param listener 删除指定监听函数
         *
         */
        off(notice: string | number, listener: Function, thisObj: Object): void;
        /**
         * 清除多个监听，不传thisObj时，表示清除所有的监听，否则只清除thisObj方法的on
         * @param thisObj
         */
        offs(thisObj?: Object): void;
        /**
         * 根据指定通知删除一个观察者
         * @param notice 指定的通知
         * @param observer 观察者接口
         *
         */
        offObserver(notice: string | number, observer: IObserver): void;
        /**
         * 监听主题，只监听一次
         * @param notice 通知
         * @param listener 监听函数
         * @param thisObj listener的类对象
         */
        once(notice: string | number, listener: Function, thisObj: Object): NoticeData;
    }
}
/**
 * @Subjects.as
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-7
 */
declare namespace asf {
    /**
     * 观察者主题，这里是集中多种主题
     * @author sodaChen
     * #Date:2016-9-7
     */
    class Subjects implements ISubjects {
        /** 存放监听函数的集合 **/
        private listenerMap;
        constructor();
        private addNoticeData;
        /**
         * 删除一个观察者通知
         * @param notice 通知名称
         * @param listener 删除指定监听函数，为空则表示删除这个通知下的所有监听函数
         *
         */
        off(notice: string | number, listener: Function, thisObj: Object): void;
        private getObserverFun;
        notice(notice: string | number, params?: any[]): void;
        /**
         * 发送一起通知
         * @param notice 通知
         * @param args 不定参数
         *
         */
        send(notice: string | number, ...args: any[]): void;
        /**
         * 添加一个观察者通知
         * @param notice 通知名称
         * @param listener 通知监听函数
         *
         */
        on(notice: string | number, listener: Function, thisObj: Object): NoticeData;
        onObserver(notice: string | number, observer: IObserver, isOnce: boolean): NoticeData;
        /**
         * 清除多个监听，不传thisObj时，表示清除所有的监听，否则只清除thisObj方法的on
         * @param thisObj
         */
        offs(thisObj?: Object): void;
        offObserver(notice: string | number, observer: IObserver): void;
        once(notice: string | number, listener: Function, thisObj: Object): NoticeData;
    }
}
/**
 * @Context.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2010-2016 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-5-25
 */
declare namespace mvc {
    /**
     * DoEasy控制中心，上下文，负责管理所有的Bean、Module和View、Helper
     * @author sodaChen
     * Date:2016-5-25
     */
    class Context {
        /** 拦截openView方法的实现类，通用的，所有的openView都会经过这个接口 **/
        static $holdupView: IHoldupView;
        /** 界面操作对象，由外部实际使用的时候扩展 **/
        static $viewHandler: IViewHandler;
        /** 外部的资源管理器的构造对象 **/
        static $resLoaderClass: any;
        /** 外部的模块管理器的构造对象，静态对象 **/
        static $moduleMgrClass: any;
        /** 外部的数据管理器的实例,静态对象 **/
        static $dbClass: any;
        /** mvc框架内通讯的全局主题对象 **/
        static subjects: asf.Subjects;
        /** 存放模块的配置信息 **/
        private static moduleBeanMap;
        /** 存放view的配置信息 **/
        private static viewBeanMap;
        /**
         * 初始化mvc框架的Context容器
         */
        static init(): void;
        /**
         * 初始化所有的模块
         */
        static initModule(): void;
        /**
         * 所有的模块拉取服务端的数据
         */
        static takeServer(): void;
        /**
         * 释放view的方法法的事件，内部函数
         * @param view
         */
        static $destroyViewEvent(view: IView<Object, any, any>): void;
        /**
         * 根据模块名称或者class构造函数获取到模块的实例
         * @param module 名称或者class构造函数
         * @returns {IModule} 模块的实例
         */
        static $getModule(module: string): IModule<any>;
        static $getViewBean(view: string): ViewBean;
        /**
         * 打开一个view界面。必须是通过注册的view才能使用这个方法。不然会抛出异常
         * @param view 唯一ID、名字和构造函数之一
         * @param callBack view完成打开之后回调
         * @param thisObj this对象，如果callBack
         * @param param 打开view时传入view的open方法的参数
         */
        static $openView(view: string, param: any): void;
        /**
         * 获取一个view
         *
         * @static
         * @param {(number | string | Function)} view
         * @param {*} [param]
         * @returns {ViewBean}
         *
         * @memberOf Context
         */
        static $getView(view: string): IView<Object, any, any>;
        /**
         * 关闭View对象
         * @param view view 唯一ID、名字和构造函数之一
         * @param isDestroy 是否销毁界面，默认是false
         */
        static $closeView(view: IView<Object, any, any>): void;
        /**
         * mvc框架内部添加一个view实例，并且显示出来(调用view的open方法)
         */
        static $addView(view: IView<any, any, any>): void;
        /**
         * 移除掉view实例，从显示列表中删除
         * @param view
         */
        static $removeView(view: IView<Object, any, any>, module?: IModule<any>): void;
        /**
         * 直接通过框架启动一个View，可以配置view以及相关helper
         * @param view
         * @param viewConfig
         * @param helpers
         */
        static startView(view: Function, viewConfig?: ViewConfig, ...controllers: any[]): void;
        /**
         * 启动整个框架，会负责初始化
         * @param complete 完成启动后的回调函数
         */
        static startup(): void;
    }
}
/**
 * Created by soda on 2017/1/26.
 */
declare namespace mvc {
    class MvcConst {
        /**
         * 基于mvc模块的view以及module以及model，controller必须有用的静态名字
         */
        static Class_Name: string;
        /**
         * 类的唯一ID
         */
        static Class_Id: string;
        /** 界面初始化完毕抛出的事件 **/
        static View_Complete: string;
        /** view打开事件，参数是view的name或者id **/
        static View_Open: string;
        /** view事关闭件，参数是view的name或者id **/
        static View_Close: string;
        /** view销毁事件，参数是view的name或者id **/
        static View_Destory: string;
        /** 打开view事件 **/
        static Open_View: string;
        /** 关闭view事件 **/
        static Close_View: string;
        /** 销毁view事件 **/
        static Destory_View: string;
        /**
         * 背景层
         * @type {string}
         */
        static Bg_Layer: string;
        /**
         * 场景层
         */
        static Scene_Layer: string;
        /**
         * 界面背景层
         */
        static View_Bg_Layer: string;
        /**
         * 主界面层
         */
        static Main_Layer: string;
        /**
         * 界面的底层层
         */
        static View_Bottom_Layer: string;
        /**
         * 界面层
         */
        static View_Layer: string;
        /**
         * 界面顶层
         */
        static View_Top_Layer: string;
        /**
         * 提示层
         */
        static Tip_Layer: string;
        /**
         * 最顶层
         */
        static Top_Layer: string;
        /** 不受排斥的界面级别 **/
        static ViewLv: number;
        /**
         * 目前设定的最大界面层级
         */
        static View_Lv_Max: number;
        /**
         * 主界面层，唯一的一层，天生只能存在一个
         */
        static View_Lv_Main: number;
        /**
         * 1层，一般是UI层
         */
        static View_Lv_1: number;
        /**
         * 一般是UI之上的2级UI层
         */
        static View_Lv_2: number;
        /**
         * 一般提示，警告层使用
         */
        static View_Lv_3: number;
    }
}
/**
 * Created by sodaChen on 2017/3/14.
 */
declare namespace mvc {
    /**
     * 注册mvc框架的类
     * @author soda.C
     * Date:2008-1-10
     */
    class MvcReg {
        /** 存放模块的配置信息 key:name && class **/
        private static moduleBeanMap;
        /** 存放view的配置信息 key:name && class **/
        private static viewBeanMap;
        static init(moduleBeanMap: asf.HashMap<string, ModuleBean>, viewBeanMap: asf.HashMap<string, ViewBean>): void;
        /**
         * 同时注册模块、View、Helper
         * @param module 模块构造函数
         * @param view view构造函数
         * @param config view的配置
         * @param helpers 多Helper的构造函数
         * @returns {ModuleConfig} 模块的配置类
         */
        static reg(module: Function, viewClass: Function, dbClass?: Function, ...controllers: any[]): void;
        /**
         * 删除注册的模块，同时会删除相关的view和heper
         * @param module
         * @param view
         */
        static del(module: Function): void;
        /**
         * 删除注册的view以及模块
         * @param view
         */
        static delView(view: Function): void;
        /**
         * 只注册Module和多个View的绑定关系
         * @param clazz 模块的构造函数
         * @param config 可选，模块的配置
         * @param name 可选，模块的名字
         */
        static regModule(clazz: Function, dbClass?: Function, ...views: any[]): ModuleBean;
        /**
         * 注册单个View的信息
         * @param clazz view的构造函数
         * @param config view的配置信息
         * @param module 可选，module的构造函数
         * @param viewName 可选，view的名字
         */
        static regView(clazz: Function, dbClass?: Function, module?: Function): ViewBean;
        /**
         * 注册跟指定view绑定的Controller对象
         * @param view
         * @param clazz
         * @param config
         */
        static regViewController(view: Function, ...controllers: any[]): ViewBean;
        static regController(view: Function, controllers: any[]): ViewBean;
    }
}
/**
 * @MvcUtils.ts
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/13
 */
declare namespace mvc {
    /**
     * mvc框架的工具方法集合,主要是处理view、controller、module公共的处理方法
     * @author sodaChen
     * Date:2017/2/13
     */
    class MvcUtils {
        /**
         * 获得框架指定的静态属性名字
         * @param clazz
         */
        static getClassName(clazz: Function): string;
        /**
         * 设置mvc核心实例的id和name
         * @param core view、controller、module之一的实例
         * @param bean 对应的bean结构
         */
        static setIdName(core: IBaseCore<any>, bean: BaseBean<any>): void;
        /**
         * 创建通用的核心对象
         * @param bean
         */
        static createCore(bean: BaseBean<IBaseCore<any>>): any;
        /**
         * 两个对象互相自动注入属性
         * @param core1
         * @param core2
         */
        static eachInject(core1: IBaseCore<any>, core2: IBaseCore<any>): void;
        /**
         * 根据实例创建对应的配置文件对象
         * @param core 核心对象
         * @param confClass 配置类的构造函数
         * @returns {any} 配置实例
         */
        static createConfig(core: IBaseCore<any>, confClass: any): any;
        static copyConfig(core: IBaseCore<any>, config: BaseConfig): void;
    }
}
/**
 * Created by sodaChen on 2017/3/14.
 */
declare namespace mvc {
    /**
     * 初始化mvc框架，传入必要的mvc内部使用到的对象
     * @param viewHandler
     * @param resLoaderClass
     * @param moduleMgrClass
     * @param dbClass
     * @param holdView
     */
    function init(viewHandler: IViewHandler, resLoaderClass: Function, moduleMgrClass?: Function, dbClass?: any, holdView?: IHoldupView): void;
    /**
     * 简化打开view的方法
     * @param {number | string | Function} view
     * @param param
     * @param {boolean} isEnforce
     */
    function open(view: string, param?: any): void;
    /**
     * 销毁界面
     * @param view 界面的标识
     */
    function destroy(view: string): void;
    /**
     * 关闭指定等级的view
     * @param lv
     */
    function closeByLv(lv?: number): void;
    /**
     * 关闭View对象
     * @param view view 唯一ID、名字和构造函数之一
     * @param isDestroy 是否销毁界面，默认是false
     */
    function close(view: string, isDestroy?: boolean): void;
    /**
     * 获得一个view
     *
     * @export
     * @param {(number | string | Function)} view
     * @param {*} [param]
     */
    function getView(view: string): IView<Object, any, any>;
    /** 判断当前曾经是否有view */
    function hasViewByLayer(layer: number): boolean;
    /**
    * 根据模块名称或者class构造函数获取到模块的实例
    * @param module 名称或者class构造函数
    * @returns {IModule} 模块的实例
    */
    function getModule(module: string): IModule<any>;
    /**
     * 发送一个mvc框架的全局通知
     * @param notice 通知名称
     * @param param 参数数组
     */
    function notice(notice: string | number, param?: any[]): void;
    /**
     * 发送一个mvc框架的全局通知
     * @param notice 通知主题
     * @param args 相关参数
     */
    function send(notice: string | number, ...args: any[]): void;
    /**
     * 添加一个观察者通知
     * @param notice 通知名称
     * @param listener 通知监听函数
     *
     */
    function on(notice: string | number, listener: Function, thisObj: Object): asf.NoticeData;
    /**
     * 监听主题，只监听一次
     * @param notice 通知
     * @param listener 监听函数
     * @param thisObj listener的类对象
     */
    function once(notice: string | number, listener: Function, thisObj: Object): asf.NoticeData;
    /**
     * 删除一个观察者通知
     * @param notice 通知名称
     * @param listener 删除指定监听函数
     *
     */
    function off(notice: string | number, listener: Function, thisObj: Object): void;
    /**
     * 移除对象的监听事件
     * @param thisObj
     */
    function offs(thisObj?: Object): void;
}
/**
 * @BaseBean.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-28
 */
declare namespace mvc {
    /**
     * 基础的存放结构
     * @author sodaChen
     * Date:2015-6-28
     */
    class BaseBean<T extends IBaseCore<any>> {
        instance: T;
        /** view的ID，由框架自动赋值(通过view、heper、module的子类的实例的构造函数的Id) **/
        id: number;
        /** view的名字，由框架自动赋值(通过view、heper、module的子类的实例的构造函数的Name)  **/
        name: string;
        /** 类的Function对象 **/
        clazz: any;
        /** 对应的实例对象(唯一的时候，这个值才有效) **/
        private _instance;
        constructor(clazz: any);
    }
}
/**
 * @BasicConfig.as
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-8
 */
declare namespace mvc {
    /**
     * 基础配置信息，包含view、helper以及moduleads
     */
    class BaseConfig {
        /** 唯一ID **/
        id: number;
        /** 是否延迟初始化 **/
        lazy: boolean;
        /** 是否一直保持实例化（即不被销毁） **/
        keep: boolean;
        /**
         *是否销毁
         */
        destroy: boolean;
        /** 资源列表 **/
        resList: ResBean[];
    }
}
/**
 * @MediatorBean.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:AStruts2
 * <br>Date:2012-4-9
 */
declare namespace mvc {
    /**
     * HelperBean的相关配置信息
     * @author sodaChen
     * Date:2012-4-9
     */
    class ControllerBean extends BaseBean<IController<any, any, any>> {
        constructor(clazz: Function);
    }
}
/**
 * Created by soda on 2017/1/27.
 */
declare namespace mvc {
    class ModuleBean extends BaseBean<IModule<any>> {
        /** 自身的数据模块实力 */
        selfDB: IModel;
        /** 数据对象 */
        dbClass: any;
        viewBeans: Array<ViewBean>;
        constructor(clazz: any);
    }
}
declare namespace mvc {
    class ResBean {
        /** loading时的提示信息 **/
        tip: string;
        /** 资源路径，可以通过"," 来输入多个资源路径(相对路径)**/
        path: string;
    }
}
/**
 * @(#)ViewBean.as
 * @author soda.C
 * @version  1.0
 * <br>Copyright (C), 2007 soda.C
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * @data 2008-1-10
 */
declare namespace mvc {
    /**
     * View的数据结构
     * @author soda.C
     * Date:2008-1-10
     */
    class ViewBean extends BaseBean<IView<Object, any, any>> {
        config: ViewConfig;
        /** 自身的数据模块实力 */
        selfDB: IModel;
        /** 数据对象 */
        dbClass: any;
        /** 正在加载资源中 **/
        isLoading: boolean;
        moduleBean: ModuleBean;
        controllerBeans: ControllerBean[];
        constructor(clazz: any, config: ViewConfig);
        /**
         * 打开view界面
         * @param param
         */
        openView(param: any): void;
    }
}
/**
 * @ViewBean.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-27
 */
declare namespace mvc {
    /**
     *
     * @author sodaChen
     * Date:2015-6-27
     */
    class ViewConfig extends BaseConfig {
        /**
         * 是否需要框架自动调用init方法(一般init方法都是加载资源之类的)
         * 不需要监听View的完成通知，直接调用view的init方法
         **/
        auto: boolean;
        /** 是否会自动在init之后，自动调用open方法 **/
        open: boolean;
        /** y坐标 **/
        x: number;
        /** x坐标 **/
        y: number;
        /** 复位设定，决定每次view被add进去（打开的）时候，是否读取配置文件里的配置的坐标(也就是还原刚开始的坐标) **/
        reset: boolean;
        /**  是否具有交换的属性，该属性可以让view有焦点时处于最顶层 **/
        swap: boolean;
        /**  是否支持tab键  **/
        tab: boolean;
        /** 是否拖动  **/
        drag: boolean;
        /** 始终在顶层的view  **/
        topView: boolean;
        /** 是否独占该组(如果独占，则同组的其他view都会被移除容器)  **/
        alone: boolean;
        constructor();
    }
}
/**
 * @BasicCore.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-12-13
 */
declare namespace mvc {
    /**
     *
     * @author sodaChen
     * Date:2016-12-13
     */
    class BaseCore<D> implements IBaseCore<D> {
        /** 额外附加数据 **/
        extra: any;
        /** 唯一ID **/
        id: number;
        /** 名称 **/
        name: string;
        /**
         * 项目中的自身模块的数据缓存对象
         */
        selfDB: D;
        /**
         * 获取到Core的配置数据，其中属性是和CoreConfig属性保持一直，
         * 当需要重置CoreConfig的对应属性时，则在这object里增加对应的属性
         * @return 包含相关属性的object对象
         *
         * @see com.asframe.doeasy.bean.ViewConfig,ModuleConfig,HelperConfig
         */
        config: any;
        /** 是否已经销毁了 **/
        isDestroy: boolean;
        /** 存放在mvc注册的通知事件，用于销毁时进行自动清除 **/
        private $mvcNotices;
        /**
         * 初始化方法
         */
        constructor();
        /**
         * 添加一个观察者通知。注意这里添加之后，调用销毁方法时会自动清除掉内部对他的引用。
         * 无法手动删除，使用请注意
         * @param notice 通知名称
         * @param listener 通知监听函数
         * @param thisObj 绑定的this对象
         */
        mvcOn(notice: string | number, listener: Function, thisObj: Object): void;
        /**
         * 清除一个mvc的时间，同时会清除该对象对这个时间的缓存
         * @param notice 通知名称
         * @param listener 通知监听函数
         * @param thisObj 绑定的this对象
         */
        mvcOff(notice: string | number, listener: Function, thisObj: Object): void;
        init(): void;
        destroy(o?: any): void;
        onDestroy(): void;
    }
}
/**
 * @BasicController.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2010-2016 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-5-24
 */
declare namespace mvc {
    /**
     * Helper的基础对象
     * @author sodaChen
     * Date:2016-5-24
     */
    class BaseController<V extends IView<any, D, any>, D extends IModel, M extends IModule<D>> extends BaseCore<D> implements IController<V, D, M> {
        /**
         * 设置控制的显示对象。框架底层自动调用，一般也是框架内部使用，外部推荐使用直接声明相关View的属性
         * @param view 实现了View接口的实例
         */
        view: V;
        constructor();
        viewOpen(): void;
        viewClose(): void;
    }
}
/**
 * @IController.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame AStruts2
 * <br>Date:2012-4-1
 */
declare namespace mvc {
    /**
     * 单个View的控制器,控制view的相关逻辑
     * @author sodaChen
     * Date:2012-4-1
     */
    interface IController<V extends IView<Object, D, any>, D extends IModel, M extends IModule<D>> extends IBaseCore<D> {
        /**
         * 设置控制的显示对象。框架底层自动调用，一般也是框架内部使用，外部推荐使用直接声明相关View的属性
         * @param view 实现了View接口的实例
         */
        view: V;
        /**
         * view每次打开时都会调用整个方法
         */
        viewOpen(): void;
        /**
         * view每次关闭或者销毁时都会调用整个方法
         */
        viewClose(): void;
    }
}
/**
 * @IResLoader.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame MVC
 * <br>Date:2017/4/11
 */
declare namespace mvc {
    /**
     * 跟容器接口，存放了游戏中各种基层容器对象
     * @author sodaChen
     * Date:2017/4/11
     */
    interface IRootContainer<T> extends asf.IDestroy {
        /** 背景层 **/
        bgLayer: T;
        /** 场景层 **/
        sceneLayer: T;
        /** bg界面最低层 **/
        viewBgLayer: T;
        /** 主UI层 **/
        mainLayer: T;
        /** 界面最低层 **/
        viewBottomLayer: T;
        /** 界面层 **/
        viewLayer: T;
        /** 界面层 **/
        viewTopLayer: T;
        /** 提示层 **/
        tipLayer: T;
        /** 最顶层 **/
        topLayer: T;
        /**
         * 初始化容器列表
         */
        initContainers(): any;
        /**
         * 获得根容器对象
         */
        getRootContainer(): T;
        /**
         * 根据层的名称返回相应的容器对象
         * @param layer
         * @returns {T容器对象}
         */
        getLayerContainer(layer: string): T;
        /**
         * 添加child到指定层中
         * @param layer
         * @param child
         */
        addChild(layer: string, child: T): void;
        /**
         * 从指定层中删除child
         * @param layer
         * @param child
         */
        removeChild(layer: string, child: T): void;
    }
}
/**
 * @IResLoader.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2017/4/11
 */
declare namespace mvc {
    /**
     * 基础的根容器对象，存放了游戏中各种基层容器对象，实现了基本的层级和通用操作
     * @author sodaChen
     * Date:2017/4/11
     */
    class BaseRootContainer<T> implements IRootContainer<T> {
        /** 背景层 **/
        bgLayer: T;
        /** 场景层 **/
        sceneLayer: T;
        /** bg界面最低层 **/
        viewBgLayer: T;
        /** 主UI层 **/
        mainLayer: T;
        /** 界面最低层 **/
        viewBottomLayer: T;
        /** 界面层 **/
        viewLayer: T;
        /** 界面层 **/
        viewTopLayer: T;
        /** 提示层 **/
        tipLayer: T;
        /** 最顶层 **/
        topLayer: T;
        protected readonly rootContainer: T;
        /**
         * 容器集合，用来控制容器的显示层次
         */
        protected containerMap: asf.HashMap<string, T>;
        constructor(rootContainer: T);
        /**
         * 获得根容器对象
         */
        getRootContainer(): T;
        /**
         * 初始化容器列表
         * 子类根据是情况进行重写
         */
        initContainers(): void;
        /**
         * 添加一个容器对象到集合里面
         * @param layer
         */
        private addContainer;
        /**
         * 根据层的名称返回相应的容器对象
         * @param layer
         * @returns {egret.Sprite}
         */
        getLayerContainer(layer: string): T;
        /**
         * 创建一个指定层名称的容器对象，子类根据不同的UI框架进行创建实现
         * @param layer
         */
        protected createContainer(): T;
        /**
         * 添加child到指定层中
         * @param layer
         * @param child
         */
        addChild(layer: string, child: T): void;
        /**
         * 从指定层中删除child
         * @param layer
         * @param child
         */
        removeChild(layer: string, child: T): void;
        destroy(o?: any): void;
    }
}
/**
 * @IBaseCore.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-12-13
 */
declare namespace mvc {
    /**
     * 框架的基础接口，mvc的接口和基类都是继承这个接口
     * @author sodaChen
     * #Date:2016-12-13
     */
    interface IBaseCore<D> extends asf.IDestroy {
        /** 唯一ID **/
        id: number;
        /** 唯一名字 **/
        name: string;
        /**
         * 项目中的自身模块的数据缓存对象
         */
        selfDB: D;
        /**
         * 获取到View的配置数据，其中属性是和ViewConfig属性保持一直，
         * 当需要重置ViewConfig的对应属性时，则在这object里增加对应的属性
         * @return 包含相关属性的object对象
         *
         * @see com.asframe.doeasy.bean.ViewConfig,ModuleConfig,HelperConfig
         */
        config: Object;
        /**
         * 是否已经释放资源了
         */
        isDestroy: boolean;
        /**
         * 添加一个观察者通知。注意这里添加之后，调用销毁方法时会自动清除掉内部对他的引用。
         * 无法手动删除，使用请注意
         * @param notice 通知名称
         * @param listener 通知监听函数
         * @param thisObj 绑定的this对象
         */
        mvcOn(notice: string | number, listener: Function, thisObj: Object): void;
        /**
         * 清除一个mvc的时间，同时会清除该对象对这个时间的缓存
         * @param notice 通知名称
         * @param listener 通知监听函数
         * @param thisObj 绑定的this对象
         */
        mvcOff(notice: string | number, listener: Function, thisObj: Object): void;
        /**
         * 初始化方法。建议在构造器里的数据都在这里初始化。
         * 如果配置有相应的资源，会加载完资源之后再调用该方法。
         */
        init(): void;
        /**
         * 调用释放方法destroy之后，会自动调用这个方法。所以子类需要在释放的时候进行处理时，重写这个发方法
         * 尽量不要重写destroy方法
         */
        onDestroy(): void;
    }
}
/**
 * @IResLoader.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2017/4/11
 */
declare namespace mvc {
    /**
     * mvc的资源加载器，如果不是使用mvc框架默认的资源加载器时，请重新实现这个接口
     * @author sodaChen
     * Date:2017/4/11
     */
    interface IResLoader extends asf.IDestroy {
        loadResList(resList: ResBean[], reser: IReser): void;
    }
}
/**
 * @IReser.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2017/4/11
 */
declare namespace mvc {
    /**
     * 资源接收器
     * @author sodaChen
     * Date:2017/4/11
     */
    interface IReser {
        /**
         * 用来接受资源加载器加载完成资源
         * @param values 实际的资源
         * @param resList 原来加载资源路径
         */
        setResList(values: any[], resList: mvc.ResBean[]): void;
    }
}
/**
 * @BaseBindingModel.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2021-10-01
 */
declare namespace mvc {
    /**
     * 基础绑定数据模型的基类
     * @author sodaChen
     * #Date: 2021-10-01
     */
    class BaseBindingModel implements IBindingModel {
        /**
         * 观察者监听函数的集合
         */
        private readonly observerMap;
        constructor();
        /**
         * 当前数据模型广播自己，所有监听了这个观察者的对象，都会收到对应的广播
         */
        notice(): void;
        /**
         * 添加一个观察者
         * @param callBack
         * @param that
         */
        addObserver(callBack: (data: any) => void, that: Object): void;
        /**
         * 删除掉一个观察者
         * @param callBack
         * @param that
         */
        removeObserver(callBack: (data: any) => void, that: Object): void;
        /**
         * 绑定数据变化和对应的回调函数
         * @param prop
         * @param callBack
         * @param that
         */
        binding(prop: string, callBack: (data: any) => void, that: Object): void;
        private bindingProp;
        init(): void;
    }
}
/**
 * @BindingData.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2021-10-01
 */
declare namespace mvc {
    /**
     * 绑定的数据结构
     * @author sodaChen
     * #Date: 2021-10-01
     */
    class BindingData {
        callBack: Function;
        that: Object;
        constructor(callBack: Function, that: Object);
    }
}
/**
 * @IBindingModel.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2021-10-01
 */
declare namespace mvc {
    /**
     * 进行数据绑定的模型接口，改对象的属性支持绑定对应的属性和回调函数
     * @author sodaChen
     * #Date: 2021-10-01
     */
    interface IBindingModel extends IModel {
        /**
         * 绑定一个属性和一个对应的回调函数
         * @param prop 绑定model的属性名字
         * @param callBack 产生变化后的回调函数
         * @param that 回调函数的作用域对象
         */
        binding(prop: string, callBack: (data: any) => void, that: Object): void;
    }
}
/**
 * Created by soda on 2017/1/25.
 */
declare namespace mvc {
    /**
     * 本地数据类，用来存放相应模块本地使用或者记录服务器数据的对象接口
     * @author sodaChen
     */
    interface IModel {
        init(): void;
    }
}
/**
 * @BasicModule.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-20
 */
declare namespace mvc {
    /**
     * 基础模块，大部分模块都应该继承这个基类，实现了一些基本和简单的功能
     * @author sodaChen
     * Date:2015-6-20
     */
    class BaseModule<D extends IModel> extends BaseCore<D> implements IModule<D> {
        isTakeServer: boolean;
        constructor();
        /**
         * 模块的启动方法，框架内部调用的方法。响应这个方法的时候，表示系统所有已经init的模块都准备好了
         * 可以直接调用其他模块
         * 子类需要出的话，在这里进行覆盖重写
         */
        start(): void;
        /**
         * 拉取服务器数据
         */
        takeServer(): void;
    }
}
/**
 * @InitModule.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-22
 */
declare namespace mvc {
    /**
     * 初始化模块对象
     * @author sodaChen
     * Date:2015-6-22
     */
    class CreateModule {
        initModule(moduleBean: ModuleBean): void;
    }
}
/**
 * @IModule.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-20
 */
declare namespace mvc {
    /**
     * 定义了模块的基础接口，所以的模块都必须实现该接口
     * 相当于一个大的控制器，可以控制模块下所有的view
     * @author sodaChen
     * #Date:2015-6-20
     */
    interface IModule<D extends IModel> extends IBaseCore<D> {
        /** 是否已经执行了 **/
        isTakeServer: boolean;
        /**
         * 拉取服务器数据
         */
        takeServer(): void;
        /**
         * 模块的启动方法，框架内部调用的方法。响应这个方法的时候，表示系统所有已经init的模块都准备好了
         * 可以直接调用其他模块
         */
        start(): void;
    }
}
/**
 * @BaseView.ts
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-20
 */
declare namespace mvc {
    /**
     * mvc框架的基础View对象
     * @author sodaChen
     * #Date:2016-9-20
     */
    class BaseView<C, D extends IModel, M extends IModule<D>> extends BaseCore<D> implements IView<C, D, M> {
        /**
         * 界面级别,默认是0，也就是不做任何处理
         */
        lv: number;
        /**
         * 界面是否打开
         */
        isOpen: boolean;
        /**
         * 属于这个view的模块对象
         */
        module: M;
        /** 容器显示对象 **/
        container: C;
        /**
         * 获取到View的配置数据，其中属性是和ViewConfig属性保持一直，
         * 当需要重置ViewConfig的对应属性时，则在这object里增加对应的属性
         * @return 包含相关属性的object对象
         *
         * @see com.asframe.doeasy.bean.ViewConfig,ModuleConfig,HelperConfig
         */
        config: ViewConfig;
        /** 容器层名字， 默认是View容器层**/
        layer: string;
        /** 自动释放，当关闭的时候就自动释放资源 **/
        protected isCloseDestroy: boolean;
        constructor();
        /**
         * 设置UI的资源，所在组的名字
         * @param group
         */
        setUIRes(group: string): void;
        /**
         * 初始化容器对象，使用方法的好处是可以在方法里进行其他扩展操作
         * @param container
         */
        initContainer(container: C): void;
        start(): void;
        onStart(): void;
        open(param?: any): void;
        onOpen(param?: any): void;
        close(): void;
        onClose(): void;
        destroy(o?: any): void;
    }
}
/**
 * @LayaViewHandler.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/7
 */
declare namespace mvc {
    /**
     * 通过View控制器，基础的View操作都在这里。框架管理所有的View
     * 通过这里可以查找到基于框架的所有View对象
     */
    class BaseViewHandler<T> implements IViewHandler {
        /** View的集合 **/
        protected readonly viewMap: asf.HashMap<string, ViewBean>;
        /** 界面等级列表的集合 **/
        protected readonly lvList: ViewBean[];
        /** 根容器对象 **/
        protected readonly rootContainer: IRootContainer<T>;
        /** 最大的界面等级 **/
        protected readonly lvMax: number;
        constructor(rootContainer: IRootContainer<T>);
        /**
         * 关闭指定等级的view
         * @param lv
         */
        closeViewByLv(lv: number): void;
        /**
         * 是否已经添加了view对象
         * @param view
         */
        hasView(view: ViewBean): boolean;
        /**
         * 添加一个View对象
         * @param view
         */
        addView(view: ViewBean): boolean;
        /**
         * 子类扩展view的实际添加
         * @param viewBean
         */
        protected onAddView(viewBean: ViewBean): void;
        /**
         * 删除一个View对象
         * @param view
         */
        delView(view: ViewBean): void;
        protected onDelView(viewBean: ViewBean): void;
        /**
         * 是否打开了指定级别的view
         * @param lv:number 界面级别
         * @return boolean 是否打开
         */
        hasViewByLv(lv: number): boolean;
        /**
         * 判断当前层级是否有显示View
         */
        hasViewByLayer(layer: any): boolean;
    }
}
/**
 * @InitView.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2010-2016 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-5-30
 */
declare namespace mvc {
    /**
     * 初始化界面的类
     * @author sodaChen
     * Date:2016-5-30
     */
    class CreateView implements IReser {
        viewBean: ViewBean;
        callBack: asf.CallBack;
        view: IView<Object, any, any>;
        /** 是否打开当前view **/
        isOpen: boolean;
        /** view打开的时候传递的参数 **/
        openParam: any;
        urlPaths: Array<String>;
        private resLoader;
        constructor(isOpen: boolean, viewBean: ViewBean, callBack?: asf.CallBack, openParam?: any);
        private hasInit;
        /**
         * 用来接受资源加载器加载完成资源
         * @param values 实际的资源
         * @param urls 原来加载资源路径
         */
        setResList(values: any[], resList: mvc.ResBean[]): void;
        onViewComplete(): void;
    }
}
/**
 * @IHoldupView.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2017/7/7
 */
declare namespace mvc {
    /**
     *
     * @author sodaChen
     * Date:2017/7/7
     */
    interface IHoldupView {
        init(): void;
        /**
         * hold住openView方法，true的时候则表示拦截住，false则表示可以通过openView方法
         * @param viewBean
         * @param param
         * @param callBack
         */
        openViewHold(viewBean: mvc.ViewBean, param: any): boolean;
    }
}
/**
 * @IView.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame AStruts2
 * <br>Date:2012-4-1
 */
declare namespace mvc {
    /**
     * 显示对象接口,View对象一般都是要实现该接口
     * @author sodaChen
     * Date:2012-4-1
     */
    interface IView<C, D extends IModel, M extends IModule<D>> extends IBaseCore<D> {
        /**
         * 界面级别
         */
        lv: number;
        /**
         * 界面是否打开
         */
        isOpen: boolean;
        /**
         * 返回需要添加到的容器层
         * @return
         *
         */
        layer: string;
        /**
         * 属于这个view的模块对象
         */
        module: M;
        /**UI */
        container: C;
        /**
         * 初始化容器对象，使用方法的好处是可以在方法里进行其他扩展操作
         * @param container
         */
        initContainer(container: C): void;
        /**
         * 框架自动调用方法。当view和他的helper等依赖关系建立好之后，才会自动调用，慢于init方法
         */
        start(): void;
        /**
         * 系统自动调用的私有方法。打开并显示view对象，一般是这个方法是系统调用的，所以要重写，请重写onOpen方法
         * @param param
         *
         */
        open(param?: any): void;
        /**
         * 需要对打开界面进行一些操作时，请重写这个方法
         * @param param
         */
        onOpen(param?: any): void;
        /**
         * 关闭view对象（不等于销毁，默认是不在显示列表或者隐藏起来了）
         * @param isDestroy 是否释放掉界面，默认为false
         */
        close(): void;
    }
}
/**
 * @IViewHandler.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/7
 */
declare namespace mvc {
    interface IViewHandler {
        /**
         * 关闭指定等级的view
         * @param lv 指定界面级别，-1时表示关闭当前所有界面
         */
        closeViewByLv(lv: number): void;
        /**
         * 是否已经添加了view对象
         * @param view
         */
        hasView(view: ViewBean): boolean;
        /**
         * 添加一个View对象
         * @param view
         * @return 添加成功则返回true，否则返回false
         */
        addView(view: ViewBean): boolean;
        /**
         * 删除一个View对象
         * @param view
         */
        delView(view: ViewBean): void;
        /**
         * 当前是否存在指定等级的view
         */
        hasViewByLv(lv: number): boolean;
        /**判断当前层级是否存在view */
        hasViewByLayer(layer: number): boolean;
    }
}
/**
 * @ObjectPool.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2014-6-15
 */
declare namespace asf {
    /**
     * 基于工厂的生成对象的对象池，主要是用于创建和休闲以及销毁时需要做额外处理的对象
     * 包具体工作外包给IFactoryObject接口，使用者只需要实现自己的IFactoryObject来处理即可。
     * @author sodaChen
     * Date:2014-6-15
     */
    class FactoryObjectPool<T> implements IPool<T> {
        protected objectFactory: IFactoryObject<T>;
        protected maxIdle: number;
        protected list: T[];
        constructor(objectFactory: IFactoryObject<T>, maxIdle?: number);
        create(): T;
        release(obj: T): void;
        getNumIdle(): number;
        clear(): void;
        destroy(o?: any): void;
        isFull(): boolean;
    }
}
/**
 * @IActivate.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2013-9-26
 */
declare namespace asf {
    /**
     *
     * @author sodaChen
     * #Date:2013-9-26
     */
    interface IActivate {
        /**
         * 激活(被借出的时候调用的方法) ，该方法是对象池内部调用，外面使用不需要调用该方法。
         * 以免引起不必要的bug
         */
        activate(): void;
    }
}
/**
 * @PoolableObjectFactory.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame Pool
 * <br>Date:2012-1-15
 */
declare namespace asf {
    /**
     * 接口定义的生命周期方法
     * 实例可以服务的链接ObjectPool。
     * @author sodaChen
     * @version 2013.03.31
     */
    interface IFactoryObject<T> {
        /**
         * 生成一个全新的对象
         * @return
         */
        create(): T;
        /**
         * 销毁池里不需要的对象
         * @param obj
         */
        destroy(obj: T): void;
        /**
         * 使用前重新激活对象
         * @param obj
         */
        activate(obj: T): void;
        /**
         * 对象返回池之前进行调整(使钝化，即进入休眠状态)
         * @param obj
         */
        passivate(obj: T): void;
    }
}
/**
 * @IPassivate.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2013-9-26
 */
declare namespace asf {
    /**
     *
     * @author sodaChen
     * #Date:2013-9-26
     */
    interface IPassivate {
        /**
         * 进入待机状态(就是被还回的时候调用的方法)
         */
        passivate(): void;
    }
}
/**
 * @IObjectPool.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Pool
 * <br>Date:2012-1-15
 */
declare namespace asf {
    /**
     * 对象池接口，该接口可以用来存放任意对象的对象池接口
     * @author sodaChen
     *
     */
    interface IPool<T> extends IDestroy {
        /**
         * 借出一个对象
         * @return
         *
         */
        create(): T;
        /**
         * 是否已满
         * @param obj
         *
         */
        isFull(): boolean;
        /**
         * 归还对象
         * @param obj
         *
         */
        release(obj: T): void;
        /**
         * 返回池里的可用对象
         * @return
         *
         */
        getNumIdle(): number;
        /**
         * 清除池里的所有对象，恢复成最初状态
         */
        clear(): void;
    }
}
/**
 * @ObjectPool.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:挂机冒险
 * <br>Date:2014-6-15
 */
declare namespace asf {
    /**
     * 普通对象池
     * @author sodaChen
     * Date:2014-6-15
     */
    class Pool<T> implements IPool<Object> {
        /** 用到池的对的class **/
        private $clazz;
        private $maxIdle;
        private $idles;
        private $hasStatus;
        /**
         * 创建一个通用对象池实例
         * @param clazz 使用对象的class
         * @param maxIdle 最大缓存数量
         * @param hasStatus 是否需要做状态检测（默认为false，如果没有状态，设置false会提升性能）
         *
         */
        constructor(clazz: any, maxIdle?: number, hasStatus?: boolean);
        create(): T;
        release(obj: T): void;
        getNumIdle(): number;
        clear(): void;
        isFull(): boolean;
        destroy(o?: any): void;
    }
}
/**
 * @PoolManager.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2013-6-20
 */
declare namespace asf {
    /**
     * 对象池管理器包括IObjectPool的对象，实现了统一管理。具体使用到了pool包里面的各种对象池的实例和接口
     * @author sodaChen
     * #Date:2013-6-20
     */
    class PoolMgr {
        /** 默认一个对象池的最大空闲对象是数量 **/
        static defaultMax: number;
        /** 对象池集合 **/
        private static $poolMap;
        static init(): void;
        /**
         * 获取到指定对象池对象
         * @param clazz 对象池缓存的对象Class
         * @return 返回指定的对象池对象
         *
         */
        static getPool<T>(clazz: any): IPool<T>;
        /**
         * 注册一个需要缓存池的Class对象，内部会自动生成通用的ObjectPool对象池
         * @param clazz 缓存对象class
         * @param maxIdle 最大的空闲数量
         * @param hasStatus 是否需要做激活和不激活的状态检测(false有助提升性能)
         *
         */
        static regClass(clazz: any, maxIdle?: number, hasStatus?: boolean): IPool<any>;
        /**
         * 注册一个对象池，由外部提供对象池
         * @param clazz 生产的对象class
         * @param objectPool 对象池实例
         */
        static regPool(clazz: any, objectPool: IPool<Object>): void;
        /**
         * 销毁指定池对象中的所有子节点
         */
        static clearPoolChildren(clazz: any): void;
        /**
         * 清除指定的对象池
         * @param clazz 对象class
         * @param isDestory 是否同时销毁。默认false
         */
        static clearPool(clazz: any, isDestory?: boolean): void;
        /**
         * 借出一个对象(借出的对象，对象池和管理器本身不会对它有任何引用，完全干净无引用的对象)
         * @param clazz
         * @return
         *
         */
        static create(clazz: any): any;
        /**
         * 归还对象。如果对象池满了，则会进行销毁。调用dispose方法或者IDestroy接口的方法
         * @param obj 实例
         * @param clazz 对象class.为空时则会自动从obj上获取到对应的class
         */
        static release(obj: Object, clazz?: any): void;
        /**
         * 是否已满
         * @param obj
         * @param clazz
         * @return
         *
         */
        static isFull(obj: Object, clazz?: any): Boolean;
    }
}
/**
 * @IAutoTick.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-5-4
 */
declare namespace asf {
    /**
     * 具备自动加入,自动结束，同时还具备了销毁的功能 的心跳对象
     * @author sodaChen
     * Date:2011-5-4
     */
    interface IAutoTick extends ITick, IDestroy {
        /**
         * 是否已经结束了.返回true时，执行器会删除掉该tick
         * @return
         */
        isFinish(): boolean;
    }
}
/**
 * @ITick.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-5-4
 */
declare namespace asf {
    /**
     * 心跳接口，以一定的频率执行tick方法.最基本，简单的心跳接口,提供纯粹的心跳功能。所以基于心跳的功能都实现了该接口
     * @author sodaChen
     * Date:2011-5-4
     */
    interface ITick {
        /**
         * 心跳
         */
        tick(): void;
    }
}
declare namespace asf {
    /**
     * 计时器管理器，游戏中所有的计时器，目前是采用时间，不支持帧计时器
     *
     * @author sodaChen
     * Date:2011-5-4
     */
    class TimeMgr {
        /** 离开焦点的时间，暂时写在这里的而已，实际是要写正心跳机制那里，每次执行time都会重置为0 **/
        lifecycleTime: number;
        /**
         * vo对象池
         */
        private _pool;
        /**
         * 唯一key和时间执行器对象绑定
         */
        private _keyMap;
        /**
         * 当前执行器数量
         */
        private _count;
        /**
         * 当前
         */
        private _maxIndex;
        /**待删除的计时器 */
        private _waitDelHandlerArr;
        /**
         * 是否停止
         */
        isStop: boolean;
        constructor();
        start(): void;
        /**
         * 停止所有timer心跳
         */
        stop(): void;
        clear(): void;
        onEnterFrame(timestamp: number): void;
        /**
         * 创建1个计时器
         */
        private create;
        /**
         * 增加计时器处理方法
         */
        private addHandler;
        /**定时执行一次
         * 用的时候一定要把上一个key给去掉
         * @param	delay  延迟时间(单位毫秒)
         * @param	method 结束时的回调方法
         * @param	args   回调参数
         * @param	lastKey   上一个重复方法实例key，如果没有则可以传0
         * @return 返回唯一ID，均用来作为clearTimer的参数*/
        doOnce(delay: number, method: Function, thisObj: any, lastKey?: number, args?: any[]): number;
        /**定时重复执行
         * 用的时候一定要把上一个key给去掉
         * @param	delay  延迟时间(单位毫秒)
         * @param	method 结束时的回调方法
         * @param	thisObj 方法的this对象
         * @param	args   回调参数
         * @param	lastKey   上一个重复方法实例key，如果没有则可以传0
         * @return 返回唯一ID，均用来作为clearTimer的参数*/
        doLoop(delay: number, method: Function, thisObj: any, lastKey?: number, args?: any[]): number;
        /**定时器执行数量*/
        readonly count: number;
        /**清理定时器
         * @param 唯一ID，均用来作为clearTimer的参数
         */
        clearTimer(key: number): void;
        /**删除待删除列表的计时器 */
        toClearTimer(): void;
        /**
         *  获取TimerVo
         */
        getHandler(key: number): TimerVo;
        /**
         *暂停定时器
         */
        pauseTimer(key: number): void;
        /**
         *恢复定时器
         */
        resumeTimer(key: number): void;
    }
    /**定时处理器*/
    class TimerVo {
        /**执行间隔*/
        delay: number;
        /**是否重复执行*/
        repeat: boolean;
        /**执行时间*/
        exeTime: number;
        /**处理方法*/
        method: Function;
        /**参数*/
        args: any[];
        /**是否暂停*/
        isPause: boolean;
        /**目标this*/
        thisObj: any;
        /**唯一ID */
        key: number;
        /**是否补帧操作 */
        isFillFrame: boolean;
        /**是否已经删除 */
        isDel: boolean;
        /**清理*/
        clear(): void;
    }
}
/**
 * @ArrayUtils.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/23
 */
declare namespace asf {
    class ArrayUtils {
        static removeItem(array: Array<any>, item: any): void;
        /**新随机数组*/
        static randomAry(arr: any[]): any[];
        /**
         * 拷贝一个字典
         */
        static copyDict(dict: any, isDeepCopy?: any): any;
    }
}
/**
 * @ClassUtils.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-1-14
 */
declare namespace asf {
    /**
     *
     * @author sodaChen
     * Date:2012-1-14
     */
    class ClassUtils {
        private static CONSTRUCTOR_NAME;
        /**
         * 获取到Class的名字（不包含包名）.这个方法要注意，因为打包成min.js的时候，会修改类名，所以到时无法获取真实的类名字了
         * @param clazz 构造函数实例
         * @param firstMin 名字的首字母是否小写。默认是不修改
         * @returns {string} 返回名字
         */
        static getClassName(clazz: Function, firstMin?: boolean): string;
        /**
         * 根据对象的实例返回<code>Class<code>对象
         * @param instance 需要返回class对象的实例
         * @return 实例的<code>构造函数</code>
         */
        static forInstance(instance: Object): any;
        /**
         * 根据名字获得对应的构造函数对象
         * @param name 构造函数名字
         * @return the 名字的<code>构造函数</code>
         */
        static forName(name: string): any;
        /**
         * Creates an instance of the given class and passes the arguments to
         * the constructor.
         *
         * TODO find a generic solution for this. Currently we support constructors
         * with a maximum of 10 arguments.
         *
         * @param clazz the class from which an instance will be created
         * @param args the arguments that need to be passed to the constructor
         */
        static newInstance(clazz: any, args?: any[]): any;
    }
}
declare namespace asf {
    /**
        Provides utility functions for dealing with color.
        @author Aaron Clinger
        @version 03/29/10
    */
    class ColorUtil {
        /**
            Converts a series of individual RGB(A) values to a 32-bit ARGB color value.
            
            @param r: A uint from 0 to 255 representing the red color value.
            @param g: A uint from 0 to 255 representing the green color value.
            @param b: A uint from 0 to 255 representing the blue color value.
            @param a: A uint from 0 to 255 representing the alpha value. Default is <code>255</code>.
            @return Returns a hexidecimal color as a String.
            @example
                <code>
                    var hexColor:string = ColorUtil.getHexStringFromARGB(128, 255, 0, 255);
                    trace(hexColor); // Traces 80FF00FF
                </code>
        */
        static getColor(r: number, g: number, b: number, a?: number): number;
        /**
            Converts a 32-bit ARGB color value into an ARGB object.
            
            @param color: The 32-bit ARGB color value.
            @return Returns an object with the properties a, r, g, and b defined.
            @example
                <code>
                    var myRGB:Object = ColorUtil.getARGB(0xCCFF00FF);
                    trace("Alpha = " + myRGB.a);
                    trace("Red = " + myRGB.r);
                    trace("Green = " + myRGB.g);
                    trace("Blue = " + myRGB.b);
                </code>
        */
        static getARGB(color: number): Object;
        /**
            Converts a 24-bit RGB color value into an RGB object.
            
            @param color: The 24-bit RGB color value.
            @return Returns an object with the properties r, g, and b defined.
            @example
                <code>
                    var myRGB:Object = ColorUtil.getRGB(0xFF00FF);
                    trace("Red = " + myRGB.r);
                    trace("Green = " + myRGB.g);
                    trace("Blue = " + myRGB.b);
                </code>
        */
        static getRGB(color: number): Object;
        /**
            Converts a 32-bit ARGB color value into a hexidecimal String representation.
            
            @param a: A uint from 0 to 255 representing the alpha value.
            @param r: A uint from 0 to 255 representing the red color value.
            @param g: A uint from 0 to 255 representing the green color value.
            @param b: A uint from 0 to 255 representing the blue color value.
            @return Returns a hexidecimal color as a String.
            @example
                <code>
                    var hexColor:string = ColorUtil.getHexStringFromARGB(128, 255, 0, 255);
                    trace(hexColor); // Traces 80FF00FF
                </code>
        */
        static getHexStringFromARGB(a: number, r: number, g: number, b: number): string;
        /**
            Converts an RGB color value into a hexidecimal String representation.
            
            @param r: A uint from 0 to 255 representing the red color value.
            @param g: A uint from 0 to 255 representing the green color value.
            @param b: A uint from 0 to 255 representing the blue color value.
            @return Returns a hexidecimal color as a String.
            @example
                <code>
                    var hexColor:string = ColorUtil.getHexStringFromRGB(255, 0, 255);
                    trace(hexColor); // Traces FF00FF
                </code>
        */
        static getHexStringFromRGB(r: number, g: number, b: number): string;
        /**
         * 输入一个颜色,将它拆成三个部分:
         * 红色,绿色和蓝色
         */
        static retrieveRGB(color: number): number[];
        /**
         * 红色,绿色和蓝色三色组合
         */
        static generateFromRGB(rgb: number[]): number;
        /**
         * color1是浅色,color2是深色,实现渐变
         * steps是指在多大的区域中渐变,
         */
        static generateTransitionalColor(color1: number, color2: number, steps: number): number[];
    }
}
declare namespace asf {
    /**
     * 时间日期工具
     *
     * @export
     * @class DateUtils
     */
    class DateUtils {
        constructor();
    }
}
/**
 * @DestroyUtils.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 FeiYin.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Game3DAnd2D
 * <br>Date:2013-9-27
 */
declare namespace asf {
    /**
     * 销毁对象的工具方法，会自动寻找数组以及"destroy","dispose"这个几个因素来销毁对象
     * @author sodaChen
     * #Date:2012-9-27
     */
    class DestroyUtils {
        static keyNames: string[];
        static destroy(obj: any): void;
    }
}
/**
 * @FunctionUtils.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-2-1
 */
declare namespace asf {
    /**
     *
     * @author sodaChen
     * Date:2012-2-1
     */
    class FuncUtils {
        /**
         * 执行回调函数
         * @param fun
         * @param thisObj
         * @param param 可选参数
         */
        static execute(fun: Function, thisObj: Object, param?: any): void;
        /**
         * 执行具有数组参数的回调函数
         * @param fun
         * @param thisObj
         * @param params 可选参数
         */
        static executeAry(fun: Function, thisObj: Object, params?: any[]): void;
    }
}
declare namespace asf {
    class JSUtils {
        /**
         * 添加一个js脚本
         * @param {String} js
         * @param {HTMLElement} ele
         */
        static addJS(js: string, ele?: HTMLElement): void;
    }
}
/**
 * @ObjectUtils.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/18
 */
declare namespace asf {
    class ObjectUtils {
        static hasProperty(target: Object, property: string): boolean;
        /**
         * 字符串的属性不为空
         * @param target
         * @param property
         * @returns {boolean}
         */
        static strNoNull(target: Object, property: string): boolean;
        static copyObject(target: Object, obj?: Object): any;
    }
}
/**
 * @RandomUtils.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-1-19
 */
declare namespace asf {
    /**
     * 随机数工具类
     * @author sodaChen
     * Date:2012-1-19
     */
    class RandomUtils {
        /**
         * 返回一个
         * @param minNum
         * @param maxNum
         * @returns {number}
         */
        static random(minNum: number, maxNum: number): number;
        /**
         * 随机获得int整数
         * @param minNum:最小范围(0开始)
         * @param maxNum:最大范围
         * @param stepLen:增加范围（整数，默认为1）
         * @return
         *
         */
        static randomInt(minNum: number, maxNum?: number, stepLen?: number): number;
        /**
         * 随机布尔值
         * @return
         *
         */
        static randomBoolean(): Boolean;
        /**
         * 取得随机正负波动值(1 / -1)
         * @return
         *
         */
        static randomWave(): number;
        /**
         * 概率是否成功(100%  1- 100)
         * @param rate:最大值
         * @return 随机生成数是否小于或者等于rate
         */
        static isRateSucced(rate: number): Boolean;
        static isRateSuccedInt(rate: number): Boolean;
        /**
         * 判断值是否小于0-10000
         */
        static isRandTrue(rate: number): boolean;
    }
}
declare namespace asf {
    class SortUtil {
        constructor();
        /**
         *  对数组的进行NUMBER排序
         *  对象数组
         *  re:true升序 false降序
         *  isNew 是否创建新的数组
         */
        static sortBy(_arr1: any[], re?: boolean, isNew?: boolean): any[];
        private static _p;
        private static _re;
        /**
         *  对数组的某个属性排序
         *  _arr1:对象数组
         *  p:属性名
         *  re:false降序 true升序
         *  isNew:是否创建新的数组
         */
        static sortBy2(_arr1: any[], p: string, re?: boolean, isNew?: boolean): any[];
        static sortFun(a: any, b: any): number;
        /**
         * 对数组的某些属性排序
         * @param _arr1 对象数组
         * @param p 属性名
         * @param re false 升序 true 降序
         * @param isNew 是否创建新的数组
         * @return
         *
         */
        static sortBy3(_arr1: any[], p: any[], re?: boolean, isNew?: boolean): any[];
        /**
         *  对数组的某个属性的属性排序
         *  对象数组
         *  属性名
         *  false 升序 true 降序
         *  isNew 是否创建新的数组
         */
        static sortBy4(_arr1: any[], _arr2: any[], reArr: any[], isNew?: boolean): any[];
        /**
         * 对数组的某些属性排序
         * @param _arr1 对象数组
         * @param p 属性名
         * @param reArr false 升序 true 降序
         * @param isNew 是否创建新的数组
         *
         * 例子:sortBy5(list,["type","id"],[fales,true],false);//对type升序排完再对id降序排
         * @return
         *
         */
        static sortBy5(_arr1: any[], p: any[], reArr?: boolean[], isNew?: boolean): any[];
    }
}
/**
 * @StringUtils.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-1-19
 */
declare namespace asf {
    /**
     * 字符串工具类
     * @author sodaChen
     * Date:2012-1-19
     */
    class StringUtils {
        /**
         * 字符串有内容
         * @param str
         * @returns {boolean}
         */
        static hasValue(str: string): boolean;
        /**
         * string str1 = "杰卫，这里有{0}个苹果，和{1}个香蕉！{0}个苹果{3} 元，{1}个香蕉{4} 元，一共{2}钱";
         * string str2 = "Hei jave, there are {0} apples，and {1} banana！ {2} dollar all together";
         * Console.WriteLine(string.Format(str1, 5, 10, 20, 7, 13));
         * Console.WriteLine(string.Format(str2, 5, 10, 20));
         * //输出：
         * 杰卫，这里有5个苹果，和10个香蕉！5个苹果7 元，10个香蕉13 元，一共20钱
         * Hei jave, there are 5 apples，and 10 banana！ 20 dollar all together
         * @param str
         * @param args
         * @return
         *
         */
        static formate(str: string, ...args: any[]): string;
        static formateArys(str: string, args: any[]): string;
        static formateOne(index: number, str: string, param: any): string;
        /**
         * <p>从字符串的末尾删除一个换行符，如果它在那里，
         *否则孤独。 换行符是“<code> \ n </ code>”
         *“<code> \ r </ code>”或“<code> \ r \ n </ code>”。</ p>
         *
         * <pre>
         * StringUtils.chomp(null)          = null
         * StringUtils.chomp("")            = ""
         * StringUtils.chomp("abc \r")      = "abc "
         * StringUtils.chomp("abc\n")       = "abc"
         * StringUtils.chomp("abc\r\n")     = "abc"
         * StringUtils.chomp("abc\r\n\r\n") = "abc\r\n"
         * StringUtils.chomp("abc\n\r")     = "abc\n"
         * StringUtils.chomp("abc\n\rabc")  = "abc\n\rabc"
         * StringUtils.chomp("\r")          = ""
         * StringUtils.chomp("\n")          = ""
         * StringUtils.chomp("\r\n")        = ""
         * </pre>
         *
         * @param str  the String to chomp a newline from, may be null
         * @return String without newline, <code>null</code> if null String input
         */
        static chomp(str: string): string;
        /**
         * <p>从末尾除去<code>分隔符</ code>
         * <code> str </ code>如果它在那里，否则孤独。</ p>
         *：
         * <p>现在更接近匹配Perl chomp。
         *对于以前的行为，使用#substringBeforeLast（String，String）。
         *此方法使用#endsWith（String）。</ p>
         *
         * <pre>
         * StringUtils.chompString(null, *)         = null
         * StringUtils.chompString("", *)           = ""
         * StringUtils.chompString("foobar", "bar") = "foo"
         * StringUtils.chompString("foobar", "baz") = "foobar"
         * StringUtils.chompString("foo", "foo")    = ""
         * StringUtils.chompString("foo ", "foo")   = "foo "
         * StringUtils.chompString(" foo", "foo")   = " "
         * StringUtils.chompString("foo", "foooo")  = "foo"
         * StringUtils.chompString("foo", "")       = "foo"
         * StringUtils.chompString("foo", null)     = "foo"
         * </pre>
         *
         * @param str将字符串转换为chomp，可以为null
         * @param分隔符分隔符String，可以为null
         * @return String无尾部分隔符，<code> null </ code>如果为空字符串输入
         */
        static chompString(str: string, separator: string): string;
        /**
         * <p>从两者中删除控制字符（char <= 32）
         *这个String的结尾，通过返回处理<code> null </ code>
         * <code> null </ code>。</ p>
         *：
         * <p> Trim删除开始和结束字符＆lt; = 32。
         *要剥离空格，请使用#strip（String）。</ p>
         *：
         * <p>要修剪您选择的字符，请使用
         * #strip（String，String）methods。</ p>
         *
         * <pre>
         * StringUtils.trim(null)          = null
         * StringUtils.trim("")            = ""
         * StringUtils.trim("     ")       = ""
         * StringUtils.trim("abc")         = "abc"
         * StringUtils.trim("    abc    ") = "abc"
         * </pre>
         *
         * @param str要修剪的字符串，可以为null
         * @返回修剪后的字符串，<code> null </ code>如果为null String输入
         */
        static trim(str: string): string;
        /**
         * <p>从字符串中删除所有空格。</p>
         *
         * <pre>
         * StringUtils.deleteWhitespace(null)         = null
         * StringUtils.deleteWhitespace("")           = ""
         * StringUtils.deleteWhitespace("abc")        = "abc"
         * StringUtils.deleteWhitespace("   ab  c  ") = "abc"
         * </pre>
         *
         * @param str要删除空格的String，可以为null
         * @返回无空格的字符串，<code> null </ code>如果为空字符串输入
         */
        static deleteWhitespace(str: string): string;
        static deleteFromString(str: string, pattern: RegExp): string;
        /**
         * <p>替换另一个字符串中所有出现的字符串。</ p>
         * ：
         * <p>传递给此方法的<code> null </ code>引用是无操作。</ p>
         *
         * <pre>
         * StringUtils.replace(null, *, *)        = null
         * StringUtils.replace("", *, *)          = ""
         * StringUtils.replace("any", null, *)    = "any"
         * StringUtils.replace("any", *, null)    = "any"
         * StringUtils.replace("any", "", *)      = "any"
         * StringUtils.replace("aba", "a", null)  = "aba"
         * StringUtils.replace("aba", "a", "")    = "b"
         * StringUtils.replace("aba", "a", "z")   = "zbz"
         * </pre>
         *
         * @param文本文本要搜索和替换，可以为null
         * @param pattern要搜索的字符串，可以为null
         * @param repl将String替换为，可以为null
         * @返回带有任何替换处理的文本，
         * <code> null </ code>如果为null String输入
         */
        static replace(text: string, pattern: string, repl: string): string;
        /**
         * <p>返回在String中传递的值，或者返回String
         * empty或<code> null </ code>，<code> defaultStr </ code>的值。</ p>
         *
         * <pre>
         * StringUtils.defaultIfEmpty(null, "NULL")  = "NULL"
         * StringUtils.defaultIfEmpty("", "NULL")    = "NULL"
         * StringUtils.defaultIfEmpty("bat", "NULL") = "bat"
         * </pre>
         *
         * @param str要检查的String，可以为null
         * @param defaultStr返回的默认字符串.如果输入为空（“”）或<code> null </ code>，可以为null
         * @返回传入的String或默认值
         */
        static defaultIfEmpty(str: string, defaultStr: string): string;
        /**
         * <p>检查字符串是否为空（""）或null</p>
         *
         * <pre>
         * StringUtils.isEmpty(null)      = true
         * StringUtils.isEmpty("")        = true
         * StringUtils.isEmpty(" ")       = false
         * StringUtils.isEmpty("bob")     = false
         * StringUtils.isEmpty("  bob  ") = false
         * </pre>
         *
         * <p>注：此方法在Lang 2.0版中更改。
         * 它不再修剪字符串。
         * 该功能在isBlank（）中可用。</ p>
         * ：
         * @param str要检查的String，可以为null
         * @return <code> true </ code>如果String为空或为null
         */
        static isEmpty(str: string): boolean;
        /**
         * <p>将字母改为首字母大写字母大写。
         * 没有其他字母更改。</ p>
         * ：
         * A <code> null </ code> input String返回<code> null </ code>。</ p>
         *
         * <pre>
         * StringUtils.capitalize(null)  = null
         * StringUtils.capitalize("")    = ""
         * StringUtils.capitalize("cat") = "Cat"
         * StringUtils.capitalize("cAt") = "CAt"
         * </pre>
         *
         * @param str将String转换为大写，可以为null
         * @返回大写字符串，<code> null </ code>如果为空字符串输入
         * @see titleize（String）
         * @see #uncapitalize（String）
         */
        static capitalize(str: string): string;
        /**
         * <p>取消资格将字符串更改为首字母到标题大小写。没有其他字母更改。</ p>
         *
         * <pre>
         * StringUtils.uncapitalize(null)  = null
         * StringUtils.uncapitalize("")    = ""
         * StringUtils.uncapitalize("Cat") = "cat"
         * StringUtils.uncapitalize("CAT") = "cAT"
         * </pre>
         *
         * @param str将String设置为uncapitalize，可以为null
         * @返回未资本化的字符串，<code> null </ code>如果为空字符串输入
         * @see #capitalize（String）
         */
        static uncapitalize(str: string): string;
        /**
         * 替换有颜色而替换对应的字符
         *
         * @static
         * @param {String} str
         * @param {Array} elementArr
         * @param {String} [symbol="&"]
         * @param {String} [color="#E2EAFB"]
         * @returns {String}
         *
         * @memberOf StringUtils
         */
        static replaceStrColorAndStr(str: string, elementArr: any[], symbol?: string, color?: string): string;
        /**
         * 替换字符
         * @param sourceStr 源字符串
         * @param elementArr 要替换的元素
         * @param symbol 分割符号
         * @return
         *
         */
        static replaceString(sourceStr: string, elementArr: any[], symbol?: string): string;
        /**
         * 替代字符串的颜色，返回html
         * 		注意：使用回默认颜色的都要加回默认颜色值
         */
        static replaceStrColor(str: string, color?: string, fontSize?: number, isBold?: boolean): string;
        /**
         * 匹配替换\\n(用于xml里的\n)
         * @param str
         *
         */
        static replaceGanN(str: string): string;
        /**获取带颜色的HTML字符串
         * str 字符串
         * color 颜色字符串 如f8d274
         */
        static getColorHtmlString(str: string, color: string): string;
        /**获取带颜色的HTML字符串(不带#)
                * str 字符串
                * color 颜色字符串 如f8d274
                */
        static getColorHtmlString2(str: string, color: string): string;
        /**获取textLink字符串 */
        static getLinkHtmlString(str: string, event: string, needUnderline?: boolean): string;
        /**数字转成带2位小数+1个中文字的格式
         * 可以是number或者string
         * 可以是带小数点的数值
         */
        static numberToShortString(num: any, showDot?: boolean): string;
        /**数字转汉字 */
        static numToChn(_num: number): string;
    }
}
declare namespace asf {
    class TimeUtils {
        static ServerTime: number;
        static ServerTimeStamp: number;
        private static BeiJingDate;
        /**
         * 获取服务器时间
         */
        static getServerTime(nowTime: number): number;
        static getServerTimeMill(nowTime: number): number;
        /**
         * 获取服务器时间的Ymd格式
         */
        static getServerTimeYmd(nowTime: number): number;
        /**
         * 获取服务器时间的Ymd格式
         */
        static getServerTimeYmd2(nowTime: number): string;
        static dateFormat(fmt: string, time: number): string;
        /**
    * 将时间长度格式化
    *
    */
        static diffTimeFormat(fmt: string, time: number, type?: number): string;
        /**
         * 返回 12:00:00这种格式
         * @param time
         * @returns {string}
         */
        static format1(time: any): string;
        static format2(time: any): string;
        static number2int(num: number): any;
        static number2str(num: number): string;
        static secondsPerDay: number;
        private static secondsPerHour;
        /**
         * 用剩余时间显示 0:00:00格式
         * @param totalsec
         * @param showHour 是否显示小时
         * @param showAsChinese 是否显示成中文时分秒
         * @return
         */
        static getRemainTimeBySeconds(totalsec: number, showHour?: boolean, showAsChinese?: boolean): string;
        private static formatString;
        /** 获取当前月份的天数 */
        static getMonthLength(nowTime: number): number;
        /**获取传入的时间戳的北京时间
         * time 时间戳(毫秒)
         * return Date
         */
        static getBeiJingTime(time: number): Date;
        /**
         * 根据星期获取中文字符
         * sunDayIs0 是否周日是0
         */
        static getWeekName(day: number, sunDayIs0?: boolean): string;
    }
}
/**
 * Created by sodaChen on 2017/3/14.
 */
declare namespace asf {
    class TypeofUtils {
        static isNumber(value: any): boolean;
        static isString(value: any): boolean;
        static isArray(value: any): boolean;
        static isBoolean(value: any): boolean;
    }
}
import asframe = asf;