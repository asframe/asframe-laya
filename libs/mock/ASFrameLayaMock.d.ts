declare namespace asf
{
    class TemplateModel
    {
        /**
         * 动画的实际模板对象
         */
        template:Laya.Templet;
        /**
         * 引用次数
         */
        ref:number;
        /**
         * 是否正在加载资源中
         */
        isLoading:boolean;
        /**
         * 回调列表
         */
        callBackList:asf.CallBack[];
    }
}