
import HashMap = asf.HashMap;
import FuncUtils = asf.FuncUtils;
import CallBack = asf.CallBack;
import Templet = Laya.Templet;
/**
 * 腊鸭的动画管理器，龙骨和spine骨骼动画都是存储腊鸭自身的动画格式来使用
 */
export default class AniManager
{
    private static templateMap:HashMap<string,asf.TemplateModel> = new HashMap<string, asf.TemplateModel>();
    /**
     * 加载一个模板文件
     * @param url
     * @param callBack
     * @param that
     */
    static load(url:string, callBack:Function,that:any):void
    {
        let model = this.templateMap.get(url);
        if(model)
        {
            if(model.isLoading)
            {
                model.callBackList.push(new CallBack(callBack,that));
                return;
            }
            model.ref++;
            FuncUtils.execute(callBack,that,model.template);
        }
        //进行生成和加载
        model = {} as any;
        this.templateMap.put(url,model);
        model.isLoading = true;
        model.callBackList = [];
        model.callBackList.push(new CallBack(callBack,that));
        model.template = new Laya.Templet();
        model.template.name = url;
        model.template.once(Laya.Event.COMPLETE, this, this.parseComplete);
        model.template.once(Laya.Event.ERROR, this, this.onError);
        model.template.loadAni(url);
    }

    /**
     * 释放资源，当引用次数为0时，则直接释放模板的资源
     * @param url
     */
    static release(url:string)
    {
        let model = this.templateMap.get(url);
        if(model)
        {
            model.ref--;
            if(model.ref <= 0)
            {
                //进行资源的释放
                model.template.destroy();
                this.templateMap.remove(url);
            }
        }
    }

    private static onError(template:Templet):void
    {
        console.log("parse error:" + template.name);
        //清除掉资源
        this.templateMap.remove(template.name);
    }
    private static parseComplete(template:Templet):void
    {
        let model = this.templateMap.get(template.name);
        if(!model)
        {
            console.error("template.name:" + template.name + "没有对应的asf.TemplateModel");
            return;
        }
        model.isLoading = false;
        let length = model.callBackList.length;
        model.ref = length;
        for (let i = 0; i < length; i++)
        {
            //执行回调函数
            model.callBackList[i].execute(template);
        }
        //已经缓存了，则清除掉相应的数组实例，不再需要了
        model.callBackList.length = 0;
        delete model.callBackList;
    }
}