/**
 * @.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2021-10-01
 */
import BaseView = mvc.BaseView;
import IModule = mvc.IModule;
import IModel = mvc.IModel;

/**
 * 基于Laya的mvc的view对象
 * @author sodaChen
 * #Date: 2021-10-01
 */
export default class LayaView<C, D extends IModel, M extends IModule<D>> extends BaseView<C, D, M>
{
    constructor()
    {
        super();
    }
}
