/**
 * @.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2021-10-01
 */
import BaseModule = mvc.BaseModule;
import IModel = mvc.IModel;

/**
 * laya的模块用来给游戏中的模块继承
 * @author sodaChen
 * #Date: 2021-10-01
 */
export default class LayaModule<D extends IModel> extends BaseModule<D>
{
    constructor()
    {
        super();
    }
}