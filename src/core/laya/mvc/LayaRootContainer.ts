/**
 * @LayaRootContainerts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2021-10-01
 */
import Sprite = Laya.Sprite;

/**
 * Egret实现的mvc框架的主体容器，所有基于mvc框架的view，最终都是放到这个容器中来
 * @author sodaChen
 * #Date:2021-10-01
 */
export class LayaRootContainer extends mvc.BaseRootContainer<Sprite>
{
    constructor(rootContainer:Sprite)
    {
        super(rootContainer);
    }
}