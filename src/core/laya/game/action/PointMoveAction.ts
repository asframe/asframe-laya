/**
 * 两点之间的移植机制
 *
 * @author sodaChen
 */
import Point = Laya.Point;
import {IRole} from "../scene/role/IRole";
import GameDefine from "../define/GameDefine";

export default class PointMoveAction<T extends IRole<any>> extends asf.TickAction<T>
{
    /** 移动结束目标点 **/
    protected endPoint:Point;
    /** 移动速度 **/
    protected speed:number;
    /** 当前角色的方向 **/
    protected currentDir:number;
    /**
     * 终止移动的绝对距离
     * @protected
     */
    protected endDistance:number = 10;
    /**
     * 设置终点
     * @param x
     * @param y
     * @param speed
     */
    setEndPoint(x:number,y:number,speed:number):void
    {
        if(!this.endPoint)
            this.endPoint = new Point();
        this.endPoint.x = x;
        this.endPoint.y = y;
        this.speed = speed;
    }

    protected onStart(): void
    {
        if(this.mActor.getLocation().x < this.endPoint.x)
        {
            this.mActor.setDir(GameDefine.Across_Dir_Right);
        }
        else
        {
            this.mActor.setDir(GameDefine.Across_Dir_Left);
        }
        this.currentDir = this.mActor.getDir();
    }

    /**
     * 是否结束移动了，子类可以重写这个方法，进行停止移动的条件编写
     * @protected
     */
    protected isMoveOver():boolean
    {
        //两点之间的距离，得重新计算
        // if(Math.abs(Point.distance(this.mActor.getLocation(),this.endPoint)) <= this.endDistance)
        // {
        //     return true;
        // }
        return false;
    }


    tick(): boolean
    {
        let rolePoint = this.mActor.getLocation();
        // let xSpeed:number = this.speed;
        // let ySpeed:number = this.speed;
        if(rolePoint.x > this.endPoint.x)
        {
            // xSpeed = -this.speed;
            //方向不一样，切换方向
            if(this.currentDir != this.mActor.getDir())
            {
                this.currentDir = -this.currentDir;
                this.mActor.setDir(this.currentDir);
            }
        }
        if(rolePoint.y > this.endPoint.y)
        {
            // ySpeed = -this.speed;
            if(this.currentDir != this.mActor.getDir())
            {
                this.currentDir = -this.currentDir;
                this.mActor.setDir(this.currentDir);
            }
        }
        let angle: number = Math.atan2((this.endPoint.y - rolePoint.y), (this.endPoint.x - rolePoint.x)) * 180 / Math.PI;
        if (angle < 0)
        {
            angle += 360;
        }
        let xSpeed = this.speed * Math.cos(angle * Math.PI / 180);
        let ySpeed = this.speed * Math.sin(angle * Math.PI / 180);
        rolePoint.x += xSpeed;
        rolePoint.y += ySpeed;
        this.mActor.setLocation(rolePoint.x,rolePoint.y);
        //判断是否需要终止移动了
        if(this.isMoveOver())
        {
            //直接把自己销毁
            this.destroy();
        }
        return false;
    }
}