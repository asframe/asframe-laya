/**
 * 播放动画的json数据类
 */
import Rectangle = Laya.Rectangle;
import Point = Laya.Point;
import GameDefine from "../define/GameDefine";

export default class PlayerJsonData
{
	setting: Object;
	totalTime: number = 0;
	keyFrameMap: asf.HashMap<number, FrameJson>;
	keyFrameIndexMap: asf.HashMap<string, number>
	keyFrame: FrameJson[] = [];
	allFrames: Object[] = [];
	frameTick: number = 0;
	rect: Rectangle;
	/**模型第一帧中心点 */
	centerPoint: Point;

	public constructor()
	{
		this.keyFrameMap = new asf.HashMap<number, FrameJson>();
		this.keyFrameIndexMap = new asf.HashMap<string, number>();
	}

	/**获取下一帧的帧数 */
	getFrameTime(index: number): number
	{
		if (index >= this.allFrames.length)
		{
			return this.frameTick;
		}
		let frame = this.allFrames[index];
		if (frame)
		{
			return frame["delayTime"] + this.frameTick;
		}
		else
		{
			console.error("frame为空！：" + "allFrames:" + this.allFrames + "index:" + index + "this.frameTick:" + this.frameTick);
			return this.frameTick;
		}
	}

	setSetting(data: any, url: string): void
	{
		this.setting = data.setting;
		// this.keyFrame = this.setting["keyFrame"];
		this.keyFrame = [];
		//这里加1是因为飞鹿的打包工具预览满了1帧，美术需要设置更短的帧来达到需要的效果
		//但在程序里就会变快了，所以这里加1
		this.frameTick = this.setting["frameTick"] ? this.setting["frameTick"] + 1 : 3;
		this.allFrames = this.setting["allFrame"];
		var total: number = 0;
		for (var i: number = 0; i < this.allFrames.length; i++)
		{
			total += GameDefine.OtherFps * (this.allFrames[i]["delayTime"] + this.frameTick);
			if (this.allFrames[i]["dataStr"] != "")
			{
				var frameVo: FrameJson = new FrameJson();
				frameVo.dataStr = this.allFrames[i]["dataStr"];
				frameVo.index = this.allFrames[i]["index"];
				frameVo.playTime = total;
				this.keyFrame.push(frameVo);
				this.keyFrameMap.put(frameVo.index, frameVo);
				this.keyFrameIndexMap.put(frameVo.dataStr, frameVo.index);
			}
		}
		this.totalTime = total //+ GameDefine.MoveFps * 2  //* 10//+ GameDefine.MoveFps * 10;//this.setting["totalTime"];
		// if (this.setting["keyFrames"])
		// {
		// 	this.keyFrame = this.setting["keyFrames"]
		// }

		if (data["mc"])
		{
			var i: number = url.lastIndexOf("/");
			var u: string = url.substr(i + 1);
			var key: string = u.substr(0, u.indexOf("_json"));
			if (key == "") key = u.substr(0, u.indexOf(".json"));
			if (data["mc"][key])
			{
				var resObj: any = data["mc"][key]["frames"][0];
				if (resObj)
				{
					var h: number = data["res"][resObj["res"]]["h"]
					var w: number = data["res"][resObj["res"]]["w"]
					var x: number = data["res"][resObj["res"]]["x"]
					var y: number = data["res"][resObj["res"]]["y"]
					this.rect = new Rectangle(x, y, w, h);
					// this.centerPoint = Point.create(resObj["x"], resObj["y"]);
				}
			}
		}
		else
		{
			this.rect = new Rectangle(0, 0, 0, 0);
		}
		// if (setting["rect"])
		// {
		// 	this.rect = new Rectangle(setting["rect"].x, setting["rect"].y, setting["rect"].w, setting["rect"].h);
		// }
	}
}

export class FrameJson
{
	dataStr: string;
	delayTime: number;
	index: number;
	playTime: number;
	public constructor()
	{
	}
}
