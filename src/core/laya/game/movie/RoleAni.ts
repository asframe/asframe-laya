import AniManager from "../../ani/AniManager";


import Sprite = Laya.Sprite;

/***
 * 最基础的角色动画播放器,主要是播放laya的动画（spine和龙谷转换的动画）
 * @author sodaChen
 * Date:2010-5-21
 */
export default class RoleAni extends Sprite
{
	protected currAct:string;
	protected count:number;
	protected completeBack:asf.CallBack;
	protected isLoading:boolean;
	protected url:string;
	/**
	 * 骨骼动画对象
	 */
	protected skeleton:Laya.Skeleton;
	constructor()
	{
		super();
	}

	private onPlayComplete(evt:Event):void
	{
		console.info("动作播放结束了");
	}

	/*
	 * 加载图片资源
	 * @param url
	 * @param complete
	 * @param that
	 */
	load(act:string,url:string,complete?:Function,that?:any):void
	{
		if(this.url == url)
		{
			return ;
		}
		this.url = url;
		this.currAct = act;
		if(complete)
		{
			if(!this.completeBack)
				this.completeBack = new asf.CallBack(complete,that,url);
			else
			{
				this.completeBack.init(complete,that);
			}
		}
		AniManager.load(url,this.onLoad,this);
	}
	protected onLoad(template:Laya.Templet): void
	{
		console.info(template.name + "图像资源加载完成");
		if(!this.skeleton)
		{
			this.skeleton = template.buildArmature(0);
			this.addChild(this.skeleton);
			this.skeleton.on(Laya.Event.COMPLETE,this,this.onComplete);
			this.skeleton.player.on(Laya.Event.COMPLETE,this,this.onPlayerComplete);
			this.skeleton.on(Laya.Event.LABEL,this,this.onLabel);
			// this.skeleton.pos(0, 0);
			this.selfPlay();
			if(this.completeBack)
			{
				this.completeBack.execute();
			}
		}
	}
	private onLabel(evt:Laya.Event):void
	{
		console.log("onComplete");
	}
	private onPlayerComplete(evt:Laya.Event):void
	{
		console.log("onPlayerComplete");
	}
	private onComplete(evt:Laya.Event):void
	{
		console.log("onComplete");
	}
	/**
	 * 根据指定的名字进行播放
	 * @param act 动作名字
	 * @param count 播放次数，默认-1为循环播放
	 */
	playAct(act:string,count:number = -1):void
	{
		//当前动作一样，或者没有动画数据，则不进行处理
		if(act == this.currAct || !this.skeleton)
		{
			if(count != 1)
				return ;
		}
		this.currAct = act;
		this.count = count;
		//后面播放可以自己来控制，这样方便加速之类的控制
		this.skeleton.play(act,count == -1);
	}
	/**
	 * 内部进行自我播放，如果有播放动作的话
	 */
	private selfPlay():void
	{
		if(this.currAct)
		{
			let act = this.currAct;
			this.currAct = null;
			this.playAct(act);
		}
	}
	private stopPlay():void{
		// this.stop()
	}
}
