/***
 * 最基础的特效播放器
 */

import Sprite = Laya.Sprite;

export class EffetMovie extends Sprite
{
	static Play_Complete: string = "Play_Complete";

	protected currAct:string;
	protected count:number;
	protected completeBack:asf.CallBack;
	protected isLoading:boolean;
	protected url:string;
	constructor()
	{
		super();


	}

	private onPlayComplete(evt:Event):void
	{
		console.info("动作播放结束了");
	}
	/**
	 * 加载图片资源
	 * @param url
	 * @param complete
	 * @param that
	 */

	load(act:string,url:string,complete?:Function,that?:any):void
	{
		// if(this.url == url)
		// {
		// 	return ;
		// }
		this.url = url;
		this.currAct = act;
		if(complete)
		{
			if(!this.completeBack)
				this.completeBack = new asf.CallBack(complete,that,url);
			else
			{
				this.completeBack.init(complete,that);
			}
		}
		// MovieMgr.getInstance().load(url,this.onLoad,this);
	}
	// protected onLoad(mcDataFactory: egret.MovieClipDataFactory, url: string): void
	// {
	// 	console.info(url + "图像资源加载完成");
	// 	this.mcDataFactory = mcDataFactory;
	//
	// 	this.selfPlay();
	// 	if(this.completeBack)
	// 	{
	// 		this.completeBack.execute();
	// 	}
	// }
	/**
	 * 根据指定的名字进行播放
	 * @param act 动作名字
	 * @param count 播放次数，默认-1为循环播放
	 */
	playAct(count:number = -1):void
	{
		// //count = 1;
		// this.movieClipData = this.mcDataFactory.generateMovieClipData();
		// // this.currAct = act;
		// this.count = count;
		// //后面播放可以自己来控制，这样方便加速之类的控制
		// this.gotoAndPlay(0,count)
		// //this.play(count);
		// if(count!=-1)
		// this.addEventListener(egret.Event.COMPLETE,this.isPlayComplete,this);
	}
	/**
	 * 内部进行自我播放，如果有播放动作的话
	 */
	private selfPlay():void
	{
		if(this.currAct)
		{
			let act = this.currAct;
			this.currAct = null;
			this.playAct();
		}
	}
	//自我删除
	private isPlayComplete():void{
		//this.stop();
		// this.removeEventListener(egret.Event.COMPLETE,this.onPlayComplete,this)
		// if(this.parent)
		// 	this.parent.removeChild(this);
	}
}