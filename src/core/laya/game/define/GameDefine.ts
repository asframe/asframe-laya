/**
 * @Game
 * Define.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:GameEngine
 * <br>Date:2013-6-30
 */

/**
 * 场景相关的常量定义
* @author sodaChen
* Date:2013-6-30
*/
export default class GameDefine
{
	/**
	 * 每多少毫秒走1帧
	 */
	static MoveFps: number = 33;

	/**
	 * 每多少毫秒走1帧
	 */
	static OtherFps: number = 33;

	/**
	 * 游戏模型的方向配置
	 */
	static Game_Model_Dir: number = 2;
	/**
	 * 游戏模型2方向
	 */
	static Game_Model_Dir_2: number = 2;
	/**
	 * 游戏模型5方向
	 */
	static Game_Model_Dir_5: number = 5;


	/**
	* 公共CD
	*/
	static Figth_Common_CD: number = 1000;

	/**
	 * 移动网络允许延迟的数值
	 */
	static Net_Move_Delay: number = 2000;
	/**
	 * 攻击网络允许延迟的数值
	 */
	static Net_Atk_Delay: number = 500;

	static Original_Width: number = 1000;
	static Original_Height: number = 600;

	// static verson: string = "default";
	// static verson: string = "legDemo";
	static ReleaseUrl: string;
	static Res_Main_Url: string;
	static Res_ASSETS: string = "";
	static Res_Loacal: string;

	static Res_Map_Url: string;
	static Res_Weapon_Url: string;
	static Res_Wing_Url: string;
	static Res_Role_Url: string;
	static Res_Mount_Url: string;
	static Res_Effect_Url: string;
	static Res_Bless_Url: string;
	static Res_Asset_Common: string;
	static Res_Icon_Url: string;
	static Res_Mini_Map: string;
	static Res_Module_Map: string;
	static Res_Emoji_Url: string;
	static RES_ICON_SIZE: string[] = ["small", "middle", "big", "small35x35", "middle60x60"];

	static Server_Ip: string;

	/**断线重连提示描叙语 */
	static SocketOffDesc: string = "网络好像出了点问题，请重试一下吧.....";
	/** 男 */
	static Sex_Man: number = 1;
	/** 女 */
	static Sex_Woman: number = 2;


	/** 背包格子数量变化 */
	///////////////////场景中的层，根据该值对场景的元素进行归类////////////////////
	/** 地图层 **/
	static MAP_LAYER: string = "mapContainer";
	/** 影子层 **/
	static MAP_SHADOW_LAYER: string = "shadowContainer";
	/** 脚底特效层 **/
	static MAP_MAGIC_LAYER: string = "magicContainer";
	/** 地图特效层 **/
	static MAP_EFFECT_LAYER: string = "mapEffectContainer";
	/** 掉落层 **/
	static MAP_DROP_LAYER: string = "dropContainer";
	/** 特效层 **/
	static EFFECT_Bottom_LAYER: string = "effectBottomContainer";
	/** 角色层 **/
	static ROLE_LAYER: string = "roleContainer";
	/** 角色遮罩层 **/
	static ROLE_NAME_LAYER: string = "roleNameContainer";
	/** 特效层 **/
	static EFFECT_LAYER: string = "effectContainer";
	/** 受击特效层 **/
	static HIT_EFFECT_LAYER: string = "hitEffectLayer";
	/** 飘血层层 **/
	static HURT_EFFECT_LAYER: string = "hurtContainer";
	/** 中间层 **/
	static CENTER_LAYER: string = "centerContainer";
	/** 最顶层 **/
	static TOP_LAYER: string = "topContainer";

	////////////////战斗场景状态/////////////////
	/** 战斗前的准备 **/
	static BATTLE_READY: string = "battleReady";
	/** 战斗中 **/
	static BATTLING: string = "battling";
	/** 战斗结束 **/
	static BATTLE_OVER: string = "battleOver";



	//////////////横板的方向/////////////////
	/**
	 * 朝右边
	 */
	static Across_Dir_Right:number = 1;
	/**
	 * 朝左边
	 */
	static Across_Dir_Left:number = -1;
}