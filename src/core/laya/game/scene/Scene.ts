/**
 * @SpriteScene.as
 *
 * @author sodaChen mail:sujun10@21cn.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame GameEngine
 * <br>Date:2012-10-15
 */
import Sprite = Laya.Sprite;

import GameDefine from "../define/GameDefine";
import Point = Laya.Point;
import IScene from "./IScene";
import Containers from "../../display/Containers";
import IUnit from "./unit/IUnit";
import DepthYHelper from "./depth/DepthYHelper";
import IDepthHelper from "./depth/IDepthHelper";

/**
 * 具备容器的场景对象，一般是作为抽象类。这个场景应该是比较普通的场景，不应该增加深度管理之类的
 * @author sodaChen
 * Date:2012-10-15
 */
export class Scene extends Containers implements IScene
{
	/** 地图层 **/
	mapContainer: Sprite = null;
	/** 影子层 **/
	shadowContainer: Sprite = null;
	/** 脚底特效层 **/
	magicContainer: Sprite = null;
	/** 地图特效层 **/
	mapEffectContainer: Sprite = null;
	/** 地图掉落层 **/
	dropContainer: Sprite = null;
	/** 底部特效层 **/
	effectBottomContainer: Sprite = null;
	/** 角色层 **/
	roleContainer: Sprite = null;
	/** 角色名字层 **/
	roleNameContainer: Sprite = null;
	/** 特效层 **/
	effectContainer: Sprite = null;
	/** 受击特效层 **/
	hitEffectLayer: Sprite = null;
	/** 飘血层 **/
	hurtContainer: Sprite = null;
	/** 中间层 **/
	centerContainer: Sprite = null;
	/** 最顶层 **/
	topContainer: Sprite = null;

	/** 场景id **/
	id: string;
	/** 场景类型 **/
	type: number;
	/** 元件集合 **/
	unitMap: asf.HashMap<string, IUnit<IScene>>;
	/** 元件名字的集合 **/
	unitNameMap: asf.HashMap<string, IUnit<IScene>>;
	/** 实体分类集合 **/
	entityTypeMap: asf.HashMap<number, IUnit<IScene>[]>;
	/** 深度管理器 **/
	depthHelper: IDepthHelper;
	/** 需要做深度管理的层名称 **/
	depthLayer: string;
	/** 状态 **/
	status: string;

	constructor()
	{
		super();
		this.unitMap = new asf.HashMap<string, IUnit<IScene>>();
		this.unitNameMap = new asf.HashMap<string, IUnit<IScene>>();
		this.entityTypeMap = new asf.HashMap<number, IUnit<IScene>[]>();
	}

	/**
	 *
	 * @param depthLayer
	 * @param layers
	 * @param depthHelper
	 */
	init(depthLayer: string = null, layers: string[] = null, depthHelper: IDepthHelper = null): void
	{
		//生成相关的默认数据
		if (depthLayer == null)
			depthLayer = GameDefine.ROLE_LAYER;
		if (layers == null)
			layers = [
				GameDefine.MAP_LAYER,
				GameDefine.MAP_SHADOW_LAYER,
				GameDefine.MAP_MAGIC_LAYER,
				GameDefine.MAP_EFFECT_LAYER,
				GameDefine.MAP_DROP_LAYER,
				GameDefine.EFFECT_Bottom_LAYER,
				GameDefine.ROLE_LAYER,
				GameDefine.ROLE_NAME_LAYER,
				GameDefine.EFFECT_LAYER,
				GameDefine.HIT_EFFECT_LAYER,
				GameDefine.HURT_EFFECT_LAYER,
				GameDefine.TOP_LAYER
			];
		this.setContainers(layers);
		//自动赋值
		for (let i: number = 0; i < layers.length; i++)
		{
			if (this.hasOwnProperty(layers[i]))
			{
				this[layers[i]] = this.getChildContainer(layers[i]);
			}
		}
		if (depthHelper == null)
		{
			depthHelper = new DepthYHelper();
		}
		if(!depthLayer)
		{
			//默认就是设置角色层的容器，里面的对象需要做深度交换
			depthLayer = GameDefine.ROLE_LAYER;
		}

		this.depthLayer = depthLayer;
		depthHelper.setTarget(this.getChildContainer(depthLayer));
		this.depthHelper = depthHelper;
		//启动定时器
		// Tick.addFun(depthHelper.update);
		//原来
		depthHelper.start(33);
		//最新的改成1秒结算一次
		// depthHelper.start(1000);
		this.onInit();
	}
	/**
	 * 进入场景
	 */
	enter(): void
	{

	}
	onEnter(): void
	{

	}
	/**
	 * 离开场景
	 */
	leave(): void
	{

	}
	onLeave(): void
	{

	}
	protected onInit(): void
	{

	}
	setStatus(status: string): void
	{
		this.status = status;
	}
	getStatus(): string
	{
		return status;
	}

	/**
	 * 重置场景元件的id
	 * @param oldPartId:旧id
	 * @param newPartId:新id
	 */
	restUnitId(oldPartId: string, newPartId: string): void
	{
		if (this.unitMap.hasKey(oldPartId))
		{
			var unit = this.unitMap.remove(oldPartId);
			this.unitMap.put(newPartId, unit);
		}
	}
	getId(): string
	{
		return this.id;
	}
	getName(): string
	{
		return this.name;
	}
	getType(): number
	{
		return this.type;
	}
	setLocation(x: number, y: number): void
	{
		this.x = x;
		this.y = y;
	}

	getLocation(): Point
	{
		return new Point(this.x, this.y);
	}
	destroy(o: any = null): void
	{
		this.depthHelper.removeAll();
		//释放资源,背景，物件，角色
		let unitsObj: Object = this.unitMap.getContainer();
		for (let key in unitsObj)
		{
			this.removeUnitById(key, o);
		}
		this.unitMap.clear();
		this.unitNameMap.clear();
	}

	/**
	 * 更新场景
	 *
	 *
	 * @memberOf Scene
	 */
	update(): void
	{
	}

	/**
	 * 添加一个元件(包含比较广，如物件，物品等)
	 * @param Unit
	 *
	 */
	addUnit(unit: IUnit<IScene>): void
	{
		asf.Assert.notNull(unit.getId(), "重现id为空的情况：" + unit);
		var isRepeat: boolean = false;
		//检测是否重复的id
		if (this.unitMap.hasKey(unit.getId()))
		{
			//抛出错误
			console.error(unit + "出现重复的ID:" + unit.getId());
			isRepeat = true;
		}
		this.unitMap.put(unit.getId(), unit);
		this.unitNameMap.put(unit.getName(), unit);
		unit.setScene(this);
		//添加到对应的层中
		this.getChildContainer(unit.getLayer()).addChild(unit.getDisplay());

		//以前arpg的
		// this.addUnitToTypeMap(unit);
		// //添加到对应的层中
		// if (!isRepeat)
		// {
		// 	this.queue.push(unit);
		// 	this.queueAddChild();
		// }
	}

	/**
	 * 将实体进行分类
	 */
	addUnitToTypeMap(unit: IUnit<IScene>): void
	{
		var units: IUnit<IScene>[] = this.entityTypeMap.get(unit.type);
		if (!units)
		{
			units = [];
			this.entityTypeMap.put(unit.type, units);
		}
		if (units.indexOf(unit) == -1)
		{
			units.push(unit);
		}
	}

	/**
	 * 将分类实体进行移除
	 */
	removeUnitToTypeMap(unit: IUnit<IScene>): void
	{
		// if (unit instanceof GameEntity)
		// {
		// 	var units: IUnit[] = this.entityTypeMap.get(unit.type);
		// 	if (units)
		// 	{
		// 		var index: number = units.indexOf(unit);
		// 		units.splice(index, 1);
		// 	}
		// }
	}

	/**
	 * 根据类型获取分类实体
	 */
	getUnitsByType(types: number[]): IUnit<IScene>[]
	{
		var arr: IUnit<IScene>[] = [];
		for (var i: number = 0; i < types.length; i++)
		{
			var units: IUnit<IScene>[] = this.entityTypeMap.get(types[i]);
			if (units)
			{
				arr = arr.concat(units);
			}
		}

		return arr
	}

	public queue: IUnit<IScene>[] = []
	queueAddChild(): void
	{
		if (this.queue.length > 0)
		{
			var unit: IUnit<IScene> = this.queue.shift();
			this.getChildContainer(unit.getLayer()).addChild(unit.getDisplay());
			unit.onAdd();

			//添加到深度管理器
			if (this.depthLayer == unit.getLayer() && this.depthHelper != null)
			{
				this.depthHelper.addChild(unit.getDisplay());
			}
			// if (unit instanceof Hero)
			// {
			// 	this.getChildContainer(unit.getLayer()).addChild(unit.getDisplay());
			// 	unit.onAdd();

			// 	//添加到深度管理器
			// 	if (this.depthLayer == unit.getLayer() && this.depthHelper != null)
			// 	{
			// 		this.depthHelper.addChild(unit.getDisplay());
			// 	}
			// }
			// else
			// {
			// 	mvc.RootContainer.getInstance().topLayer.addChild(unit.getDisplay());
			// 	// this.getChildContainer(unit.getLayer()).addChild(unit.getDisplay());
			// 	unit.onAdd();
			// }
		}
	}

	removeUnitById(id: string, isDispose: Boolean = false): IUnit<IScene>
	{
		var unit: IUnit<IScene> = this.unitMap.remove(id) as IUnit<IScene>;
		if (unit != null)
		{
			this.unitNameMap.remove(unit.getName());
			this.removeUnit(unit, isDispose);
		}
		else
		{
			// console.error(this, "removeElement 提供的id：" + id + " 找不到对应的角色");
			// Log.error(this,"removeElement 提供的id：" + id + " 找不到对应的角色");
		}
		return unit;
	}

	getUnits(type: number = 0): IUnit<IScene>[]
	{
		if (type == 0)
		{
			return this.unitMap.values();
		}
		var ary: IUnit<IScene>[] = [];
		var values: IUnit<IScene>[] = this.unitMap.values();
		var unit: IUnit<IScene> = null;
		for (var i: number = 0; i < values.length; i++)
		{
			unit = values[i];
			if (unit.getType() == type)
			{
				ary.push(unit);
			}
		}
		return ary;
	}

	getDepthHelper(): IDepthHelper
	{
		return this.depthHelper;
	}
	getUnitById(id: string): IUnit<IScene>
	{
		return this.unitMap.get(id);
	}
	getUnitByName(unitName: string): IUnit<IScene>
	{
		return this.unitNameMap.get(unitName);
	}
	removeUnitByName(name: string, isDispose: Boolean = false): IUnit<IScene>
	{
		var unit: IUnit<IScene> = this.getUnitByName(name);
		if (unit != null)
		{
			return this.removeUnitById(unit.getId(), isDispose);
		}
		return null;
	}

	private removeUnit(unit: IUnit<IScene>, isDispose: Boolean = false): void
	{
		this.removeUnitToTypeMap(unit);
		if (this.queue.indexOf(unit) != -1)
		{
			let queueIndex: number = this.queue.indexOf(unit)
			this.queue.splice(queueIndex, 1)
		}

		if (this.depthLayer == unit.getLayer())
		{
			// if (unit instanceof game.MonsterEntity)
			// {
			// 	console.log("减少" + unit.mId);
			// }
			this.depthHelper.removeChild(unit.getDisplay());
		}
		// else
		// {
		// 	if (unit instanceof game.MonsterEntity)
		// 	{
		// 		console.log("depthLayer");
		// 	}
		// }

		if (unit.getDisplay().parent == this.getChildContainer(unit.getLayer()))
			this.getChildContainer(unit.getLayer()).removeChild(unit.getDisplay());

		unit.onRemove();
		if (isDispose)
		{
			//释放掉这个角色的资源
			unit.destroy();
		}
	}
}
