/**
 * 角色机制，所有在场景中活物，都必须实现这个接口，具备基础公共方法
 *
 * @author sodaChen
 */
import IUnit from "../unit/IUnit";
import IScene from "../IScene";
import IActor = asf.IActor;

export default interface IRole<T extends IScene> extends IUnit<T>, IActor
{
    /**
     * 设置方向
     * @param dir
     */
    setDir(dir:number):void;

    /**
     * 获得方向值
     */
    getDir():number;
}