/**
 * 角色机制，所有在场景中活物，基础实现对象
 *
 * @author sodaChen
 */
import GameDefine from "../../define/GameDefine";
import IScene from "../IScene";
import Unit from "../unit/Unit";
import IRole from "./IRole";
import RoleAni from "../../movie/RoleAni";

export default class BaseRole<T extends IScene> extends Unit<T> implements IRole<T>
{
    /**
     * 表演者的代理对象，代替表演中实现一些通用功能。当然也可以实现一个基类表演者。
     * 建议采用组合模式
     */
    protected actorProxies:asf.ActorProxies;
    /**
     * 角色当前的方向
     */
    protected dir:number;
    /**
     * 角色动画对象
     */
    protected roleAni:RoleAni;

    constructor()
    {
        super();
        this.actorProxies = new asf.ActorProxies(this);
        this.dir = GameDefine.Across_Dir_Right;
        this.roleAni = new RoleAni();
        this.addChild(this.roleAni);
    }

    setDir(dir:number):void
    {
        if(this.dir == dir)
        {
            return ;
        }
        this.dir = dir;
        this.onDirChange();
    }
    getDir():number
    {
        return this.dir;
    }
    /**
     * 方向改变的回调方法，一般是给子类重写的
     */
    onDirChange()
    {

    }

    act(action: asf.IAction<any>, callBack?: Function, thisObj?: any, param?: any): boolean {
        return this.actorProxies.act(action,callBack,thisObj,param);
    }

    delAction(action: asf.IAction<any>, isDestroy?: boolean): void {
        this.actorProxies.delAction(action,isDestroy);
    }
}