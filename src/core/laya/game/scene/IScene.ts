/**
 * @IScene.as
 *
 * @author sodaChen mail:sujun10@21cn.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame GameEngine
 * <br>Date:2012-10-6
 */


import IDestroy = asf.IDestroy;
import Sprite = Laya.Sprite;
import IUnit from "./unit/IUnit";
import IDepthHelper from "./depth/IDepthHelper";

/**
 * 场景控制器接口,游戏中所有的场景都基于这个接口
 * @author soda.chen
 *
 */
export default interface IScene extends IDestroy
{
	/**
	 * 进入场景
	 */
	enter(): void;
	/**
	 * 离开场景
	 */
	leave(): void;
	/**
	 * 获取场景的ID
	 * @return
	 *
	 */
	getId(): string;
	/**
	 * 获取场景的名称
	 * @return
	 *
	 */
	getName(): string;
	/**
	 * 获取场景的类型
	 * @return
	 *
	 */
	getType(): number;
	/**
	 * 更新场景
	 * @memberOf IScene
	 */
	update(): void;
	/**
	 * 重置场景元件的id
	 * @param oldPartId:旧id
	 * @param newPartId:新id
	 */
	restUnitId(oldUnitId: string, newUnitId: string): void;

	/**
	 * 添加一个元件(包含比较广，如物件，物品等)
	 * @param Unit
	 *
	 */
	addUnit(sceneUnit: IUnit<IScene>): void;

	removeUnitById(id: string, isDispose?: boolean): IUnit<IScene>;
	removeUnitByName(id: string, isDispose?: boolean): IUnit<IScene>;

	/**
	 * 返回当前类型所有角色的数组
	 * @param type
	 * @return
	 *
	 */
	getUnits(type: number): Array<IUnit<IScene>>;


	getUnitById(id: string): IUnit<IScene>;
	getUnitByName(id: string): IUnit<IScene>;
	/**
	 * 获取一个场景视图.
	 * @return
	 *
	 */
	getContainer(): Sprite;
	getDepthHelper(): IDepthHelper;
}
