/**
 * @ISceneUnit.as
 *
 * @author sodaChen mail:sujun10@21cn.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame GameEngine
 * <br>Date:2012-10-5
 */

import IDestroy = asf.IDestroy;
import Sprite = Laya.Sprite;
import Point = Laya.Point;
import IScene from "../IScene";

/**
 * 场景单元接口，所以需要放到场景中的元素，都必须实现该接口
 * 主要是游戏场景中的各种元素
 * @author sodaChen
 * Date:2012-10-5
 */
export default interface IUnit<T extends IScene> extends IDestroy
{	/**
	 * 类型
	 */
	type: number;
	/**
	 * 获取场景单元的ID
	 * @return
	 *
	 */
	getId(): string;
	/**
	 * 获取场景单元的名称
	 * @return
	 *
	 */
	getName(): string;
	/**
	 * 获取场景单元的类型
	 * @return
	 *
	 */
	getType(): number;
	/**
	 * 返回对象所在的容器层
	 * @return
	 */
	getLayer(): string;
	/**
	 * 设置当前所在的场景.
	 * @return
	 *
	 */
	setScene(scene: T): void;
	/**
	 * 返回当前所在的场景.
	 * @return
	 *
	 */
	getScene(): T;
	/**
	 * 获取显示对象
	 * @return
	 */
	getDisplay(): Sprite;
	/**
	 * 设置坐标
	 * @param x
	 * @param y
	 *
	 */
	setLocation(x: number, y: number): void;
	/**
	 * 显示名字标题(名字在元件里显示出来)
	 * @param isShow
	 *
	 */
	// showNameTitle(isShow: Boolean, title?: string): void;
	/**
	 * 返回坐标
	 * @return
	 *
	 */
	getLocation(): Point;

	/**
	 * 销毁处理
	 * @memberOf IUnit
	 */
	onRemove(): void;
	/**
	 * 添加到场景后
	 */
	onAdd(): void;
}
