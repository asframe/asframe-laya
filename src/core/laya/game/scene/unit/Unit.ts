/**
* @SpriteUnit.as
*
* @author sodaChen mail:sujun10@21cn.com
* @version 1.0
* <br>Copyright (C), 2012 ASFrame.com
* <br>This program is protected by copyright laws.
* <br>Program Name:ASFrame GameEngine
* <br>Date:2012-10-7
*/

import IScene from "../IScene";
import IUnit from "./IUnit";
import GameDefine from "../../define/GameDefine";
import Containers from "../../../display/Containers";
import Point = Laya.Point;
import Sprite = Laya.Sprite;

/**
 * 场景的基本单元，所以的位于场景的元素都必须是Unit以及子类或者IUni接口.可用于显示静态的显示对象
 * @author sodaChen
 * Date:2012-10-7
 */
export default class Unit<T extends IScene> extends Containers implements IUnit<T> {

	/**实体类型**/
	type: number;
	/**id进行替换前的id (如怪物死亡后进行id替换)*/
	// preId: string;
	/** id **/
	mId: string;
	/** 类型 **/
	mType: number;
	/** 类型 **/
	mScene: T;
	/** 处于层（即所在容器） **/
	mLayer: string;
	/** 名字标题 **/
		// mNameTitle: INameTitle;
	mLocation: Point;
	/** 是否已经销毁 **/
	isDestroy: boolean = false;


	constructor() {
		super();
		this.x = 0;
		this.y = 0;
		this.mLocation = new Point();
		//默认角色层
		this.mLayer = GameDefine.ROLE_LAYER;
	}

	setId(id: string): void {
		//如果场景不为空，则重新刷新在场景中的引用
		if (this.mScene != null && this.mId != null) {
			this.mScene.restUnitId(this.mId, id);
		}
		this.mId = id;
	}

	getId(): string {
		return this.mId;
	}

	getNumberId(): number {
		var num: number = Number(this.mId);
		if (isNaN(num))
			return 0;
		return num;
	}

	getName(): string {
		return this.name;
	}

	/**
	 * 设置用户名的显示
	 */
	setName(name: string): void {
		this.name = name;
	}

	getScene(): T {
		return this.mScene;
	}

	setScene(scene: T): void {
		this.mScene = scene;
	}

	setType(type: number): void {
		this.mType = type;
	}

	getType(): number {
		return this.mType;
	}

	getLayer(): string {
		return this.mLayer;
	}

	setLayer(layer: string): void {
		this.mLayer = layer;
	}

	remove(isDispose: boolean = true): void {
		if (this.mScene) {
			this.mScene.removeUnitById(this.mId, isDispose);
		}
		// if (this.parent)
		// 	this.parent.removeChild(this);
	}

	// destroy(o?: any): void {
	// 	this.mId = null;
	// 	this.mLayer = null;
	// 	this.mScene = null;
	// 	this.isDestroy = true;
	// 	super.destroy(o);
	// 	this.onDestroy();
	// }
	//
	// onDestroy(): void {
	//
	// }

	getDisplay(): Sprite {
		let any  = this;
		// @ts-ignore
		return any;
	}

	setLocation(x: number, y: number): void {
		// if(x.toString() == "NaN")
		// {
		// 	// console.info(" setLocation 异常数据");
		// 	x = 0;
		// }
		// if(y.toString() == "NaN")
		// {
		// 	// console.info(" setLocation 异常数据");
		// 	y = 0;
		// }
		// console.info("x:" + x + " y:" + y);
		this.x = x;
		this.y = y;
	}

	// set x(x:number)
	// {
	//
	// }

	getLocation(): Point {
		this.mLocation.x = this.x;
		this.mLocation.y = this.y;
		return this.mLocation;
	}

	onAdd(): void {

	}

	// showNameTitle(isShow: Boolean, title: string = null): void
	// {
	// 	this.mNameTitle.show(isShow);
	// 	if (title != null)
	// 		this.mNameTitle.setTitle(title);
	// }
	// initNameTitle(titleClazz: any, titleX: number, titleY: number, titleLayer: string = "titleContainer"): void
	// {
	// 	if (this.mNameTitle == null)
	// 	{
	// 		this.mNameTitle = new titleClazz();
	// 		//创建，在现有的容器最顶层
	// 		this.mNameTitle.setContainer(this.getChildContainer(titleLayer, true));
	// 	}
	// 	this.mNameTitle.setLocation(titleX, titleY);
	// }
	onRemove(): void {
	}
};