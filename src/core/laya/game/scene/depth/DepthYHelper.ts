/**
 * @DepthHelper90.as
 *
 * @author sodaChen mail:sujun10@21cn.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame GameEngine
 * <br>Date:2012-10-5
 */

import Sprite = Laya.Sprite;
import BaseDepthHelper from "./BaseDepthHelper";
/**
 * 基于Y坐标的深度管理.
 * @author sodaChen
 *
 */
export default class DepthYHelper extends BaseDepthHelper
{
	constructor(target: Sprite = null)
	{
		super(target);
	}

	update(): void
	{
		//直接对egret的数组进行排序，效率最高，不用再重新添加和删除的过程
		// var array: Sprite[] = this.target.;
		// array.sort(function (display1:Sprite, display2:Sprite) {
		// 	if (display1.y < display2.y) return -1;
		// 	else if (display1.y > display2.y) return 1;
		// 	else return 0;
		// });
	}
}
