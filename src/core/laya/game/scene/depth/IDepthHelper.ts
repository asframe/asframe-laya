/**
* @IDepthManager.as
*
* @author sodaChen mail:sujun10@21cn.com
* @version 1.0
* <br>Copyright (C), 2012 asFrame.com
* <br>This program is protected by copyright laws.
* <br>Program Name:ASFrame GameEngine
* <br>Date:2010-5-21
*/

import Sprite = Laya.Sprite;

/**
 * 深度管理助手
 * @author sodaChen
 * Date:2010-5-21
 */
export default interface IDepthHelper
{
	/**
	 * 开始启动
	 * @param delay
	 */
	start(delay: number): void;
	/**
	 *
	 */
	stop(): void;
	/**
	 *
	 * @param target
	 */
	setTarget(target: Sprite): void;
	/**
	 * 添加
	 * @param child
	 */
	addChild(child: Sprite): void;
	/**
	 * 删除
	 * @param child
	 */
	removeChild(child: Sprite): void;
	/**
	 * 每帧更新
	 */
	update(): void;
	/**
	 * 删除所有
	 */
	removeAll(): void;
}
