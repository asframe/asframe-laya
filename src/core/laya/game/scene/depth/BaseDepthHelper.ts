/**
* @BaseDepthHelper.as
*
* @author sodaChen mail:sujun10@21cn.com
* @version 1.0
* <br>Copyright (C), 2012 ASFrame.com
* <br>This program is protected by copyright laws.
* <br>Program Name:ASFrame GameEngine
* <br>Date:2012-10-5
*/

import Sprite = Laya.Sprite;
import IDepthHelper from "./IDepthHelper";
import App = asf.App;
/**
 *
 * @author sodaChen
 * Date:2012-10-5
 */
export default class BaseDepthHelper implements IDepthHelper
{
	/**
	 * 目标容器
	 */
	protected target: Sprite;
	/**
	 * 排序列表
	 */
	protected list: Array<Sprite>;
	/**
	 * 定时的id
	 */
	protected timeId:number;


	constructor(target: Sprite = null)
	{
		this.target = target;
		this.list = new Array<Sprite>();
	}

	/**
	 * 开启深度排序
	 *
	 * @param {number} delay 毫秒
	 *
	 * @memberOf BaseDepthHelper
	 */
	start(delay: number): void
	{
		this.stop();
		this.timeId = App.timer.doLoop(1000,this.update,this);
	}

	stop(): void
	{
		if(this.timeId)
		{
			App.timer.clearTimer(this.timeId);
		}
	}

	setTarget(target: Sprite): void
	{
		this.target = target;
	}

	addChild(child: Sprite): void
	{
		this.list.push(child);
	}

	removeChild(child: Sprite): void
	{
		asf.ArrayUtils.removeItem(this.list, child);
	}

	removeAll(): void
	{
		this.list = [];
	}

	update(): void
	{
	}
}
