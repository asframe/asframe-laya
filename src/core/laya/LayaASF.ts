/**
 * @.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2021-10-01
 */
import ASF = asf.ASF;

/**
 * 基于Laya框架，对ASFrame框架进行一些扩展，使用Laya框架后， 必须调用这个类的初始化方法
 * 进入舞台后调用这个类的方法
 * @author sodaChen
 * #Date: 2021-10-01
 */
export default class LayaASF
{
    static init():void
    {
        //初始化框架的一些必要参数
        ASF.init();
        ASF.timer.start();
        Laya.timer.frameLoop(1,this,this.tickFrame);
    }

    private static tickFrame(timeStamp: number): boolean
    {
        ASF.timer.onEnterFrame(timeStamp);
        return false;
    }
}