/**
 * @.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2021-10-01
 */
import BaseViewHandler = mvc.BaseViewHandler;
import IRootContainer = mvc.IRootContainer;
import ViewBean = mvc.ViewBean;

/**
 * ASFrame框架的共享对象(ASFrameSession)，一些需要跨框架使用的实例，一般存放在这里。
 * 需要对框架的一些默认实现时，可以覆盖这里的接口对象。
 * 一般可以通过动态设置这里属性，达到调节框架性能的目的。
 * @author sodaChen
 * #Date:
 */
export default class FairyViewHandler extends BaseViewHandler<fairygui.GComponent>
{
    constructor(rootContainer:IRootContainer<fairygui.GComponent>)
    {
        super(rootContainer);
    }

    protected onAddView(viewBean:ViewBean):void
    {
        let container = this.rootContainer.getLayerContainer(viewBean.instance.layer);
        container.addChild(viewBean.instance.container as fairygui.GComponent);
    }

    protected onDelView(viewBean:ViewBean):void
    {
        let container = this.rootContainer.getLayerContainer(viewBean.instance.layer);
        container.removeChild(viewBean.instance.container as fairygui.GComponent);
    }
}