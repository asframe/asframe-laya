/**
 * @IResLoader.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2021/10/01
 */
import UIPackage = fgui.UIPackage;
import IResLoader = mvc.IResLoader;
import ResBean = mvc.ResBean;
import IReser = mvc.IReser;

/**
 * mvc的资源加载器，如果不是使用mvc框架默认的资源加载器时，请重新实现这个接口
 * @author sodaChen
 * Date:2021/10/01
 */
export default class FairyViewLoader implements IResLoader
{
    loadResList(resList:ResBean[],reser:IReser):void
    {
        //目前就只允许配置一个资源，不允许配置多个资源加载
        let resBean = resList[0];
        let method = function(uiPackage:UIPackage)
        {
            console.log("uiPackage:" + uiPackage);
            console.info(resBean.path + "UI资源加载成功:");
            reser.setResList([uiPackage],resList);
        }
        let handler = Laya.Handler.create(this,method);
        fgui.UIPackage.loadPackage("res/ui/" + resBean.path,handler);
    }

    private onLoad(uiPackage:UIPackage)
    {

    }

    destroy(o?: any): void {
    }
}