/**
 * @IResLoader.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2021/10/01
 */
import BaseRootContainer = mvc.BaseRootContainer;
import GComponent = fgui.GComponent;

/**
 * 基于FairyGUI的显示对象实现的mvc框架根容器对象
 * @author sodaChen
 * Date:2021/10/01
 */
export default class FairyRootContainer extends BaseRootContainer<GComponent>
{
    constructor(rootContainer:fairygui.GComponent)
    {
        super(rootContainer);
    }

    /**
     * 创建一个指定层名称的容器对象
     * @param layer
     */
    protected createContainer(): fairygui.GComponent
    {
        let component = new fairygui.GComponent();
        this.rootContainer.addChild(component);
        return component;
    }

    /**
     * 添加child到指定层中
     * @param layer
     * @param child
     */
    addChild(layer: string, child: fairygui.GComponent): void
    {
        this.containerMap.get(layer).addChild(child);
    }

    /**
     * 从指定层中删除child
     * @param layer
     * @param child
     */
    removeChild(layer: string, child: fairygui.GComponent): void
    {
        if (child.parent == this.containerMap.get(layer))
        {
            this.containerMap.get(layer).removeChild(child);
        }
    }

    getContainer(Scene_Layer: string)
    {
        // @ts-ignore
        return this.getLayerContainer(this.sceneLayer);
    }
}