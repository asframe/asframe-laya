;window.asframe = window.asf = {};/**
 * @ASF.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2013-8-22
 */

/**
 * @ASF.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2013-8-22
 */
(function (asf) {
    /**
     * ASFrame框架的共享对象(ASFrameSession)，一些需要跨框架使用的实例，一般存放在这里。
     * 需要对框架的一些默认实现时，可以覆盖这里的接口对象。
     * 一般可以通过动态设置这里属性，达到调节框架性能的目的。
     * @author sodaChen
     * #Date:2013-8-22
     */
    class ASF {
        static init() {
            asf.Global.init();
            this.timer = new asf.TimeMgr();
        }
        /**
         * 获得当前的时间毫秒数
         */
        static currentTimeMillis() {
            return new Date().getTime();
        }
    }
    /**
     * 资源解析的时间，指的是在一帧的时间里，可以用多少时间来进行解析资源（默认是15毫秒）
     * 注意，解析时间消耗多，则会影响其他程序运行，有可能会掉帧。时间少，解析又慢。
     * 可以根据实际情况来动态调节这个值。
     **/
    ASF.resParseTime = 15;
    /**
     * 共享池的引用完毕之后的延迟销毁时间。默认是30秒
     * @see com.asframe.share.SharingPool
     */
    ASF.sharingPoolDelay = 30000;
    /** 是否启动共享缓存策略（一般用于工具开发） **/
    ASF.isResSharing = true;
    /** 是否使用内存缓存二进制字节 **/
    ASF.isCacheBytes = true;
    asf.ASF = ASF;
})(asf);

(function (asf) {
    class App {
        constructor() {
        }
        static init() {
            App.timer = new asf.TimeMgr();
        }
    }
    asf.App = App;
})(asf);
// /**
//  * @Constants.as
//  *
//  * @author sodaChen mail:asframe#163.com
//  * @version 1.0
//  * <br>Copyright (C), 2012 ASFrame.com
//  * <br>This program is protected by copyright laws.
//  * <br>Program Name:ASFrame
//  * <br>Date:2012-6-20
//  */
// namespace asf
// {
// 	/**
// 	 * 框架里自带的常量，供其他应用直接调用
// 	 * @author sodaChen
// 	 * Date:2012-6-20
// 	 */
// 	export class Const
// 	{
// 		//////////////////////数值的最大和最小值////////////////
// 		/** 场景形式存在 **/
// 		static SCENE_CACHE:number = 0;
// 		/** 永久存在 **/
// 		static LONG_CACHE:number = 1;
// 		/** 引用计数的缓存机制 **/
// 		static COUNT_CACHE:number = 2;
// 		/** 构造器的属性名称 **/
// 		static CONSTRUCTOR_NAME:String = "constructor";
// 		/** 对象 **/
// 		static VALUE:Object = {};
// 		/** true **/
// 		static TRUE:boolean = true;
// 		/** false **/
// 		static FALSE:boolean = false;
// 		/** 播放特效 **/
// 		static PLAY_EFFECT:String = "play";
// 		/**
// 		 * 将弧度转换为度时乘以的值。
// 		 */
// 		static RADIANS_TO_DEGREES : number = 180 / Math.PI;
// 		/**
// 		 * 将度转换为弧度时乘以的值
// 		 */
// 		static DEGREES_TO_RADIANS : number = Math.PI / 180;
// 	}
// }
/**
 * @asframe.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2013-6-20
 */

/**
 * @asframe.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2013-6-20
 */
(function (asf) {
    /**
     * 获得当前的时间毫秒数
     */
    function currentTimeMillis() {
        return new Date().getTime();
    }
    asf.currentTimeMillis = currentTimeMillis;
    function addTick(tick) {
    }
    asf.addTick = addTick;
})(asf);

(function (asf) {
    /***
     * Action的相关常量
     */
    class ActionConst {
        constructor() {
        }
    }
    ////////////////////动作类型////////////////////
    /** 队列动作代理动作 */
    ActionConst.QUEUE = "queue";
    /** 普通正常类型,可以多个动作同时存在 **/
    ActionConst.SAME = "same";
    /** 普通正常类型,可以多个动作同时存在 **/
    ActionConst.REPEAT = "repeat";
    /** 该类型动作只允许队列里只存在一个动作 **/
    ActionConst.ALONE = "alone";
    ////////////////////框架指定的依赖注入属性////////////////////
    /** 表演者注入 **/
    ActionConst.INJECT_ACTOR = "actor";
    /** 参数注入 **/
    ActionConst.INJECT_PARAM = "param";
    /** 方法注入 **/
    ActionConst.INJECT_SET_PARAMS = "setParams";
    asf.ActionConst = ActionConst;
})(asf);

(function (asf) {
    /***
     * 表演代理者，支持aciton的队列、并发、同名排斥模式
     */
    class ActorProxies {
        constructor(actor) {
            this._actor = actor;
            this._actionMap = new asf.HashMap();
            this._sameIdMap = new asf.Dictionary();
        }
        setActor(actor) {
            this._actor = actor;
        }
        act(action, callBack = null, thisObj = null, param = null) {
            this._thisObj = thisObj;
            action.initAction(callBack, this._thisObj, param);
            //查看是否有同名的,目前先不处理队列，只处理单独，并列以及替换3种情况
            var act = this._actionMap.get(action.getName());
            if (action.getType() == asf.ActionConst.QUEUE) {
                var queueProxy = null;
                if (act == null) {
                    //创建一个新的队列Action代理
                    queueProxy = new asf.QueueActionProxy();
                    this._actionMap.put(action.getName(), queueProxy);
                    queueProxy.addAction(action);
                    queueProxy.start(this._actor);
                }
                else {
                    queueProxy = act;
                    queueProxy.addAction(action);
                }
                return true;
            }
            if (act != null) {
                //查看是否是独占，已经存在，就不可代替
                if (action.getType() == asf.ActionConst.ALONE) {
                    //直接删除老的
                    this.delAction(act);
                }
                else {
                    //处理并发情况，生成新的索引作为name
                    this._sameIdCount++;
                    this._sameIdMap.put(action, action.getName() + this._sameIdCount);
                    this._actionMap.put(action.getName() + this._sameIdCount, action);
                }
            }
            else {
                //单独排斥和第一次并发就直接添加
                this._actionMap.put(action.getName(), action);
            }
            action.start(this._actor);
            return true;
        }
        delAction(action, isDestory = true) {
            if (!action)
                return;
            //如果action是队列，并且不是队列代理本身销毁
            if (action.getType() == asf.ActionConst.QUEUE && action instanceof asf.QueueActionProxy) {
                action.actAction();
                return;
            }
            //并发类型，并且是存放了并发索引新生成key的对象
            if (action.getType() == asf.ActionConst.SAME && this._sameIdMap.hasKey(action)) {
                this._actionMap.remove(this._sameIdMap.get(action));
            }
            else {
                this._actionMap.remove(action.getName());
            }
            //执行销毁)
            if (isDestory)
                action.destroy();
        }
    }
    asf.ActorProxies = ActorProxies;
})(asf);

(function (asf) {
    /***
     * 表演代理者，支持aciton的队列、并发、同名排斥模式
     */
    class BasicAction {
        constructor() {
            this.mType = asf.ActionConst.SAME;
            this.mName = BasicAction.NAME;
        }
        /**
         * 初始化方法
         * @param callBack 销毁前的回调函数
         * @param param 参数
         *
         */
        initAction(callBack, thisObj, param = null) {
            this.mCallBack = callBack;
            this.mThisObj = thisObj;
            this.mParam = param;
        }
        setName(name) {
            this.mName = name;
        }
        setType(type) {
            this.mType = type;
        }
        getType() {
            return this.mType;
        }
        getName() {
            return this.mName;
        }
        /**
         * 开始执行具体的动作逻辑，BasicAction做了一些基础处理，一般不建议子类重写。如果重写了，请调用super.start();
         *
         * @param actor 动作的服务对象(表演者)
         */
        start(actor) {
            if (this.mIsStart)
                throw new Error("重复调用start了" + this);
            this.isDestory = false;
            this.mIsStart = true;
            this.mActor = actor;
            //拥有公共的actor属性，则可以直接给子类赋值。这里有可能报错,其实用Type可以做出精准分析的
            if (this.hasOwnProperty(asf.ActionConst.INJECT_ACTOR)) {
                this[asf.ActionConst.INJECT_ACTOR] = actor;
            }
            if (this.hasOwnProperty(asf.ActionConst.INJECT_PARAM)) {
                this[asf.ActionConst.INJECT_PARAM] = this.mParam;
            }
            else if (this.hasOwnProperty(asf.ActionConst.INJECT_SET_PARAMS)) {
                this[asf.ActionConst.INJECT_PARAM].call(this, this.mParam);
            }
            this.onStart();
        }
        /**
         * 子类必须重写该方法
         */
        onStart() {
        }
        destroy(o = null) {
            //防止重复销毁引起错误
            if (this.isDestory)
                return;
            this.isDestory = true;
            this.mIsStart = false;
            //手动自己从actor中清除自己
            this.mActor.delAction(this, false);
            this.onDestory();
            if (this.mCallBack) {
                if (this.mCallBack.length == 1)
                    this.mCallBack.call(this.mThisObj, this.mActor);
                else
                    this.mCallBack.call(this.mThisObj);
                this.mCallBack = null;
            }
            this.mActor = null;
        }
        onDestory() {
        }
    }
    BasicAction.NAME = "BasicAction";
    asf.BasicAction = BasicAction;
})(asf);

(function (asf) {
    /**
     * 队列动作代理器，代理所有类型为ActionConstants.QUEUE类型的动作,进行队列播放<br>
     * 当代理器里的所有动作都播放完时，则该代理器从动作管理器中删除
     * 队列机制一般不建议使用，不太好掌控，因为是个action列表，很容易导致列表阻塞，缓存过多
     * @author soda.C
     */
    class QueueActionProxy extends asf.BasicAction {
        constructor() {
            super();
            this.actions = [];
            this.mType = asf.ActionConst.QUEUE;
            this.mName = "QueueProxyAction";
        }
        destroy(o = null) {
            if (this.currAction != null) {
                this.overAction(this.currAction);
                this.currAction = null;
            }
            if (this.actions != null && this.actions.length > 0) {
                for (var i = 0; i < this.actions.length; i++) {
                    this.overAction(this.actions[i]);
                }
            }
            this.actions = null;
        }
        onStart() {
            //直接选择一个action进行处理
            this.actAction();
        }
        /**
         * 结束一个action，同时会调用action的overAction方法
         * @param action
         *
         */
        overAction(action) {
            if (action) {
                action.destroy();
            }
        }
        /**
         * 添加一个新的action
         * @param action
         *
         */
        addAction(action) {
            this.actions.push(action);
        }
        /**
         * 选择一个新的action来处理
         */
        actAction() {
            if (this.actions.length == 0) {
                this.destroy();
            }
            //更新新的动作
            this.currAction = this.actions.shift();
            if (!this.currAction) {
                //出现空的，继续选择
                this.actAction();
            }
            else {
                this.currAction.start(this.mActor);
            }
        }
    }
    asf.QueueActionProxy = QueueActionProxy;
})(asf);

(function (asf) {
    /***
     * 基于帧的心跳机制的动作对象，具备每帧跳动的
     */
    class TickAction extends asf.BasicAction {
        constructor() {
            super();
            this.timerId = -1;
        }
        start(actor) {
            super.start(actor);
            // this.timerId = App.timer.doFrameLoop(1, this.tick, this, this.timerId);
        }
        destroy(o = null) {
            asf.App.timer.clearTimer(this.timerId);
            if (this.isDestory)
                return;
            super.destroy(o);
        }
        tick() {
            return false;
        }
    }
    asf.TickAction = TickAction;
})(asf);
/**
 * @RgbColor.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame Color
 * <br>Date:2012-9-6
 */

/**
 * @RgbColor.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame Color
 * <br>Date:2012-9-6
 */
(function (asf) {
    /**
     *
     * @author sodaChen
     * Date:2012-9-6
     */
    class RgbColor {
        /**
         * 构造
         */
        constructor(red = 0, green = 0, blue = 0) {
            /**
             * 红色
             */
            this.red = 0;
            /**
             * 绿色
             */
            this.green = 0;
            /**
             * 蓝色
             */
            this.blue = 0;
            this.red = red;
            this.green = green;
            this.blue = blue;
        }
        /**
         * 获取3通道16进制值
         * @return
         */
        getHex24() {
            return ((this.red << 16) | (this.green << 8) | this.blue);
        }
        clone() {
            return new RgbColor(this.red, this.green, this.blue);
        }
        /**
         * @return
         */
        toString() {
            return "red:" + this.red + "  " +
                "green:" + this.green + "  " +
                "blue:" + this.blue;
        }
    }
    asf.RgbColor = RgbColor;
})(asf);
/**
 * @RgbaColor.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame Color
 * <br>Date:2012-9-6
 */

/**
 * @RgbaColor.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame Color
 * <br>Date:2012-9-6
 */
(function (asf) {
    /**
     *
     * @author sodaChen
     * Date:2012-9-6
     */
    class RgbaColor extends asf.RgbColor {
        constructor(red = 0, green = 0, blue = 0, alpha = 1) {
            super(red, green, blue);
            /** 透明通道 **/
            this.alpha = 1;
            this.alpha = alpha;
        }
        /**
         * 获取4通道16进制值
         * @return
         */
        getHex32() {
            var hex24 = this.getHex24();
            var c = hex24 / 0x10000000;
            return (hex24 & 0xffffff) + Math.floor(this.alpha * c) * 0x10000000;
        }
        /**
         * 应用一个alpha值 , 并返回一个32位通道值
         * @param	a
         * @return
         */
        applyAlpha(alpha) {
            this.alpha = alpha;
            return this.getHex32();
        }
        /**
         * 克隆
         * @return
         */
        clone() {
            return new RgbaColor(this.red, this.green, this.blue, this.alpha);
        }
        /**
         * 输出文本
         * @return
         */
        toString() {
            return "red:" + this.red + "  " +
                "green:" + this.green + "  " +
                "blue:" + this.blue + "  " +
                "alpha:" + this.alpha;
        }
    }
    asf.RgbaColor = RgbaColor;
})(asf);
/**
 * @CallBack.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/6
 */

/**
 * @CallBack.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/6
 */
(function (asf) {
    /**
     * 回调函数对象，主要是增加了对this对象和参数的引用
     * @author sodaChen
     */
    class CallBack {
        /**
         *
         * @param fun 回调函数
         * @param thisObj 调函数需要的执行环境对象
         * @param param 执行回调函数时需要返回的参数。如果有参数。会是回调函数的第一个参数
         */
        constructor(fun = null, thisObj = null, param) {
            if (fun != null)
                this.init(fun, thisObj, param);
        }
        /**
         * 通过对象池获得或者创建CallBack对象
         * @param fun 函数
         * @param thisObj this对象
         * @param param 可选参数z
         * @param autoRelease 是否执行完execute就主动放回对象池
         * @returns {CallBack} 实例
         */
        static create(fun, thisObj, param, autoRelease = false) {
            if (this.pool == null)
                this.pool = new asf.Pool(CallBack, 100);
            let callBack = this.pool.create();
            callBack.autoRelease = autoRelease;
            callBack.inPool = false;
            callBack.init(fun, thisObj, param);
            return callBack;
        }
        /**
         * 释放对象
         * @param callBack
         */
        static release(callBack) {
            //return;
            callBack.destroy();
            callBack.inPool = true;
            callBack.autoRelease = false;
            this.pool.release(callBack);
        }
        /**
         * 初始化，如果放入了对象池，可以使用这个方法来重新初始化
         * @param fun 回调函数
         * @param thisObj 调函数需要的执行环境对象
         * @param param 执行回调函数时需要返回的参数
         */
        init(fun, thisObj, param) {
            this.fun = fun;
            this.param = param;
            this.thisObj = thisObj;
        }
        /**
         * 执行回调函数
         * @param args 不定参数，这里输入的参数必须和回调函数保持相同的顺序
         */
        execute(...args) {
            if (!this.fun) {
                return;
            }
            if (!this.fun)
                return;
            // var a: any[] = [];
            if (this.param != null) {
                //附带的参数加在最前面
                args.unshift(this.param);
            }
            let value = this.fun.apply(this.thisObj, args);
            //进行回调
            if (this.autoRelease) {
                if (this.inPool)
                    throw new Error("需要多次调用CallBack的execute方法的，不能设置为自动释放autoRelease");
                this.inPool = true;
                CallBack.release(this);
            }
            else if (this.autoClean) {
                this.passivate();
            }
            return value;
        }
        passivate() {
            this.fun = null;
            this.param = null;
            this.thisObj = null;
        }
        destroy() {
            this.passivate();
            this.autoRelease = false;
            this.autoClean = false;
        }
    }
    asf.CallBack = CallBack;
})(asf);
/**
 * @ASFrame.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012-present ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2012-1-7
 */

/**
 * @ASFrame.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012-present ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2012-1-7
 */
(function (asf) {
    /**
     * @private
     * 哈希计数，全局使用，外部不可更改
     */
    asf.$hashCount = 1;
    /**
     * ASFrame框架的顶级对象。框架内所有对象的基类，为对象实例提供唯一的hashCode值。
     */
    class ASFObject {
        /**
         * 创建一个 HashObject 对象
         */
        constructor() {
            //生成这个值，主要是为了让对象具有全局唯一的属性
            this.hashCode = asf.$hashCount++;
        }
        clone() {
            return new ASFObject();
        }
    }
    asf.ASFObject = ASFObject;
})(asf);
/**
 * @Assert.as
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2012-1-7
 */

/**
 * @Assert.as
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2012-1-7
 */
(function (asf) {
    /**
     * 断言对象
     * @author sodaChen
     * Date:2012-1-7
     */
    class Assert {
        /**
         * 断言值是为<code>true</code>.
         * <pre class="code">Assert.isTrue(value, "该表达式必须是为真");</pre>
         * @param expression 一个布尔值表达式
         * @param message 当表达式不对时给出的提示信息
         * @param isThrowError 是否抛出异常，默认为false。传true时抛出异常
         */
        static isTrue(expression, message = "") {
            if (!expression) {
                this.isThrowIllegalArgumentError(message, "[Assert.isTrue] - 该表达式必须是为真。");
            }
        }
        /**
         * 断言该对象不为 <code>null</code>.
         * <pre class="code">Assert.notNull(value, "参数不能为空");</pre>
         * @param object 检测对象
         * @param message 错误时的提示信息
         * @param isThrowError 是否抛出异常，默认为false。传true时抛出异常
         * @throws com.asframe.lang.error.IllegalArgumentError 如果表达式的值为<code>null</code>
         */
        static notNull(object, message = "") {
            if (!object) {
                this.isThrowIllegalArgumentError(message, "[Assert.notNull] - 参数不能为空。");
            }
        }
        /**
         * 断言该对象是属于type类
         * <pre class="code">Assert.isInstanceOf(value, type, "vlaue必须type类或者他的子类");</pre>
         * @param object 检测对象
         * @param message 错误时的提示信息
         * @param isThrowError 是否抛出异常，默认为false。传true时抛出异常
         * @throws com.asframe.lang.error.IllegalArgumentError 如果value不属于type时抛出
         */
        static isInstanceOf(object, type, message = "") {
            // if(!(object is type))
            // {
            // 	isThrowIllegalArgumentError(message,"[Assert.isInstanceOf] - vlaue必须type类或者他的子类。");
            // }
        }
        /**
         * 检测对象是否为抽象类，抽象是不能进行实例化的
         * @param object
         * @param clazz
         *
         */
        static isAbstractClass(object, clazz) {
            // if (getQualifiedClassName(object) == clazz)
            // {
            // 	throw new AbstractClassError(clazz + "是抽象类，不能实例化!");
            // }
        }
        /**
         * 是否索引越界了，小于等0或者i大于等于len
         * @param i
         * @param len
         * @param message
         *
         */
        static isIndexOutOfBounds(i, len, message = "") {
            if (i < 0 || i >= len) {
                throw new Error(message + "越界了，传入的是:" + i + "，实际长度是:" + len);
            }
        }
        static isThrowIllegalArgumentError(message, defMessage) {
            if (!message || message.length == 0) {
                message = defMessage;
            }
            throw new Error(message);
        }
    }
    asf.Assert = Assert;
})(asf);
/**
 * @Global.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2016-5-29
 */

/**
 * @Global.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2016-5-29
 */
(function (asf) {
    /**
     * 全局对象，用于存放全局使用的东西，主要是提供一些公共的数据，以及做防止垃圾回收的措施
     * 游戏中最好不要直接在这里增加内容，这里维护的是属于框架通用的属性
     * @author sodaChen
     * Date:2016-5-29
     */
    class Global {
        static addRef(target) {
            this.refMap.put(target, target);
        }
        static removeRef(target) {
            this.refMap.remove(target);
        }
        static hasValue(key) {
            return this.dataMap.hasKey(key);
        }
        static addValue(key, value) {
            this.dataMap.put(key, value);
        }
        static getValue(key) {
            return this.dataMap.get(key);
        }
        static removeValue(key) {
            return this.dataMap.remove(key);
        }
        static init() {
            this.refMap = new asf.HashMap();
            this.dataMap = new asf.HashMap();
        }
        /**
         * 创建一个object的any属性对象
         */
        static createAny() {
            return {};
        }
    }
    asf.Global = Global;
})(asf);
/**
 * Created by sodaChen on 2017/3/1.
 */

/**
 * Created by sodaChen on 2017/3/1.
 */
(function (asf) {
    /**
     * 输出信息扩展类
     * @author sodaChen
     * Date:2017/2/15
     */
    class ConsolePlus {
        static init(isPlusMode = false) {
            this.funList = [];
            this.funPulsList = [];
            this.emptyFun = function () { };
            //存放原来的方法
            for (var i = 0; i < this.funNames.length; i++) {
                this.funList.push(console[this.funNames[i]]);
            }
            //生成新的扩展方法
            for (var i = 0; i < this.funNames.length; i++) {
                this.createPlusFun(this.funList[i], this.funNames[i]);
            }
            this.setConsoleMode(isPlusMode);
        }
        /**
         * 设置日志等级，所有大于等于这个日志级别的输出都可以正常输出，小于的则不能进行输出
         * @param level
         */
        static setLevel(level) {
            this.$level = level;
            this.openConsole();
        }
        /**
         * 设置输出模式
         * @param isPlusMode 是否为扩展模式
         */
        static setConsoleMode(isPlusMode) {
            this.isPlusMode = isPlusMode;
            if (isPlusMode)
                this.openConsolePlus();
            else
                this.openConsole();
        }
        /**
         * 打开日志
         */
        static openConsole() {
            this.closeConsole();
            //扩展默认，则调用扩展方法
            if (this.isPlusMode) {
                this.openConsolePlus();
                return;
            }
            for (var i = this.$level; i < this.funNames.length; i++) {
                //还原成最初的方法
                console[this.funNames[i]] = this.funList[i];
            }
        }
        /**
         * 关闭所有的日志输出
         */
        static closeConsole() {
            //设置回最初的方法
            for (var i = 0; i < this.funNames.length; i++) {
                //定义个空方法屏蔽掉
                console[this.funNames[i]] = this.emptyFun;
            }
        }
        /**
         * 开启日志输出的扩展模式
         */
        static openConsolePlus() {
            this.isPlusMode = true;
            //扩展console的所有方法
            for (var i = this.$level; i < this.funNames.length; i++) {
                console[this.funNames[i]] = this.funPulsList[i];
            }
        }
        /**
         * 关闭日志输出的扩展模式
         */
        static closeConsolePlus() {
            this.isPlusMode = false;
            //还原成原来的方法
            this.openConsole();
        }
        /**
         * 创建扩展函数
         * @param oldFun 原有的函数
         * @param funName 函数名称
         */
        static createPlusFun(oldFun, funName) {
            var fun = function (message, ...optionalParams) {
                console.group("[" + funName + "][" + new Date() + "]");
                oldFun.apply(console, arguments);
                console.groupEnd();
            };
            this.funPulsList.push(fun);
        }
    }
    /////////////////日志级别////////////////
    /** 调试级别，trace函数路径 **/
    ConsolePlus.TRACE = 0;
    /** 普通日志级别 **/
    ConsolePlus.DEBUG = 1;
    /** 普通日志级别 **/
    ConsolePlus.LOG = 2;
    /** 信息日志级别 console.info() **/
    ConsolePlus.INFO = 3;
    /** 警告日志级别 console.warn() **/
    ConsolePlus.WARN = 4;
    /** 错误日志级别 console.error() **/
    ConsolePlus.ERROR = 5;
    /** 默认是最初的日志级别，改变这个值。可以改变日志输出的结果 **/
    ConsolePlus.$level = ConsolePlus.LOG;
    /** 需要屏蔽覆盖的方法名 **/
    ConsolePlus.funNames = ["trace", "debug", "log", "info", "warn", "error"];
    asf.ConsolePlus = ConsolePlus;
})(asf);
/**
 * @Level.as.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-9-4
 */

/**
 * @Level.as.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-9-4
 */
(function (asf) {
    /**
     * 日志级别对象
     * @author sodaChen
     * Date:2011-9-4
     */
    class Level {
        constructor(level, name, color) {
            this.level = level;
            this.name = name;
            this.color = color;
        }
        toString() {
            return "[level=" + this.level + " levelName=" + this.name + "]";
        }
    }
    Level.TRACE = new Level(1, "trace", "#000000");
    Level.DEBUG = new Level(10, "debug", "#000000");
    Level.LOG = new Level(100, "log", "#0000FF");
    Level.INFO = new Level(1000, "info", "#fffa55");
    Level.WARN = new Level(10000, "warn", "#FF9900");
    Level.ERROR = new Level(100000, "error", "#FF0000");
    asf.Level = Level;
})(asf);
/**
 * @Logger.as.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-8-12
 */

/**
 * @Logger.as.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-8-12
 */
(function (asf) {
    /**
     * Log使用对象,最主要和最基础的使用对象，一般情况下都是使用这个来进行日志的输入和输出。
     * 带有默认的输出渠道，实际项目使用者。可以动态修改日志的输出格式
     * @author sodaChen
     * Date:2011-8-12
     */
    class Log {
        /**
         * 输出调试信息
         * @param target:日志输出的源头
         * @param arg0:输出参数
         * @param arg1:输出参数
         * @param arg2:输出参数
         *
         */
        static debug(...logs) {
            this.formatLog(asf.Level.DEBUG, logs);
        }
        /**
         * 输出提示信息
         * @param msg
         *
         */
        static info(...logs) {
            this.formatLog(asf.Level.INFO, logs);
        }
        /**
         * 输出提示信息
         * @param msg
         *
         */
        static log(...logs) {
            this.formatLog(asf.Level.LOG, logs);
        }
        /**
         * 输出警告的信息
         * @param target:日志输出的源头
         * @param arg0:输出参数
         * @param arg1:输出参数
         * @param arg2:输出参数
         *
         */
        static warn(...logs) {
            this.formatLog(asf.Level.WARN, logs);
        }
        /**
         * 输出错误的信息
         * @param	msg:需要输出信息的对象
         */
        static error(...logs) {
            this.formatLog(asf.Level.ERROR, logs);
        }
        static formatLog(logLevel, logs) {
            //当前等级小于或者等于日志输出等级
            if (this.isOpen && this.level.level <= logLevel.level) {
                var msg = "";
                for (var i = 0; i < logs.length; i++) {
                    if (msg == "")
                        msg = msg + logs[i];
                    else
                        msg = msg + "," + logs[i];
                }
                console[logLevel.name]("[" + logLevel.name + "]: " + msg);
            }
        }
        /**
         * 设置日志等级
         * @param level:日志等级描述对象
         *
         */
        static setLevel(level) {
            Log.level = level;
        }
    }
    /** 是否开启日志系统 **/
    Log.isOpen = true;
    /** 默认是开启调试模式的 **/
    Log.level = asf.Level.DEBUG;
    asf.Log = Log;
})(asf);
/**
 * @Logger.as.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-8-12
 */

/**
 * @Logger.as.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-8-12
 */
(function (asf) {
    /**
     * Log使用对象,最主要和最基础的使用对象，一般情况下都是使用这个来进行日志的输入和输出。
     * 带有默认的输出渠道，实际项目使用者。可以动态修改日志的输出格式
     * @author sodaChen
     * Date:2011-8-12
     */
    class LogFactory {
        /**
         * 输出调试信息
         * @param target:日志输出的源头
         * @param arg0:输出参数
         * @param arg1:输出参数
         * @param arg2:输出参数
         *
         */
        static debug(...logs) {
            this.formatLog(asf.Level.DEBUG, logs);
        }
        /**
         * 输出提示信息
         * @param msg
         *
         */
        static info(...logs) {
            this.formatLog(asf.Level.INFO, logs);
        }
        /**
         * 输出提示信息
         * @param msg
         *
         */
        static log(...logs) {
            this.formatLog(asf.Level.LOG, logs);
        }
        /**
         * 输出警告的信息
         * @param target:日志输出的源头
         * @param arg0:输出参数
         * @param arg1:输出参数
         * @param arg2:输出参数
         *
         */
        static warn(...logs) {
            this.formatLog(asf.Level.WARN, logs);
        }
        /**
         * 输出错误的信息
         * @param	msg:需要输出信息的对象
         */
        static error(...logs) {
            this.formatLog(asf.Level.ERROR, logs);
        }
        static formatLog(logLevel, logs) {
            //当前等级小于或者等于日志输出等级
            if (this.isOpen && this.level.level <= logLevel.level) {
                for (var i = 0; i < logs.length; i++)
                    console[logLevel.name](logs[i]);
            }
        }
        /**
         * 设置日志等级
         * @param level:日志等级描述对象
         *
         */
        static setLevel(level) {
            asf.Log.level = level;
        }
    }
    /** 是否开启日志系统 **/
    LogFactory.isOpen = true;
    /** 默认是开启调试模式的 **/
    LogFactory.level = asf.Level.LOG;
    asf.LogFactory = LogFactory;
})(asf);
/**
 * @Logger.as.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-8-12
 */

/**
 * @Logger.as.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2011-8-12
 */
(function (asf) {
    /**
     * Log使用对象,最主要和最基础的使用对象，一般情况下都是使用这个来进行日志的输入和输出。
     * 带有默认的输出渠道，实际项目使用者。可以动态修改日志的输出格式
     * @author sodaChen
     * Date:2011-8-12
     */
    class Logger {
        constructor(path) {
            if ((typeof path) == "string")
                this.logHead = path;
            else {
                //构造函数，转
            }
        }
        /**
         * 输出调试信息
         * @param target:日志输出的源头
         * @param arg0:输出参数
         * @param arg1:输出参数
         * @param arg2:输出参数
         *
         */
        debug(...logs) {
            this.formatLog(asf.Level.DEBUG, logs);
        }
        /**
         * 输出提示信息
         * @param msg
         *
         */
        info(...logs) {
            this.formatLog(asf.Level.INFO, logs);
        }
        /**
         * 输出提示信息
         * @param msg
         *
         */
        log(...logs) {
            this.formatLog(asf.Level.LOG, logs);
        }
        /**
         * 输出警告的信息
         * @param target:日志输出的源头
         * @param arg0:输出参数
         * @param arg1:输出参数
         * @param arg2:输出参数
         *
         */
        warn(...logs) {
            this.formatLog(asf.Level.WARN, logs);
        }
        /**
         * 输出错误的信息
         * @param	msg:需要输出信息的对象
         */
        error(...logs) {
            this.formatLog(asf.Level.ERROR, logs);
        }
        formatLog(logLevel, logs) {
            if (asf.Log.isOpen && asf.Log.level.level <= logLevel.level) {
                var msg = "";
                for (var i = 0; i < logs.length; i++) {
                    if (msg == "")
                        msg = msg + logs[i];
                    else
                        msg = msg + "," + logs[i];
                }
                console[logLevel.name]("[" + logLevel.name + "]" + this.logHead + ":" + msg);
            }
        }
        /**
         * 设置日志等级
         * @param level:日志等级描述对象
         *
         */
        setLevel(level) {
            asf.Log.level = level;
        }
    }
    asf.Logger = Logger;
})(asf);
/**
 * @Dictionary.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/19
 */

/**
 * @Dictionary.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/19
 */
(function (asf) {
    /**
     * 字典对象，支持key和value的绑定，支持任意对象作为key。内部使用了Array来实现
     * @author sodaChen
     * Date:2017/1/19
     */
    class Dictionary extends asf.ASFObject {
        constructor() {
            super();
            this.clear();
        }
        put(k, v) {
            var index = this.$keys.indexOf(k);
            if (index == -1) {
                this.$keys.push(k);
                this.$values.push(v);
            }
            else {
                this.$values[index] = v;
            }
        }
        get(k) {
            var index = this.$keys.indexOf(k);
            if (index == -1) {
                return null;
            }
            return this.$values[index];
        }
        remove(k) {
            var index = this.$keys.indexOf(k);
            if (index == -1) {
                return null;
            }
            var v = this.get(k);
            this.$keys.splice(index, 1);
            this.$values.splice(index, 1);
            return v;
        }
        indexOf(key) {
            return this.$keys.indexOf(key);
        }
        /**
         * 如果此映射未包含键-值映射关系，则返回 true.
         * @return 如果此映射未包含键-值映射关系，则返回 true.
         */
        isEmpty() {
            return this.$keys.length == 0;
        }
        /**
         * 如果此映射包含指定键的映射关系，则返回 true.
         * @param key  测试在此映射中是否存在的键.
         * @return 如果此映射包含指定键的映射关系，则返回 true.
         */
        hasKey(key) {
            return this.$keys.indexOf(key) != -1;
        }
        /**
         * 如果该映射将一个或多个键映射到指定值，则返回 true.
         * @param key 测试在该映射中是否存在的值
         * @return 如果该映射将一个或多个键映射到指定值，则返回 true.
         */
        hasValue(value) {
            return this.$values.indexOf(value) != -1;
        }
        size() {
            return this.$keys.length;
        }
        keys(copy = true) {
            if (copy)
                return this.$keys.concat();
            return this.$keys;
        }
        values(copy = true) {
            if (copy)
                return this.$values.concat();
            return this.$values;
        }
        forEach(fun, thisObj) {
            let values = this.$values;
            let len = values.length;
            for (let i = 0; i < len; i++) {
                fun.call(thisObj, values[i]);
            }
        }
        forKeyValue(fun, thisObj) {
            let values = this.$values;
            let keys = this.$keys;
            let len = values.length;
            for (let i = 0; i < len; i++) {
                fun.call(thisObj, keys[i], values[i]);
            }
        }
        /**
         * @inheritDoc
         */
        getContainer() {
            return this.$values;
        }
        clear() {
            this.$keys = [];
            this.$values = [];
        }
        destroy(o) {
            var len = this.$values.length;
            for (var i = 0; i < len; i++) {
                asf.DestroyUtils.destroy(this.$values[i]);
            }
            delete this.$keys;
            delete this.$values;
        }
    }
    asf.Dictionary = Dictionary;
})(asf);
/**
 * @HashMap.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012-present ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2012-1-7
 */

/**
 * @HashMap.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012-present ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2012-1-7
 */
(function (asf) {
    /**
     * 集合对象
     * @author sodaChen
     * Date:2012-1-7
     */
    class HashMap extends asf.ASFObject {
        constructor() {
            super();
            this.clear();
        }
        /**
         * 获取源
         */
        getSource() {
            return this.obj;
        }
        /**
         * 将指定的值与此映射中的指定键相关联.
         * @param key 与指定值相关联的键.
         * @param value 与指定键相关联的值.
         */
        put(key, value) {
            if (this.obj[key] == null) {
                this._length++;
            }
            this.obj[key] = value;
            // this._length++;
        }
        /**
         * 返回此映射中映射到指定键的值.
         * @param key 与指定值相关联的键.
         * @return 此映射中映射到指定值的键，如果此映射不包含该键的映射关系，则返回 null.
         */
        get(key) {
            return this.obj[key];
        }
        /**
         * 从此映射中移除所有映射关系
         */
        clear() {
            this.obj = new Object();
            this._length = 0;
        }
        /**
         * 如果存在此键的映射关系，则将其从映射中移除
         * @param key 从映射中移除其映射关系的键
         * @return 以前与指定键相关联的值，如果没有该键的映射关系，则返回 null
         */
        remove(key) {
            var temp = this.obj[key];
            if (temp != null && temp != undefined) {
                delete this.obj[key];
                this._length--;
            }
            return temp;
        }
        /**
         * 返回此映射中的键-值映射关系数.
         * @return 此映射中的键-值映射关系的个数.
         */
        size() {
            return this._length;
        }
        /**
         * 如果此映射未包含键-值映射关系，则返回 true.
         * @return 如果此映射未包含键-值映射关系，则返回 true.
         */
        isEmpty() {
            return this._length == 0;
        }
        /**
         * 如果此映射包含指定键的映射关系，则返回 true.
         * @param key  测试在此映射中是否存在的键.
         * @return 如果此映射包含指定键的映射关系，则返回 true.
         */
        hasKey(key) {
            return this.obj[key] != null;
        }
        /**
         * 如果该映射将一个或多个键映射到指定值，则返回 true.
         * @param key 测试在该映射中是否存在的值
         * @return 如果该映射将一个或多个键映射到指定值，则返回 true.
         */
        hasValue(value) {
            for (var key in this.obj) {
                if (this.obj[key] == value) {
                    return true;
                }
            }
            return false;
        }
        /**
         * 返回此映射中包含的所有值.
         * @return 包含所有值的数组
         */
        values() {
            var ary = [];
            if (this._length != 0) {
                for (var key in this.obj) {
                    ary.push(this.obj[key]);
                }
                return ary;
            }
            return ary;
        }
        /**
         * 返回此映射中包含的所有key值.
         * @return 包含所有key的数组
         */
        keys() {
            var ary = [];
            if (this._length != 0) {
                for (var key in this.obj) {
                    ary.push(key);
                }
                return ary;
            }
            return ary;
        }
        /**
         * @inheritDoc
         */
        getContainer() {
            return this.obj;
        }
        forEach(fun, thisObj) {
            for (var key in this.obj) {
                fun.call(thisObj, this.obj[key]);
            }
        }
        forKeyValue(fun, thisObj) {
            for (var key in this.obj) {
                fun.call(thisObj, key, this.obj[key]);
            }
        }
        destroy(o) {
            for (var key in this.obj) {
                asf.DestroyUtils.destroy(this.obj[key]);
            }
            this.obj = null;
        }
    }
    asf.HashMap = HashMap;
})(asf);
// /**
//  * @SharingPool.as
//  *
//  * @author sodaChen mail:asframe@qq.com
//  * @version 1.0
//  * <br>Copyright (C), 2013 ASFrame.com
//  * <br>This program is protected by copyright laws.
//  * <br>Program Name:ASFrame
//  * <br>Date:2013-8-22
//  */
// namespace asf
// {
// 	export class ReferData
// 	{
// 		timeId: number = 0;
// 		key: string;
// 		target: Object;
// 		refCount: number;
// 		destroy(): void
// 		{
// 			delete this.key;
// 			delete this.target;
// 			this.refCount = 0;
// 			this.timeId = 0;
// 		}
// 	}
// 	/**
// 	 * 共享对象池，提供引用次数的计算
// 	 * @author sodaChen
// 	 * #Date:2013-8-22
// 	 */
// 	export class RefMgr
// 	{
// 		// private static instance: RefMgr;
// 		/** 通过key引用target记录 **/
// 		private static refMap: Dictionary<string, ReferData>;
// 		/** 通过target本身作为引用，来保存target记录 **/
// 		private static targetMap: Dictionary<Object, ReferData>;
// 		/** 对象 **/
// 		private static pool: IPool<any>;
// 		/** 是否正在清除状态中 **/
// 		private static isClear: boolean;
// 		// /**
// 		//  * 获取实例的静态方法实例
// 		//  * @return
// 		//  *
// 		//  */
// 		// static getInstance(): RefMgr
// 		// {
// 		// 	if (this.instance == null)
// 		// 	{
// 		// 		this.instance = new RefMgr();
// 		// 	}
// 		// 	return this.instance;
// 		// }
// 		static init()
// 		{
// 			this.refMap = new Dictionary<string, ReferData>();
// 			this.targetMap = new Dictionary<Object, ReferData>();
// 			PoolMgr.regClass(ReferData);
// 			this.pool = PoolMgr.getPool(ReferData);
// 		}
// 		/**
// 		 * 添加一个引用共享对象
// 		 * @param key 唯一key
// 		 * @param target 共享对象
// 		 * @param isRefer 是否马上增加一次引用
// 		 *
// 		 */
// 		static addRefer(key: string, target: Object, isRefer: boolean = true): void
// 		{
// 			var referData: ReferData = this.refMap.get(key);
// 			if (referData)
// 			{
// 				throw new Error(key + "重复添加Refer");
// 			}
// 			if (!target)
// 			{
// 				throw new Error("target为空");
// 			}
// 			referData = this.pool.create();
// 			this.refMap.put(key, referData);
// 			this.targetMap.put(target, referData);
// 			referData.key = key;
// 			referData.target = target;
// 			if (isRefer)
// 			{
// 				referData.refCount++;
// 			}
// 		}
// 		/**
// 		 * 指定的key是否存在共享对象的引用
// 		 * @param key 唯一
// 		 * @return true则表示存在，否则不存在
// 		 *
// 		 */
// 		static hasRefer(key: string): boolean
// 		{
// 			if (!key)
// 				return false;
// 			return this.refMap[key] != null;
// 		}
// 		/**
// 		 * 指定key取得一个公共引用对象（对象的引用次数会增加1） 如果没有对象则会抛出异常
// 		 * @param key 唯一字符串
// 		 * @return 公共对象
// 		 */
// 		static refer(keyTarget: string | Object): any
// 		{
// 			var referData: ReferData;
// 			//先判断key
// 			if (typeof keyTarget === "string")
// 				referData = this.targetMap.get(keyTarget);
// 			else
// 				referData = this.targetMap.get(keyTarget);
// 			if (!referData)
// 			{
// 				throw new Error(keyTarget + "没有对应的Refer，请先添加");
// 			}
// 			referData.refCount++;
// 			if (referData.timeId != 0)
// 			{
// 				//清除原来的定时器
// 				referData.timeId = 0;
// 			}
// 			return referData.target;
// 		}
// 		/**
// 		 * 清除所有的共享缓存资源
// 		 * @param isDestory 清除的同时销毁缓存的资源，默认是销毁
// 		 *
// 		 */
// 		static clearAll(isDestory: boolean = true): void
// 		{
// 			if (isDestory)
// 			{
// 				this.isClear = true;
// 				this.refMap.forEach(this.forEachRef, this);
// 				this.isClear = false;
// 				//再次尝试释放target
// 				this.targetMap.forEach(this.forEachRef, this);
// 			}
// 			this.refMap = new Dictionary<string, ReferData>();
// 			this.targetMap = new Dictionary<Object, ReferData>();
// 		}
// 		private static forEachRef(refer: ReferData): void
// 		{
// 			if (this.isClear)
// 			{
// 				//同时删除targe的标签
// 				this.targetMap.remove(refer.target);
// 				DestroyUtils.destroy(refer.target);
// 			}
// 			refer.destroy();
// 			//放回对象池
// 			this.pool.release(refer);
// 		}
// 		/**
// 		 * 取消一次公共对象的引用
// 		 * @param keyOrTarget 唯一key或者共享对象实例
// 		 * @param isDestroy 立刻销毁对象，否则是引用次数为0时30(可配置设定)秒后销毁
// 		 *
// 		 */
// 		static unrefer(keyTarget: string | Object, isDestroy: boolean = false): void
// 		{
// 			//优先从key取值
// 			var referData: ReferData;
// 			//先判断key
// 			if (typeof keyTarget === "string")
// 				referData = this.targetMap.get(keyTarget);
// 			else
// 				referData = this.targetMap.get(keyTarget);
// 			//空则不处理
// 			if (!referData)
// 			{
// 				return;
// 			}
// 			referData.refCount--;
// 			if (referData.refCount <= 0)
// 			{
// 				if (isDestroy)
// 					//强制销毁，没有延迟
// 					this.recover(referData);
// 				else
// 					//ASF.sharingPoolDelay后清除资源
// 					referData.timeId = App.timer.doOnce(ASF.sharingPoolDelay, this.recover, this, [referData], referData.timeId);
// 			}
// 		}
// 		private static recover(referData: ReferData): void
// 		{
// 			// clearTimeout(referData.timeId);
// 			App.timer.clearTimer(referData.timeId);
// 			//防止延迟销毁期间又被引用
// 			if (referData.refCount > 0) return;
// 			//资源进行销毁
// 			this.refMap.remove(referData.key);
// 			this.targetMap.remove(referData.target)
// 			//判断是否销毁实例
// 			var target: Object = referData.target;
// 			DestroyUtils.destroy(target);
// 			referData.destroy();
// 			//放回对象池
// 			this.pool.release(referData);
// 		}
// 	}
// }
/**
 * @Singleton.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2010-2016 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-10-29
 */

/**
 * @Singleton.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2010-2016 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-10-29
 */
(function (asf) {
    /**
     * 所有单例的基类，做了单例的基础检查。所有子类最好都写一个getInstance的静态方法来获取
     * @author sodaChen
     * Date:2012-10-29
     */
    class Singleton {
        constructor() {
            var clazz = clazz = asf.ClassUtils.forInstance(this);
            //为空时，表示浏览器不支持这样读取构造函数
            if (!clazz)
                return;
            // 防止重复实例化
            if (Singleton.classMap.hasKey(clazz))
                throw new Error(this + " 只允许实例化一次！");
            else
                Singleton.classMap.put(clazz, this);
        }
        // 注意，Singleton是要替换成你自己实现的子类 这里没有实际的作用
        // private static instance:Singleton;
        // /**
        //  * 获取实例的静态方法实例
        //  * @return
        //  *
        //  */
        // public static getInstance():Singleton
        // {
        // 	if(this.instance == null)
        // 	{
        // 		this.instance = new Singleton();
        // 	}
        // 	return this.instance;
        // }
        /**
         * 销毁方法。事实上单例是很少进行销毁的
         */
        destroy(o = null) {
            Singleton.classMap.remove(this["constructor"]);
            this.onDestroy();
        }
        onDestroy() {
        }
        /**
         * 删除单例的实例（不对单例本身做任何的销毁，只是删除他的引用）
         * @param clazz 单例的Class对象
         *
         */
        static removeInstance(clazz) {
            this.classMap.remove(clazz);
        }
        /**
         * 获取单例类，若不存在则创建.所有的单例创建的时候，都必须使用这个方法来创建，这样可以做到统一管理单例
         * @param clazz	任意需要实现单例效果的类
         * @return
         *
         */
        static getInstanceOrCreate(clazz) {
            var obj = this.classMap.get(clazz);
            if (!obj) {
                obj = new clazz();
                //不是Singleton的子类，则手动添加Singleton构造器会自动添加到classMap
                if (!(obj instanceof Singleton))
                    this.classMap.put(clazz, obj);
            }
            return obj;
        }
    }
    /** 存放初始化过的构造函数 **/
    Singleton.classMap = new asf.Dictionary();
    asf.Singleton = Singleton;
})(asf);
/**
 * @NoticeData.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/23
 */

/**
 * @NoticeData.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/23
 */
(function (asf) {
    class NoticeData {
        constructor(listener, thisObj, isOnce = false) {
            this.listener = listener;
            this.thisObj = thisObj;
            this.isOnce = isOnce;
        }
    }
    asf.NoticeData = NoticeData;
})(asf);
/**
 * @NoticeList.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2017/6/5
 */

/**
 * @NoticeList.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2017/6/5
 */
(function (asf) {
    /**
     * 一个主题的通知列表,主要是为了确保每个不同的通知可以互相独立出来，根据isNotice属性
     * @author sodaChen
     * Date:2017/6/5
     */
    class NoticeList {
        constructor() {
            this.listeners = [];
            // this.onceList = [];
        }
    }
    asf.NoticeList = NoticeList;
})(asf);
/**
 * @Subjects.as
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-7
 */

/**
 * @Subjects.as
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-7
 */
(function (asf) {
    /**
     * 观察者主题，单一主题
     * @author sodaChen
     * #Date:2016-9-7
     */
    class Subject {
        constructor(notice) {
            this.listenerList = new Array();
            this.noticeStr = notice;
        }
        addNoticeData(listener, thisObj, isOnce) {
            if (thisObj == null)
                throw new Error("thisObj不能为空");
            var listeners = this.listenerList;
            var len = listeners.length;
            for (var i = 0; i < len; i++) {
                if (listeners[i].listener == listener)
                    return;
            }
            //添加新的
            listeners.push(new asf.NoticeData(listener, thisObj, isOnce));
        }
        getObserverFun(observer) {
            var listener = observer.update;
            if (observer.hasOwnProperty(this.noticeStr)
                && observer[this.noticeStr] instanceof Function)
                listener = observer[this.noticeStr];
            return listener;
        }
        /**
         * 设置一个通知主题
         * @param notice
         */
        setNotice(notice) {
            this.noticeStr = notice;
        }
        notice(params) {
            var listeners = this.listenerList;
            //函数注册的调用
            var length;
            var data;
            if (listeners != null && (length = listeners.length) > 0) {
                for (var i = 0; i < length; i++) {
                    data = listeners[i];
                    data.listener.apply(data.thisObj, params);
                    if (data.isOnce) {
                        //即时删除
                        listeners.splice(i, 1);
                        i--;
                        length--;
                    }
                }
            }
        }
        /**
         * 发送一起通知
         * @param notice 通知
         * @param params 通知附带的参数
         *
         */
        send(...args) {
            this.notice(args);
        }
        /**
         * 添加一个观察者通知
         * @param notice 通知名称
         * @param listener 通知监听函数
         *
         */
        on(listener, thisObj) {
            this.addNoticeData(listener, thisObj, false);
        }
        onObserver(observer, isOnce) {
            this.addNoticeData(this.getObserverFun(observer), observer, isOnce);
        }
        /**
         * 删除一个观察者通知
         * @param notice 通知名称
         * @param listener 删除指定监听函数，为空则表示删除这个通知下的所有监听函数
         *
         */
        off(listener) {
            var listeners = this.listenerList;
            if (listeners == null)
                return;
            var len = listeners.length;
            for (var i = 0; i < len; i++) {
                if (listeners[i].listener == listener) {
                    listeners.splice(i, 1);
                    return;
                }
            }
        }
        offObserver(observer) {
            this.off(this.getObserverFun(observer));
        }
        once(listener, thisObj) {
            this.addNoticeData(listener, thisObj, true);
        }
    }
    asf.Subject = Subject;
})(asf);
/**
 * @Subjects.as
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-7
 */

/**
 * @Subjects.as
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-7
 */
(function (asf) {
    /**
     * 观察者主题，这里是集中多种主题
     * @author sodaChen
     * #Date:2016-9-7
     */
    class Subjects {
        constructor() {
            this.listenerMap = new asf.HashMap();
            // //注册对象池,100灵活使用的对象，实际项目中会有大部分事件是一直监听不会删除的
            // this.listPool = asf.PoolMgr.regClass(NoticeList,100);
            // this.noticePool = asf.PoolMgr.regClass(NoticeData,500);
            // this.noticingMap = new ObjectMap<string | number, string | number>();
        }
        addNoticeData(notice, listener, thisObj, isOnce) {
            if (thisObj == null)
                throw new Error("thisObj不能为空");
            let noticeList = this.listenerMap.get(notice);
            if (!noticeList) {
                //可以考虑采用对象池技术
                // noticeList = this.listPool.create();
                noticeList = new asf.NoticeList();
                this.listenerMap.put(notice, noticeList);
            }
            let listeners = noticeList.listeners;
            let len = listeners.length;
            //去掉已经注册过的函数
            for (let i = 0; i < len; i++) {
                //必须监听方法和this相等才能决定是同一个监听
                if (listeners[i].listener == listener && listeners[i].thisObj == thisObj)
                    return null;
            }
            //是否正在遍历中，如果正在遍历的是这个事件，则进行列表复制
            if (noticeList.isNoticing) {
                //重新赋值
                noticeList.listeners = listeners = listeners.concat();
            }
            //添加新的
            // let noticeData:NoticeData = new NoticeData(listener, thisObj, isOnce);
            let noticeData = new asf.NoticeData(listener, thisObj, isOnce);
            listeners.push(noticeData);
            //判断是否一次的，只跑一次的预先添加到一次列表中
            if (isOnce) {
                if (!noticeList.onceList)
                    noticeList.onceList = [];
                noticeList.onceList.push(noticeData);
            }
            return noticeData;
        }
        /**
         * 删除一个观察者通知
         * @param notice 通知名称
         * @param listener 删除指定监听函数，为空则表示删除这个通知下的所有监听函数
         *
         */
        off(notice, listener, thisObj) {
            let noticeList = this.listenerMap.get(notice);
            if (!noticeList)
                return;
            let listeners = noticeList.listeners;
            //是否正在遍历中，如果正在遍历的是这个事件，则进行列表复制
            if (noticeList.isNoticing) {
                //重新赋值
                noticeList.listeners = listeners = listeners.concat();
            }
            let len = listeners.length;
            let data;
            for (let i = 0; i < len; i++) {
                data = listeners[i];
                if (data.listener == listener && data.thisObj == thisObj) {
                    listeners.splice(i, 1);
                    //因为添加的时候保证不重复了，所以找到删除之后就立马退出
                    return;
                }
            }
        }
        getObserverFun(notice, observer) {
            let pro = String(notice);
            //默认是接口的通用接受通知的方法
            let listener = observer.update;
            if (observer.hasOwnProperty(pro) && observer[pro] instanceof Function)
                listener = observer[pro];
            return listener;
        }
        notice(notice, params) {
            let noticeList = this.listenerMap.get(notice);
            if (!noticeList)
                return;
            let listeners = noticeList.listeners;
            //函数注册的调用
            let length = listeners.length;
            //做个标记，防止外部修改原始数组导致遍历错误。这里不直接调用list.concat()因为dispatch()方法调用通常比on()等方法频繁。
            noticeList.isNoticing = true;
            let data;
            for (let i = 0; i < length; i++) {
                data = listeners[i];
                //这里如果data.listener.apply有操作这个主题，则有可能会出现data不存在的情况
                data.listener.apply(data.thisObj, params);
            }
            noticeList.isNoticing = false;
            if (noticeList.onceList) {
                let onceList = noticeList.onceList;
                while (onceList.length) {
                    data = onceList.pop();
                    this.off(notice, data.listener, data.thisObj);
                }
                delete noticeList.onceList; //del之后就会变为undiend
            }
            //没有监听者，则进行删除
            if (listeners.length == 0)
                this.listenerMap.remove(notice);
        }
        /**
         * 发送一起通知
         * @param notice 通知
         * @param args 不定参数
         *
         */
        send(notice, ...args) {
            this.notice(notice, args);
        }
        /**
         * 添加一个观察者通知
         * @param notice 通知名称
         * @param listener 通知监听函数
         *
         */
        on(notice, listener, thisObj) {
            return this.addNoticeData(notice, listener, thisObj, false);
        }
        onObserver(notice, observer, isOnce) {
            return this.addNoticeData(notice, this.getObserverFun(notice, observer), observer, isOnce);
        }
        /**
         * 清除多个监听，不传thisObj时，表示清除所有的监听，否则只清除thisObj方法的on
         * @param thisObj
         */
        offs(thisObj) {
            this.listenerMap.forKeyValue(function (notice, datas) {
                let len = datas.length;
                for (let i = 0; i < len; i++) {
                    if (datas[i].thisObj == thisObj)
                        //从中删除
                        this.off(notice, datas[i].listener, datas[i].thisObj);
                }
            }, this);
        }
        offObserver(notice, observer) {
            this.off(notice, this.getObserverFun(notice, observer), observer);
        }
        once(notice, listener, thisObj) {
            return this.addNoticeData(notice, listener, thisObj, true);
        }
    }
    asf.Subjects = Subjects;
})(asf);
/**
 * @Context.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2010-2016 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-5-25
 */
var mvc;
/**
 * @Context.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2010-2016 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-5-25
 */
(function (mvc) {
    var Subjects = asf.Subjects;
    /**
     * DoEasy控制中心，上下文，负责管理所有的Bean、Module和View、Helper
     * @author sodaChen
     * Date:2016-5-25
     */
    class Context {
        /**
         * 初始化mvc框架的Context容器
         */
        static init() {
            this.moduleBeanMap = new asf.HashMap();
            this.viewBeanMap = new asf.HashMap();
            this.subjects = new Subjects();
            mvc.MvcReg.init(this.moduleBeanMap, this.viewBeanMap);
        }
        /**
         * 初始化所有的模块
         */
        static initModule() {
        }
        /**
         * 所有的模块拉取服务端的数据
         */
        static takeServer() {
            this.moduleBeanMap.forEach(function (bean) {
                if (bean.instance && !bean.instance.isTakeServer) {
                    bean.instance.isTakeServer = true;
                    try {
                        bean.instance.takeServer();
                    }
                    catch (e) {
                        console.error(bean.name + "takeServer报错:", e);
                    }
                }
            }, this);
        }
        /**
         * 释放view的方法法的事件，内部函数
         * @param view
         */
        static $destroyViewEvent(view) {
            let bean = this.viewBeanMap.get(view.name);
            if (!bean)
                return;
            // bean.isOpen = false;
            // asf.Assert.notNull(bean, view.name + "没有配置成ViewBean");
            bean.instance = null;
            //所有的heplerbean也进行处理
            if (bean.controllerBeans) {
                for (let i = 0; i < bean.controllerBeans.length; i++) {
                    //这里有可能需要做一些策略性的处理，目前纯粹设置为空(销毁在view的那里已经调用销毁方法了)
                    bean.controllerBeans[i].instance = null;
                }
            }
        }
        /**
         * 根据模块名称或者class构造函数获取到模块的实例
         * @param module 名称或者class构造函数
         * @returns {IModule} 模块的实例
         */
        static $getModule(module) {
            return this.moduleBeanMap.get(module).instance;
        }
        static $getViewBean(view) {
            return this.viewBeanMap.get(view);
        }
        /**
         * 打开一个view界面。必须是通过注册的view才能使用这个方法。不然会抛出异常
         * @param view 唯一ID、名字和构造函数之一
         * @param callBack view完成打开之后回调
         * @param thisObj this对象，如果callBack
         * @param param 打开view时传入view的open方法的参数
         */
        static $openView(view, param) {
            let bean = this.viewBeanMap.get(view);
            if (!bean)
                throw new Error(view + "openView方法必须通过MvcReg进行注册");
            //如果正在loading中，则不进行任何——操作
            if (bean.isLoading)
                return;
            //目前所有的模块都是一开始初始化的，所以暂时不判断模块是否已经初始化了
            if (!bean.instance) {
                // bean.isOpen = true;
                //重新实例化
                new mvc.CreateView(true, bean, null, param);
                return;
            }
            bean.openView(param);
        }
        /**
         * 获取一个view
         *
         * @static
         * @param {(number | string | Function)} view
         * @param {*} [param]
         * @returns {ViewBean}
         *
         * @memberOf Context
         */
        static $getView(view) {
            let bean = this.viewBeanMap.get(view);
            if (bean == null) {
                return null;
            }
            return bean.instance;
        }
        /**
         * 关闭View对象
         * @param view view 唯一ID、名字和构造函数之一
         * @param isDestroy 是否销毁界面，默认是false
         */
        static $closeView(view) {
            let bean = this.viewBeanMap.get(view.name);
            if (bean) {
                this.$viewHandler.delView(bean);
                return;
            }
            console.error("closeView关闭失败" + view + " name:" + view.name);
            throw new Error(view + "Context.$closeView方法必须通过MvcReg进行注册");
        }
        /**
         * mvc框架内部添加一个view实例，并且显示出来(调用view的open方法)
         */
        static $addView(view) {
            let bean = this.viewBeanMap.get(view.name);
            if (!bean) {
                //没有bean对象，表示没有走mvc框架的模块注册,第一次，自动生成一个ViewBean对象来存放
                bean = new mvc.ViewBean(view, mvc.MvcUtils.createConfig(view, mvc.ViewConfig));
                bean.name = view.name;
                this.viewBeanMap.put(view.name, bean);
            }
            bean.instance = view;
            this.$viewHandler.addView(bean);
        }
        /**
         * 移除掉view实例，从显示列表中删除
         * @param view
         */
        static $removeView(view, module) {
            let bean = this.viewBeanMap.get(view.name);
            if (!bean) {
                console.error(view.name + "$removeView删除失败");
                return;
            }
            this.$viewHandler.delView(bean);
        }
        /**
         * 直接通过框架启动一个View，可以配置view以及相关helper
         * @param view
         * @param viewConfig
         * @param helpers
         */
        static startView(view, viewConfig, ...controllers) {
            let name = mvc.MvcUtils.getClassName(view);
            let viewBean = Context.$getViewBean(name);
            //如果已经存在，不在进行打开处理
            if (!viewBean) {
                //自动进行注册
                viewBean = mvc.MvcReg.regController(view, controllers);
            }
            //实例存在
            if (viewBean.instance) {
                //没打开，进行打开
                if (!viewBean.instance.isOpen)
                    viewBean.instance.open();
            }
            else {
                //立刻进行初始化view
                new mvc.CreateView(true, viewBean);
            }
        }
        /**
         * 启动整个框架，会负责初始化
         * @param complete 完成启动后的回调函数
         */
        static startup() {
            console.info("启动mvc框架");
            //遍历所有的模块，然后对需要立刻初始化的进行初始化处理
            let moduleBean;
            let initModule = new mvc.CreateModule();
            let moduleAry = this.moduleBeanMap.values();
            for (let i = 0; i < moduleAry.length; i++) {
                moduleBean = moduleAry[i];
                //有可能有模块进行了单独初始化了
                if (!moduleBean.instance) {
                    //表示已经在初始化了，主要是防止一个module里的两个view都是需要立刻初始化的
                    initModule.initModule(moduleBean);
                    this.moduleBeanMap.put(moduleBean.name, moduleBean);
                }
            }
        }
    }
    mvc.Context = Context;
})(mvc || (mvc = {}));
/**
 * Created by soda on 2017/1/26.
 */
var mvc;
/**
 * Created by soda on 2017/1/26.
 */
(function (mvc) {
    class MvcConst {
    }
    /////////////一些特定必须拥有的字段///////////////
    /**
     * 基于mvc模块的view以及module以及model，controller必须有用的静态名字
     */
    MvcConst.Class_Name = "Name";
    /**
     * 类的唯一ID
     */
    MvcConst.Class_Id = "Id";
    /////////////mvc view、module、heper的独立事件，只有监听具体的对象才能接受到这些方法//////////
    /** 界面初始化完毕抛出的事件 **/
    MvcConst.View_Complete = "viewComplete";
    /** view打开事件，参数是view的name或者id **/
    MvcConst.View_Open = "viewOpen";
    /** view事关闭件，参数是view的name或者id **/
    MvcConst.View_Close = "viewClose";
    /** view销毁事件，参数是view的name或者id **/
    MvcConst.View_Destory = "viewDestory";
    ///////////////////////关于的view事件通知///////////////////
    /** 打开view事件 **/
    MvcConst.Open_View = "openView";
    /** 关闭view事件 **/
    MvcConst.Close_View = "openView";
    /** 销毁view事件 **/
    MvcConst.Destory_View = "openView";
    /**
     * 背景层
     * @type {string}
     */
    MvcConst.Bg_Layer = "Bg_Layer";
    /**
     * 场景层
     */
    MvcConst.Scene_Layer = "Scene_Layer";
    /**
     * 界面背景层
     */
    MvcConst.View_Bg_Layer = "View_Bg_Layer";
    /**
     * 主界面层
     */
    MvcConst.Main_Layer = "Main_Layer";
    /**
     * 界面的底层层
     */
    MvcConst.View_Bottom_Layer = "View_Bottom_Layer";
    /**
     * 界面层
     */
    MvcConst.View_Layer = "View_Layer";
    /**
     * 界面顶层
     */
    MvcConst.View_Top_Layer = "View_Top_Layer";
    /**
     * 提示层
     */
    MvcConst.Tip_Layer = "Tip_Layer";
    /**
     * 最顶层
     */
    MvcConst.Top_Layer = "Top_Layer";
    /////////////View的级别。1级，2级，3级，-1是没有级别
    /** 不受排斥的界面级别 **/
    MvcConst.ViewLv = -1;
    /**
     * 目前设定的最大界面层级
     */
    MvcConst.View_Lv_Max = 3;
    /**
     * 主界面层，唯一的一层，天生只能存在一个
     */
    MvcConst.View_Lv_Main = 0;
    /**
     * 1层，一般是UI层
     */
    MvcConst.View_Lv_1 = 1;
    /**
     * 一般是UI之上的2级UI层
     */
    MvcConst.View_Lv_2 = 2;
    /**
     * 一般提示，警告层使用
     */
    MvcConst.View_Lv_3 = 3;
    mvc.MvcConst = MvcConst;
})(mvc || (mvc = {}));
/**
 * Created by sodaChen on 2017/3/14.
 */
var mvc;
/**
 * Created by sodaChen on 2017/3/14.
 */
(function (mvc) {
    /**
     * 注册mvc框架的类
     * @author soda.C
     * Date:2008-1-10
     */
    class MvcReg {
        static init(moduleBeanMap, viewBeanMap) {
            this.moduleBeanMap = moduleBeanMap;
            this.viewBeanMap = viewBeanMap;
        }
        /**
         * 同时注册模块、View、Helper
         * @param module 模块构造函数
         * @param view view构造函数
         * @param config view的配置
         * @param helpers 多Helper的构造函数
         * @returns {ModuleConfig} 模块的配置类
         */
        static reg(module, viewClass, dbClass, ...controllers) {
            //注册模块
            this.regModule(module, dbClass);
            //注册view
            this.regView(viewClass, dbClass, module);
            for (let i = 0; i < controllers.length; i++) {
                this.regViewController(viewClass, controllers[i]);
            }
        }
        /**
         * 删除注册的模块，同时会删除相关的view和heper
         * @param module
         * @param view
         */
        static del(module) {
            let name = mvc.MvcUtils.getClassName(module);
            let bean = this.moduleBeanMap.remove(name);
            if (bean) {
                //如果有view，把所有的view给删除掉
                if (bean.viewBeans.length > 0) {
                    for (let i = 0; i < bean.viewBeans.length; i++) {
                        this.delView(bean.viewBeans[i].clazz);
                    }
                }
                if (bean.instance) {
                    bean.instance.destroy();
                    bean.instance = null;
                }
                //进行多重删除
                this.moduleBeanMap.remove(name);
            }
        }
        /**
         * 删除注册的view以及模块
         * @param view
         */
        static delView(view) {
            let name = mvc.MvcUtils.getClassName(view);
            let bean = this.viewBeanMap.remove(name);
            if (bean) {
                //先清除相关的helper
                if (bean.controllerBeans && bean.controllerBeans.length > 0) {
                    for (let i = 0; i < bean.controllerBeans.length; i++) {
                        if (bean.controllerBeans[i].instance) {
                            bean.controllerBeans[i].instance.destroy();
                            bean.controllerBeans[i].instance = null;
                        }
                    }
                }
                if (bean.instance) {
                    bean.instance.destroy();
                    bean.instance = null;
                }
            }
        }
        /**
         * 只注册Module和多个View的绑定关系
         * @param clazz 模块的构造函数
         * @param config 可选，模块的配置
         * @param name 可选，模块的名字
         */
        static regModule(clazz, dbClass, ...views) {
            if (!clazz)
                throw new Error("注册module的时候，构造函数不能为空");
            //先判断是否已经注册了模块了
            let name = mvc.MvcUtils.getClassName(clazz);
            let moduleBean = this.moduleBeanMap.get(name);
            if (!moduleBean) {
                moduleBean = new mvc.ModuleBean(clazz);
                moduleBean.dbClass = dbClass;
                moduleBean.name = name;
                //name和clazz都存放
                this.moduleBeanMap.put(name, moduleBean);
            }
            if (views && views.length > 0) {
                for (let i = 0; i < views.length; i++) {
                    //注册view
                    this.regView(views[i], dbClass, clazz);
                }
            }
            return moduleBean;
        }
        /**
         * 注册单个View的信息
         * @param clazz view的构造函数
         * @param config view的配置信息
         * @param module 可选，module的构造函数
         * @param viewName 可选，view的名字
         */
        static regView(clazz, dbClass, module) {
            //检测view是否已经注册了
            let name = mvc.MvcUtils.getClassName(clazz);
            let viewBean = this.viewBeanMap.get(name);
            if (!viewBean) {
                //生成一个默认的View配置对象.目前config几乎是没用
                let config = new mvc.ViewConfig();
                viewBean = new mvc.ViewBean(clazz, config);
                viewBean.name = name;
                viewBean.dbClass = dbClass;
                this.viewBeanMap.put(name, viewBean);
            }
            else if (dbClass) {
                //有可能二次注册的时候有db需要放进来
                viewBean.dbClass = dbClass;
            }
            //绑定view和module的关系
            if (module) {
                let moduleBean = this.regModule(module, dbClass);
                //绑定相互之间的关系
                moduleBean.viewBeans.push(viewBean);
                viewBean.moduleBean = moduleBean;
            }
            return viewBean;
        }
        /**
         * 注册跟指定view绑定的Controller对象
         * @param view
         * @param clazz
         * @param config
         */
        static regViewController(view, ...controllers) {
            return this.regController(view, controllers);
        }
        static regController(view, controllers) {
            let viewBean = this.regView(view);
            if (controllers && controllers.length > 0) {
                if (!viewBean.controllerBeans) {
                    viewBean.controllerBeans = [];
                }
                for (let i = 0; i < controllers.length; i++) {
                    viewBean.controllerBeans.push(new mvc.ControllerBean(controllers[i]));
                }
            }
            return viewBean;
        }
    }
    mvc.MvcReg = MvcReg;
})(mvc || (mvc = {}));
/**
 * @MvcUtils.ts
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/13
 */
var mvc;
/**
 * @MvcUtils.ts
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/13
 */
(function (mvc) {
    /**
     * mvc框架的工具方法集合,主要是处理view、controller、module公共的处理方法
     * @author sodaChen
     * Date:2017/2/13
     */
    class MvcUtils {
        /**
         * 获得框架指定的静态属性名字
         * @param clazz
         */
        static getClassName(clazz) {
            if (clazz.hasOwnProperty(mvc.MvcConst.Class_Name)) {
                let name = clazz[mvc.MvcConst.Class_Name];
                if (name == null || name == "") {
                    throw new Error(clazz + "必须对Name进行赋值");
                }
                return name;
            }
            throw new Error(clazz + "没有对应的静态属性Name，必须设置改制");
        }
        // /**
        //  * 注册的时候添加固有的ID和NAME
        //  * @param map 存放bean的集合
        //  * @param clazz 构造函数实例
        //  * @param bean bean结构
        //  */
        // static regIdName(map:asf.IMap<any,any>,clazz:Function,bean:BaseBean<any>):void
        // {
        //     //检测固定静态名字"NAME"，并且存放起来
        //     if(clazz.hasOwnProperty(MvcConst.Class_Name))
        //     {
        //         //名字自动变为首字母小写
        //         if(map)
        //         {
        //             //如果名字相同，抛出异常，应该是重复注册，或者别的模块名了一样的名字
        //             if(map.hasKey(bean.name))
        //                 throw new Error(bean.name + "已经注册过，出现相同名字！" + clazz);
        //             map.put(bean.name,bean);
        //         }
        //     }
        //     if(clazz.hasOwnProperty(MvcConst.Class_Id))
        //     {
        //         bean.id = clazz["ID"];
        //         if(map)
        //         {
        //             if(map.hasKey(bean.id))
        //             {
        //                 throw new Error(map.get(bean.id)["name"] + "已经注册过，出现相同ID！" + clazz + " class2:" + clazz);
        //             }
        //             map.put(bean.id,bean);
        //         }
        //     }
        // }
        /**
         * 设置mvc核心实例的id和name
         * @param core view、controller、module之一的实例
         * @param bean 对应的bean结构
         */
        static setIdName(core, bean) {
            //以配置bean的数据为中心设置
            if (bean.name && bean.name != "")
                core.name = bean.name;
            if (bean.id && bean.id != 0)
                core.id = bean.id;
        }
        /**
         * 创建通用的核心对象
         * @param bean
         */
        static createCore(bean) {
            let core = new bean.clazz();
            core.name = bean.name;
            bean.instance = core;
            return core;
        }
        /**
         * 两个对象互相自动注入属性
         * @param core1
         * @param core2
         */
        static eachInject(core1, core2) {
            if (core1.hasOwnProperty(core2.name))
                core1[core2.name] = core2;
            if (core2.hasOwnProperty(core1.name))
                core2[core1.name] = core1;
        }
        /**
         * 根据实例创建对应的配置文件对象
         * @param core 核心对象
         * @param confClass 配置类的构造函数
         * @returns {any} 配置实例
         */
        static createConfig(core, confClass) {
            var config;
            //取得实例自带的配置对象
            var tempC = core.config;
            if (tempC) {
                //直接是配置对象本身
                if (tempC instanceof confClass)
                    config = tempC;
                else {
                    //创建新实例，并复制实例的配置参数
                    config = new confClass();
                    MvcUtils.copyConfig(core, config);
                }
            }
            return config;
        }
        static copyConfig(core, config) {
            let tempC = core.config;
            if (tempC) {
                for (let name in tempC) {
                    //重置ViewConfig的属性值
                    config[name] = tempC[name];
                }
            }
        }
    }
    mvc.MvcUtils = MvcUtils;
})(mvc || (mvc = {}));
/**
 * Created by sodaChen on 2017/3/14.
 */
var mvc;
/**
 * Created by sodaChen on 2017/3/14.
 */
(function (mvc) {
    /**
     * 初始化mvc框架，传入必要的mvc内部使用到的对象
     * @param viewHandler
     * @param resLoaderClass
     * @param moduleMgrClass
     * @param dbClass
     * @param holdView
     */
    function init(viewHandler, resLoaderClass, moduleMgrClass, dbClass, holdView) {
        mvc.Context.$holdupView = holdView;
        mvc.Context.$viewHandler = viewHandler;
        mvc.Context.$resLoaderClass = resLoaderClass;
        mvc.Context.$moduleMgrClass = moduleMgrClass;
        mvc.Context.$dbClass = dbClass;
        mvc.Context.init();
        mvc_subjects = mvc.Context.subjects;
    }
    mvc.init = init;
    /**
     * 简化打开view的方法
     * @param {number | string | Function} view
     * @param param
     * @param {boolean} isEnforce
     */
    function open(view, param) {
        mvc.Context.$openView(view, param);
    }
    mvc.open = open;
    /**
     * 销毁界面
     * @param view 界面的标识
     */
    function destroy(view) {
        this.closeView(view, true);
    }
    mvc.destroy = destroy;
    /**
     * 关闭指定等级的view
     * @param lv
     */
    function closeByLv(lv = -1) {
        mvc.Context.$viewHandler.closeViewByLv(lv);
    }
    mvc.closeByLv = closeByLv;
    /**
     * 关闭View对象
     * @param view view 唯一ID、名字和构造函数之一
     * @param isDestroy 是否销毁界面，默认是false
     */
    function close(view, isDestroy = false) {
        let bean = mvc.Context.$getViewBean(view);
        if (!bean)
            return;
        // throw new Error(view + "mvc.closeView方法必须通过MvcReg进行注册");
        if (!bean.instance) {
            //实例不存在,不需要关闭
            return;
        }
        //如果还没有初始化完毕,则直接发出销毁事件
        if (isDestroy && !bean.instance.isOpen) {
            mvc.Context.$destroyViewEvent(bean.instance);
            return;
        }
        if (isDestroy)
            bean.instance.destroy();
        else
            bean.instance.close();
    }
    mvc.close = close;
    /**
     * 获得一个view
     *
     * @export
     * @param {(number | string | Function)} view
     * @param {*} [param]
     */
    function getView(view) {
        return mvc.Context.$getView(view);
    }
    mvc.getView = getView;
    /** 判断当前曾经是否有view */
    function hasViewByLayer(layer) {
        return mvc.Context.$viewHandler.hasViewByLayer(layer);
    }
    mvc.hasViewByLayer = hasViewByLayer;
    /**
    * 根据模块名称或者class构造函数获取到模块的实例
    * @param module 名称或者class构造函数
    * @returns {IModule} 模块的实例
    */
    function getModule(module) {
        return mvc.Context.$getModule(module);
    }
    mvc.getModule = getModule;
    /**
     * 发送一个mvc框架的全局通知
     * @param notice 通知名称
     * @param param 参数数组
     */
    function notice(notice, param) {
        mvc_subjects.notice(notice, param);
    }
    mvc.notice = notice;
    /**
     * 发送一个mvc框架的全局通知
     * @param notice 通知主题
     * @param args 相关参数
     */
    function send(notice, ...args) {
        mvc_subjects.notice(notice, args);
    }
    mvc.send = send;
    /**
     * 添加一个观察者通知
     * @param notice 通知名称
     * @param listener 通知监听函数
     *
     */
    function on(notice, listener, thisObj) {
        return mvc_subjects.on(notice, listener, thisObj);
    }
    mvc.on = on;
    /**
     * 监听主题，只监听一次
     * @param notice 通知
     * @param listener 监听函数
     * @param thisObj listener的类对象
     */
    function once(notice, listener, thisObj) {
        return mvc_subjects.once(notice, listener, thisObj);
    }
    mvc.once = once;
    /**
     * 删除一个观察者通知
     * @param notice 通知名称
     * @param listener 删除指定监听函数
     *
     */
    function off(notice, listener, thisObj) {
        mvc_subjects.off(notice, listener, thisObj);
    }
    mvc.off = off;
    /**
     * 移除对象的监听事件
     * @param thisObj
     */
    function offs(thisObj) {
        mvc_subjects.offs(thisObj);
    }
    mvc.offs = offs;
})(mvc || (mvc = {}));
/**
 * @BaseBean.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-28
 */
var mvc;
/**
 * @BaseBean.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-28
 */
(function (mvc) {
    /**
     * 基础的存放结构
     * @author sodaChen
     * Date:2015-6-28
     */
    class BaseBean {
        constructor(clazz) {
            this.clazz = clazz;
        }
        get instance() {
            return this._instance;
        }
        set instance(value) {
            this._instance = value;
        }
    }
    mvc.BaseBean = BaseBean;
})(mvc || (mvc = {}));
/**
 * @BasicConfig.as
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-8
 */
var mvc;
/**
 * @BasicConfig.as
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-8
 */
(function (mvc) {
    /**
     * 基础配置信息，包含view、helper以及moduleads
     */
    class BaseConfig {
        constructor() {
            /** 唯一ID **/
            this.id = 0;
            /** 是否延迟初始化 **/
            this.lazy = true;
            /** 是否一直保持实例化（即不被销毁） **/
            this.keep = false;
            /**
             *是否销毁
             */
            this.destroy = true;
        }
    }
    mvc.BaseConfig = BaseConfig;
})(mvc || (mvc = {}));
/**
 * @MediatorBean.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:AStruts2
 * <br>Date:2012-4-9
 */
var mvc;
/**
 * @MediatorBean.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:AStruts2
 * <br>Date:2012-4-9
 */
(function (mvc) {
    /**
     * HelperBean的相关配置信息
     * @author sodaChen
     * Date:2012-4-9
     */
    class ControllerBean extends mvc.BaseBean {
        constructor(clazz) {
            super(clazz);
        }
    }
    mvc.ControllerBean = ControllerBean;
})(mvc || (mvc = {}));
/**
 * Created by soda on 2017/1/27.
 */
var mvc;
/**
 * Created by soda on 2017/1/27.
 */
(function (mvc) {
    class ModuleBean extends mvc.BaseBean {
        constructor(clazz) {
            super(clazz);
            this.viewBeans = [];
        }
    }
    mvc.ModuleBean = ModuleBean;
})(mvc || (mvc = {}));
var mvc;
(function (mvc) {
    class ResBean {
    }
    mvc.ResBean = ResBean;
})(mvc || (mvc = {}));
/**
 * @(#)ViewBean.as
 * @author soda.C
 * @version  1.0
 * <br>Copyright (C), 2007 soda.C
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * @data 2008-1-10
 */
var mvc;
/**
 * @(#)ViewBean.as
 * @author soda.C
 * @version  1.0
 * <br>Copyright (C), 2007 soda.C
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * @data 2008-1-10
 */
(function (mvc) {
    /**
     * View的数据结构
     * @author soda.C
     * Date:2008-1-10
     */
    class ViewBean extends mvc.BaseBean {
        constructor(clazz, config) {
            super(clazz);
            this.controllerBeans = [];
            this.config = config;
        }
        /**
         * 打开view界面
         * @param param
         */
        openView(param) {
            //能否打开，则进行打开事件，如果没有返回值，则默认为可打开
            this.instance.open(param);
            if (this.controllerBeans != null && this.controllerBeans.length > 0) {
                for (let i = 0; i < this.controllerBeans.length; i++) {
                    if (this.controllerBeans[i].instance) {
                        this.controllerBeans[i].instance.viewOpen();
                    }
                }
            }
        }
    }
    mvc.ViewBean = ViewBean;
})(mvc || (mvc = {}));
/**
 * @ViewBean.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-27
 */
var mvc;
/**
 * @ViewBean.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-27
 */
(function (mvc) {
    /**
     *
     * @author sodaChen
     * Date:2015-6-27
     */
    class ViewConfig extends mvc.BaseConfig {
        // /** ui级别，0表示没级别。同一级别 **/
        // lv					:number = 0;
        constructor() {
            super();
            /**
             * 是否需要框架自动调用init方法(一般init方法都是加载资源之类的)
             * 不需要监听View的完成通知，直接调用view的init方法
             **/
            this.auto = true;
            /** 是否会自动在init之后，自动调用open方法 **/
            this.open = false;
            /** y坐标 **/
            this.x = 0;
            /** x坐标 **/
            this.y = 0;
            /** 复位设定，决定每次view被add进去（打开的）时候，是否读取配置文件里的配置的坐标(也就是还原刚开始的坐标) **/
            this.reset = false;
            /**  是否具有交换的属性，该属性可以让view有焦点时处于最顶层 **/
            this.swap = true;
            /**  是否支持tab键  **/
            this.tab = true;
            /** 是否拖动  **/
            this.drag = false;
            /** 始终在顶层的view  **/
            this.topView = false;
            /** 是否独占该组(如果独占，则同组的其他view都会被移除容器)  **/
            this.alone = false;
        }
    }
    mvc.ViewConfig = ViewConfig;
})(mvc || (mvc = {}));
/**
 * @BasicCore.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-12-13
 */
var mvc;
/**
 * @BasicCore.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-12-13
 */
(function (mvc) {
    /**
     *
     * @author sodaChen
     * Date:2016-12-13
     */
    // export class BasicCore<D extends IModel> implements IBaseCore<D>
    class BaseCore {
        /**
         * 初始化方法
         */
        constructor() {
        }
        /**
         * 添加一个观察者通知。注意这里添加之后，调用销毁方法时会自动清除掉内部对他的引用。
         * 无法手动删除，使用请注意
         * @param notice 通知名称
         * @param listener 通知监听函数
         * @param thisObj 绑定的this对象
         */
        mvcOn(notice, listener, thisObj) {
            let result = mvc.Context.subjects.on(notice, listener, thisObj);
            if (result) {
                if (!this.$mvcNotices)
                    this.$mvcNotices = [];
                //添加新的
                result.notice = notice;
                this.$mvcNotices.push(result);
            }
        }
        /**
         * 清除一个mvc的时间，同时会清除该对象对这个时间的缓存
         * @param notice 通知名称
         * @param listener 通知监听函数
         * @param thisObj 绑定的this对象
         */
        mvcOff(notice, listener, thisObj) {
            mvc.Context.subjects.off(notice, listener, thisObj);
            //遍历缓存是否存在
            if (this.$mvcNotices) {
                let length = this.$mvcNotices.length;
                for (let i = 0; i < length; i++) {
                    let noticeData = this.$mvcNotices[i];
                    if (noticeData.notice == notice && noticeData.listener == listener && noticeData.thisObj == thisObj) {
                        this.$mvcNotices.splice(i, 1);
                        return;
                    }
                }
            }
        }
        init() {
        }
        destroy(o = null) {
            if (this.isDestroy)
                return;
            this.isDestroy = true;
            //清除所有的mvc事件引用
            if (this.$mvcNotices) {
                let len = this.$mvcNotices.length;
                let data;
                let subjects = mvc.Context.subjects;
                for (let i = 0; i < len; i++) {
                    data = this.$mvcNotices[i];
                    subjects.off(data.notice, data.listener, data.thisObj);
                }
                delete this.$mvcNotices;
            }
            this.onDestroy();
        }
        onDestroy() {
        }
    }
    mvc.BaseCore = BaseCore;
})(mvc || (mvc = {}));
///<reference path="../core/BaseCore.ts"/>
/**
 * @BasicController.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2010-2016 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-5-24
 */
var mvc;
///<reference path="../core/BaseCore.ts"/>
/**
 * @BasicController.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2010-2016 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-5-24
 */
(function (mvc) {
    /**
     * Helper的基础对象
     * @author sodaChen
     * Date:2016-5-24
     */
    class BaseController extends mvc.BaseCore {
        constructor() {
            super();
        }
        viewOpen() {
        }
        viewClose() {
        }
    }
    mvc.BaseController = BaseController;
})(mvc || (mvc = {}));
///<reference path="IRootContainer.ts"/>
/**
 * @IResLoader.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2017/4/11
 */
var mvc;
///<reference path="IRootContainer.ts"/>
/**
 * @IResLoader.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2017/4/11
 */
(function (mvc) {
    /**
     * 基础的根容器对象，存放了游戏中各种基层容器对象，实现了基本的层级和通用操作
     * @author sodaChen
     * Date:2017/4/11
     */
    class BaseRootContainer {
        constructor(rootContainer) {
            this.rootContainer = rootContainer;
            this.containerMap = new asf.HashMap();
            this.initContainers();
        }
        /**
         * 获得根容器对象
         */
        getRootContainer() {
            return this.rootContainer;
        }
        /**
         * 初始化容器列表
         * 子类根据是情况进行重写
         */
        initContainers() {
            //添加相关的层
            this.addContainer(mvc.MvcConst.Bg_Layer);
            this.addContainer(mvc.MvcConst.Scene_Layer);
            this.addContainer(mvc.MvcConst.View_Bg_Layer);
            this.addContainer(mvc.MvcConst.Main_Layer);
            this.addContainer(mvc.MvcConst.View_Bottom_Layer);
            this.addContainer(mvc.MvcConst.View_Layer);
            this.addContainer(mvc.MvcConst.View_Top_Layer);
            this.addContainer(mvc.MvcConst.Tip_Layer);
            this.addContainer(mvc.MvcConst.Top_Layer);
        }
        /**
         * 添加一个容器对象到集合里面
         * @param layer
         */
        addContainer(layer) {
            let container = this.createContainer();
            this.containerMap.put(layer, container);
            this[layer] = container;
        }
        /**
         * 根据层的名称返回相应的容器对象
         * @param layer
         * @returns {egret.Sprite}
         */
        getLayerContainer(layer) {
            return this.containerMap.get(layer);
        }
        /**
         * 创建一个指定层名称的容器对象，子类根据不同的UI框架进行创建实现
         * @param layer
         */
        createContainer() {
            return null;
        }
        /**
         * 添加child到指定层中
         * @param layer
         * @param child
         */
        addChild(layer, child) {
        }
        /**
         * 从指定层中删除child
         * @param layer
         * @param child
         */
        removeChild(layer, child) {
        }
        destroy(o) {
        }
    }
    mvc.BaseRootContainer = BaseRootContainer;
})(mvc || (mvc = {}));
/**
 * @BaseBindingModel.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2021-10-01
 */
var mvc;
/**
 * @BaseBindingModel.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2021-10-01
 */
(function (mvc) {
    /**
     * 基础绑定数据模型的基类
     * @author sodaChen
     * #Date: 2021-10-01
     */
    class BaseBindingModel {
        constructor() {
            this.observerMap = {};
        }
        /**
         * 当前数据模型广播自己，所有监听了这个观察者的对象，都会收到对应的广播
         */
        notice() {
            let list = this.observerMap["BaseBindingModel"];
            if (list == null) {
                return;
            }
            //广播他自己出去
            let len = list.length;
            let data;
            for (let i = 0; i < len; i++) {
                data = list[i];
                data.callBack.call(data.that, this);
            }
        }
        /**
         * 添加一个观察者
         * @param callBack
         * @param that
         */
        addObserver(callBack, that) {
            let list = this.observerMap["BaseBindingModel"];
            if (!list) {
                list = [];
                this.observerMap["BaseBindingModel"] = list;
            }
            list.push(new mvc.BindingData(callBack, that));
        }
        /**
         * 删除掉一个观察者
         * @param callBack
         * @param that
         */
        removeObserver(callBack, that) {
            let list = this.observerMap["BaseBindingModel"];
            if (list == null) {
                return;
            }
            let len = list.length;
            let data;
            for (let i = 0; i < len; i++) {
                data = list[i];
                if (data.callBack == callBack && data.that == that) {
                    list.splice(i, 1);
                    return;
                }
            }
        }
        /**
         * 绑定数据变化和对应的回调函数
         * @param prop
         * @param callBack
         * @param that
         */
        binding(prop, callBack, that) {
            let value = this[prop];
            if (value || value == 0) {
                let data = new mvc.BindingData(callBack, that);
                //有值，添加对应的数据绑定
                let list = this.observerMap[prop];
                if (list == null) {
                    list = [];
                    this.observerMap[prop] = list;
                    //进行数据绑定
                    this.bindingProp(prop, list);
                }
                list.push(data);
            }
        }
        bindingProp(prop, list) {
            //这个生成闭包函数了，需要注意内存泄露
            let value = this[prop];
            Object.defineProperty(this, prop, {
                get: function () {
                    return value;
                },
                set: function (newVal) {
                    if (value === newVal)
                        return;
                    value = newVal;
                    //遍历list，进行通知所有的观察者
                    let len = list.length;
                    let data;
                    for (let i = 0; i < len; i++) {
                        data = list[i];
                        data.callBack.call(data.that, value);
                    }
                }
            });
        }
        init() {
        }
    }
    mvc.BaseBindingModel = BaseBindingModel;
})(mvc || (mvc = {}));
/**
 * @BindingData.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2021-10-01
 */
var mvc;
/**
 * @BindingData.ts
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2021-10-01
 */
(function (mvc) {
    /**
     * 绑定的数据结构
     * @author sodaChen
     * #Date: 2021-10-01
     */
    class BindingData {
        constructor(callBack, that) {
            this.callBack = callBack;
            this.that = that;
        }
    }
    mvc.BindingData = BindingData;
})(mvc || (mvc = {}));
/**
 * @BasicModule.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-20
 */
var mvc;
/**
 * @BasicModule.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-20
 */
(function (mvc) {
    /**
     * 基础模块，大部分模块都应该继承这个基类，实现了一些基本和简单的功能
     * @author sodaChen
     * Date:2015-6-20
     */
    class BaseModule extends mvc.BaseCore {
        constructor() {
            super();
        }
        /**
         * 模块的启动方法，框架内部调用的方法。响应这个方法的时候，表示系统所有已经init的模块都准备好了
         * 可以直接调用其他模块
         * 子类需要出的话，在这里进行覆盖重写
         */
        start() {
        }
        /**
         * 拉取服务器数据
         */
        takeServer() {
        }
    }
    mvc.BaseModule = BaseModule;
})(mvc || (mvc = {}));
/**
 * @InitModule.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-22
 */
var mvc;
/**
 * @InitModule.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2015-6-22
 */
(function (mvc) {
    /**
     * 初始化模块对象
     * @author sodaChen
     * Date:2015-6-22
     */
    class CreateModule {
        initModule(moduleBean) {
            //目前假设都需要立即启动
            let module = mvc.MvcUtils.createCore(moduleBean);
            mvc.MvcUtils.setIdName(module, moduleBean);
            //尝试自动注入模块
            if (mvc.Context.$moduleMgrClass) {
                // if(Context.$moduleMgrClass.hasOwnProperty(module.name))
                //不用判断，直接存放进去
                mvc.Context.$moduleMgrClass[module.name] = module;
            }
            //检测是否有
            if (moduleBean.dbClass) {
                let model = new moduleBean.dbClass();
                model.init();
                module.selfDB = model;
                //防止重复初始化
                moduleBean.selfDB = model;
                //注入到DB管理器中
                let mName = moduleBean.dbClass[mvc.MvcConst.Class_Name];
                if (mvc.Context.$dbClass && mName) {
                    mName = asf.StringUtils.uncapitalize(mName);
                    mvc.Context.$dbClass[mName] = model;
                }
            }
            //模块自己初始化
            try {
                module.init();
            }
            catch (e) {
                console.error(moduleBean.name + "initModule报错:", e);
            }
        }
    }
    mvc.CreateModule = CreateModule;
})(mvc || (mvc = {}));
/**
 * @BaseView.ts
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-20
 */
var mvc;
/**
 * @BaseView.ts
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-20
 */
(function (mvc) {
    /**
     * mvc框架的基础View对象
     * @author sodaChen
     * #Date:2016-9-20
     */
    class BaseView extends mvc.BaseCore {
        constructor() {
            super();
            /**
             * 界面级别,默认是0，也就是不做任何处理
             */
            this.lv = 0;
            /** 自动释放，当关闭的时候就自动释放资源 **/
            this.isCloseDestroy = true;
            this.layer = mvc.MvcConst.View_Layer;
            //采用欺骗编译器的做法，主要是保留ViewConfig的打点提示，不要默认属性。
            //因为这个对象的属性会copy给ViewBean的config，所以不能实际使用ViewConfig
            this.config = {};
        }
        /**
         * 设置UI的资源，所在组的名字
         * @param group
         */
        setUIRes(group) {
            let resBean = new mvc.ResBean();
            if (!this.config.resList)
                this.config.resList = [];
            resBean.path = group;
            this.config.resList.push(resBean);
        }
        /**
         * 初始化容器对象，使用方法的好处是可以在方法里进行其他扩展操作
         * @param container
         */
        initContainer(container) {
            this.container = container;
        }
        start() {
            this.onStart();
        }
        onStart() {
        }
        open(param) {
            //添加一个view
            this.isOpen = true;
            mvc.Context.$addView(this);
            this.onOpen(param);
        }
        onOpen(param) {
        }
        close() {
            if (!this.isOpen)
                return;
            this.isOpen = false;
            this.onClose();
            mvc.Context.$closeView(this);
            //自动释放，并且还没释放
            if (this.isCloseDestroy && !this.isDestroy) {
                this.destroy();
            }
        }
        onClose() {
        }
        destroy(o = null) {
            if (this.isDestroy)
                return;
            super.destroy(o);
            mvc.Context.$destroyViewEvent(this);
        }
    }
    mvc.BaseView = BaseView;
})(mvc || (mvc = {}));
/**
 * @LayaViewHandler.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/7
 */
var mvc;
/**
 * @LayaViewHandler.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/7
 */
(function (mvc) {
    /**
     * 通过View控制器，基础的View操作都在这里。框架管理所有的View
     * 通过这里可以查找到基于框架的所有View对象
     */
    class BaseViewHandler {
        constructor(rootContainer) {
            this.rootContainer = rootContainer;
            this.viewMap = new asf.HashMap();
            this.lvMax = mvc.MvcConst.View_Lv_Max;
            this.lvList = [];
            for (let i = 0; i < this.lvMax; i++) {
                // this.lvList[i] = [];
                this.lvList[i] = null;
            }
        }
        /**
         * 关闭指定等级的view
         * @param lv
         */
        closeViewByLv(lv) {
            let start = lv;
            let len = start + 1;
            //-1关闭全部打开的界面
            if (lv == -1) {
                start = 1;
                len = this.lvList.length;
            }
            let viewMap = this.viewMap;
            for (let i = start; i < len; i++) {
                // let lvBeanList: ViewBean[] = this.lvList[i];
                let lvBean = this.lvList[i];
                ;
                // let length = lvBeanList.length;
                // if(length != 0)
                // {
                //     for (let j = 0; j < length; j++)
                //     {
                //         if (lvBean && lvBean.instance) {
                //             lvBean.instance.close();
                //             viewMap.remove(lvBean.name);
                //         }
                //     }
                //     //生成新的数组
                //     this.lvList[i] = [];
                // }
            }
        }
        /**
         * 是否已经添加了view对象
         * @param view
         */
        hasView(view) {
            return this.viewMap.hasKey(view.name);
        }
        /**
         * 添加一个View对象
         * @param view
         */
        addView(view) {
            let isSame = false;
            if (view.instance.lv == 1) {
                // //监听相关事件
                // let lvBeanList: ViewBean[] = this.lvList[view.instance.lv];
                // //如果是1级界面，则关闭其他所有的界面了
                // for (let i: number = 1; i < 5; i++) {
                //     lvBean = this.lvList[i];
                //     //排除自己
                //     if (lvBean && lvBean.instance && lvBean != view) {
                //         // if (!view.instance.noCloseIds || view.instance.noCloseIds.indexOf(lvBean.name) == -1) {
                //         //     //删除掉老的view
                //         //     lvBean.instance.close();
                //         //     this.lvList[i] = null;
                //         // }
                //     }
                // }
            }
            //如果是打开同样的，则只是关闭，不再添加进来
            //设置为最新级别的view
            this.lvList[view.instance.lv] = view;
            this.viewMap.put(view.name, view);
            this.onAddView(view);
            return true;
        }
        /**
         * 子类扩展view的实际添加
         * @param viewBean
         */
        onAddView(viewBean) {
        }
        /**
         * 删除一个View对象
         * @param view
         */
        delView(view) {
            this.viewMap.remove(view.name);
            this.lvList[view.instance.lv] = null;
            //删除相关事件
            this.onDelView(view);
        }
        onDelView(viewBean) {
        }
        /**
         * 是否打开了指定级别的view
         * @param lv:number 界面级别
         * @return boolean 是否打开
         */
        hasViewByLv(lv) {
            let view = this.lvList[lv];
            if (view && view.instance)
                return true;
            return false;
        }
        /**
         * 判断当前层级是否有显示View
         */
        hasViewByLayer(layer) {
            let viewMap = this.viewMap;
            let values = viewMap.values();
            if (values) {
                for (let view of values) {
                    if (view.instance.layer == layer) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
    mvc.BaseViewHandler = BaseViewHandler;
})(mvc || (mvc = {}));
/**
 * @InitView.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2010-2016 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-5-30
 */
var mvc;
/**
 * @InitView.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2010-2016 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-5-30
 */
(function (mvc) {
    /**
     * 初始化界面的类
     * @author sodaChen
     * Date:2016-5-30
     */
    class CreateView {
        constructor(isOpen, viewBean, callBack, openParam) {
            this.viewBean = viewBean;
            this.callBack = callBack;
            this.isOpen = isOpen;
            this.openParam = openParam;
            this.view = mvc.MvcUtils.createCore(viewBean);
            let selfDB;
            let moduleBean = viewBean.moduleBean;
            if (moduleBean) {
                let module = moduleBean.instance;
                //模块注入view实例
                if (module.hasOwnProperty(this.viewBean.name))
                    module[this.viewBean.name] = this.view;
                //设置module
                this.view.module = module;
                selfDB = moduleBean.selfDB;
            }
            //检测没有module。但是有db的情况
            if (!selfDB && viewBean.dbClass) {
                if (viewBean.selfDB)
                    selfDB = viewBean.selfDB;
                else {
                    selfDB = new viewBean.dbClass();
                    viewBean.selfDB = selfDB;
                    selfDB.init();
                }
            }
            if (selfDB)
                this.view.selfDB = selfDB;
            //初始化view的config
            mvc.MvcUtils.copyConfig(this.view, viewBean.config);
            viewBean.isLoading = true;
            //自动初始化或者是属于打开的界面
            if (viewBean.config.resList && viewBean.config.resList.length > 0) {
                this.resLoader = new mvc.Context.$resLoaderClass();
                this.resLoader.loadResList(viewBean.config.resList, this);
            }
            else {
                this.hasInit();
            }
        }
        hasInit() {
            //调用初始化方法
            this.view.init();
            //目前没有异步，直接调用完成事件，进行相关初始化
            this.onViewComplete();
        }
        /**
         * 用来接受资源加载器加载完成资源
         * @param values 实际的资源
         * @param urls 原来加载资源路径
         */
        setResList(values, resList) {
            this.resLoader.destroy();
            delete this.resLoader;
            this.hasInit();
        }
        onViewComplete() {
            this.viewBean.isLoading = false;
            asf.Global.removeRef(this);
            //初始化对应的helper，helper一定是随着view初始化而设置的（这里有可能helper上次实例化了，目前还不做这个处理）
            let controllerBeans = this.viewBean.controllerBeans;
            if (controllerBeans) {
                let controllerBean;
                let controller;
                for (let i = 0; i < controllerBeans.length; i++) {
                    controllerBean = controllerBeans[i];
                    controller = controllerBean.instance;
                    if (!controller) {
                        controller = mvc.MvcUtils.createCore(controllerBean);
                        controllerBean.instance = controller;
                    }
                    //框架强制规定设置的view对象
                    controller.view = this.view;
                    //view注入控制器
                    mvc.MvcUtils.eachInject(this.view, controller);
                    //具备模块数据
                    if (this.viewBean.moduleBean) {
                        //herper注册进模块里  模块反注册到helper里面去
                        mvc.MvcUtils.eachInject(controller, this.viewBean.moduleBean.instance);
                    }
                }
                //init
                for (let k = 0; k < controllerBeans.length; k++) {
                    controllerBeans[k].instance.init();
                }
            }
            //所有的在初始化完成之后，都会自动调用start方法。通知外部知道已经完成了全部初始化
            // if(this.viewBean.moduleBean == null || this.isOpen)
            //不具备模块的view，则马上调用start方法，否则是在模块初始化那里调用start方法(这里有可能考虑统一在这里调用start方法)
            this.view.start();
            //外部打开或者内部配置了打开的属性值
            if (this.isOpen || this.viewBean.config.open) {
                if (this.viewBean.instance) {
                    this.viewBean.openView(this.openParam);
                }
            }
            if (this.callBack) {
                this.callBack.execute(this.viewBean);
                delete this.callBack;
            }
        }
    }
    mvc.CreateView = CreateView;
})(mvc || (mvc = {}));
/**
 * @ObjectPool.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2014-6-15
 */

/**
 * @ObjectPool.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2014-6-15
 */
(function (asf) {
    /**
     * 基于工厂的生成对象的对象池，主要是用于创建和休闲以及销毁时需要做额外处理的对象
     * 包具体工作外包给IFactoryObject接口，使用者只需要实现自己的IFactoryObject来处理即可。
     * @author sodaChen
     * Date:2014-6-15
     */
    class FactoryObjectPool {
        constructor(objectFactory, maxIdle = 300) {
            this.objectFactory = objectFactory;
            this.maxIdle = maxIdle;
            this.list = [];
        }
        create() {
            var obj = null;
            if (this.list.length == 0) {
                obj = this.objectFactory.create();
            }
            else {
                obj = this.list.shift();
            }
            this.objectFactory.activate(obj);
            return obj;
        }
        release(obj) {
            if (this.list.length < this.maxIdle) {
                this.objectFactory.passivate(obj);
                //放进池里
                this.list.push(obj);
            }
            else {
                // destroyObject(obj);
            }
        }
        getNumIdle() {
            return this.list.length;
        }
        clear() {
        }
        destroy(o = null) {
        }
        isFull() {
            return false;
        }
    }
    asf.FactoryObjectPool = FactoryObjectPool;
})(asf);
/**
 * @ObjectPool.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:挂机冒险
 * <br>Date:2014-6-15
 */

/**
 * @ObjectPool.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2010 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:挂机冒险
 * <br>Date:2014-6-15
 */
(function (asf) {
    /**
     * 普通对象池
     * @author sodaChen
     * Date:2014-6-15
     */
    class Pool {
        // private $tempObj:T;
        /**
         * 创建一个通用对象池实例
         * @param clazz 使用对象的class
         * @param maxIdle 最大缓存数量
         * @param hasStatus 是否需要做状态检测（默认为false，如果没有状态，设置false会提升性能）
         *
         */
        constructor(clazz, maxIdle = 100, hasStatus = false) {
            this.$clazz = clazz;
            this.$maxIdle = maxIdle;
            this.$hasStatus = hasStatus;
            this.$idles = [];
        }
        create() {
            var $tempObj;
            if (this.$idles.length == 0)
                $tempObj = new this.$clazz();
            else
                $tempObj = this.$idles.pop();
            //具备激活接口的对象
            if (this.$hasStatus && $tempObj["activate"])
                $tempObj["activate"]();
            return $tempObj;
        }
        release(obj) {
            if (this.$idles.length > this.$maxIdle) {
                asf.DestroyUtils.destroy(obj);
            }
            else {
                if (this.$hasStatus && obj["passivate"])
                    obj["passivate"]();
                this.$idles.push(obj);
            }
        }
        getNumIdle() {
            return this.$idles.length;
        }
        clear() {
            // destroy();
            this.$idles = [];
        }
        isFull() {
            if (this.$idles.length > this.$maxIdle) {
                return true;
            }
            return false;
        }
        destroy(o = null) {
            for (var i = 0; i < this.$idles.length; i++) {
                asf.DestroyUtils.destroy(this.$idles[i]);
            }
            this.$idles = null;
        }
    }
    asf.Pool = Pool;
})(asf);
/**
 * @PoolManager.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2013-6-20
 */

/**
 * @PoolManager.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2013-6-20
 */
(function (asf) {
    /**
     * 对象池管理器包括IObjectPool的对象，实现了统一管理。具体使用到了pool包里面的各种对象池的实例和接口
     * @author sodaChen
     * #Date:2013-6-20
     */
    class PoolMgr {
        static init() {
            this.$poolMap = new asf.Dictionary();
        }
        /**
         * 获取到指定对象池对象
         * @param clazz 对象池缓存的对象Class
         * @return 返回指定的对象池对象
         *
         */
        static getPool(clazz) {
            return this.$poolMap.get(clazz);
        }
        /**
         * 注册一个需要缓存池的Class对象，内部会自动生成通用的ObjectPool对象池
         * @param clazz 缓存对象class
         * @param maxIdle 最大的空闲数量
         * @param hasStatus 是否需要做激活和不激活的状态检测(false有助提升性能)
         *
         */
        static regClass(clazz, maxIdle = -1, hasStatus = false) {
            let poolMap = this.$poolMap;
            if (poolMap.hasKey(clazz))
                return; //注册过1次就不能再注册了
            if (maxIdle == -1)
                maxIdle = this.defaultMax;
            var pool = new asf.Pool(clazz, maxIdle, hasStatus);
            poolMap.put(clazz, pool);
            return pool;
        }
        /**
         * 注册一个对象池，由外部提供对象池
         * @param clazz 生产的对象class
         * @param objectPool 对象池实例
         */
        static regPool(clazz, objectPool) {
            this.$poolMap.put(clazz, objectPool);
        }
        /**
         * 销毁指定池对象中的所有子节点
         */
        static clearPoolChildren(clazz) {
            let pool = this.$poolMap.get(clazz);
            if (pool) {
                pool.destroy();
            }
        }
        /**
         * 清除指定的对象池
         * @param clazz 对象class
         * @param isDestory 是否同时销毁。默认false
         */
        static clearPool(clazz, isDestory = false) {
            let poolMap = this.$poolMap;
            let pool = poolMap.get(clazz);
            if (!pool)
                return;
            if (isDestory) {
                pool.destroy();
                poolMap.remove(clazz);
            }
            else {
                pool.clear();
            }
        }
        /**
         * 借出一个对象(借出的对象，对象池和管理器本身不会对它有任何引用，完全干净无引用的对象)
         * @param clazz
         * @return
         *
         */
        static create(clazz) {
            let pool = this.$poolMap.get(clazz);
            if (!pool)
                return null;
            return pool.create();
        }
        /**
         * 归还对象。如果对象池满了，则会进行销毁。调用dispose方法或者IDestroy接口的方法
         * @param obj 实例
         * @param clazz 对象class.为空时则会自动从obj上获取到对应的class
         */
        static release(obj, clazz = null) {
            if (!clazz)
                clazz = asf.ClassUtils.forInstance(obj);
            let pool = this.$poolMap.get(clazz);
            if (pool)
                pool.release(obj);
        }
        /**
         * 是否已满
         * @param obj
         * @param clazz
         * @return
         *
         */
        static isFull(obj, clazz = null) {
            if (!clazz)
                clazz = asf.ClassUtils.forInstance(obj);
            let pool = this.$poolMap.get(clazz);
            return pool.isFull();
        }
    }
    /** 默认一个对象池的最大空闲对象是数量 **/
    PoolMgr.defaultMax = 20;
    asf.PoolMgr = PoolMgr;
})(asf);

(function (asf) {
    /**
     * 计时器管理器，游戏中所有的计时器，目前是采用时间，不支持帧计时器
     *
     * @author sodaChen
     * Date:2011-5-4
     */
    class TimeMgr {
        constructor() {
            /**
             * vo对象池
             */
            this._pool = [];
            /**
             * 当前执行器数量
             */
            this._count = 0;
            /**
             * 当前
             */
            this._maxIndex = 0; //当前最大id值
            /**待删除的计时器 */
            this._waitDelHandlerArr = [];
            this._keyMap = new asf.HashMap();
        }
        start() {
            this.isStop = false;
        }
        /**
         * 停止所有timer心跳
         */
        stop() {
            this.isStop = true;
            console.log("请不要随意停止timer");
        }
        clear() {
            this.isStop = false;
            this.toClearTimer();
            this._keyMap.clear();
            this._count = 0;
        }
        onEnterFrame(timestamp) {
            if (this.isStop) {
                return;
            }
            this.lifecycleTime = 0;
            let self = this;
            let handler;
            //新做法，直接遍历object对象
            let container = this._keyMap.getContainer();
            for (let key in container) {
                handler = container[key];
                if (!handler || handler.isPause || handler.isDel) {
                    continue;
                }
                let t = timestamp;
                if (t >= handler.exeTime) {
                    let method = handler.method;
                    let args = handler.args;
                    if (handler.repeat) {
                        method.apply(handler.thisObj, args);
                    }
                    else {
                        self.clearTimer(handler.key);
                        method.apply(handler.thisObj, args);
                    }
                }
            }
            //处理待删除列表
            this.toClearTimer();
        }
        /**
         * 创建1个计时器
         */
        create(repeat, delay, method, thisObj, args, lastKey) {
            if (lastKey)
                this.clearTimer(lastKey);
            //如果执行时间小于1，直接执行
            if (delay < 1) {
                method.apply(thisObj, args);
                return 0;
            }
            //从池里拿出对象
            let handler = this._pool.length > 0 ? this._pool.shift() : new TimerVo();
            handler.repeat = repeat;
            handler.delay = delay;
            handler.method = method;
            handler.thisObj = thisObj;
            handler.args = args;
            handler.exeTime = delay;
            this.addHandler(handler);
            return handler.key;
        }
        /**
         * 增加计时器处理方法
         */
        addHandler(handler) {
            if (!this.isStop)
                return;
            this._count++; //数量增加
            let key = ++this._maxIndex;
            handler.key = key;
            this._keyMap.put(key, handler); //每一个计时器都绑定1个新的唯一的计时器ID
        }
        /**定时执行一次
         * 用的时候一定要把上一个key给去掉
         * @param	delay  延迟时间(单位毫秒)
         * @param	method 结束时的回调方法
         * @param	args   回调参数
         * @param	lastKey   上一个重复方法实例key，如果没有则可以传0
         * @return 返回唯一ID，均用来作为clearTimer的参数*/
        doOnce(delay, method, thisObj, lastKey = 0, args = null) {
            return this.create(false, delay, method, thisObj, args, lastKey);
        }
        /**定时重复执行
         * 用的时候一定要把上一个key给去掉
         * @param	delay  延迟时间(单位毫秒)
         * @param	method 结束时的回调方法
         * @param	thisObj 方法的this对象
         * @param	args   回调参数
         * @param	lastKey   上一个重复方法实例key，如果没有则可以传0
         * @return 返回唯一ID，均用来作为clearTimer的参数*/
        doLoop(delay, method, thisObj, lastKey = 0, args = null) {
            return this.create(false, delay, method, thisObj, args, lastKey);
        }
        /**定时器执行数量*/
        get count() {
            return this._count;
        }
        /**清理定时器
         * @param 唯一ID，均用来作为clearTimer的参数
         */
        clearTimer(key) {
            let handler = this.getHandler(key);
            if (handler && !handler.isDel) {
                //标记以删除
                handler.isDel = true;
                this._waitDelHandlerArr.push(handler); //添加到待删除列表
            }
        }
        /**删除待删除列表的计时器 */
        toClearTimer() {
            let len = this._waitDelHandlerArr.length;
            let handler;
            for (let i = 0; i < len; i++) {
                handler = this._waitDelHandlerArr[i];
                //删除key
                this._keyMap.remove(handler.key);
                handler.clear(); //重置属性
                this._pool.push(handler); //回收对象池
                this._count--; //数量-1
            }
            this._waitDelHandlerArr.length = 0;
        }
        /**
         *  获取TimerVo
         */
        getHandler(key) {
            return this._keyMap.get(key);
        }
        /**
         *暂停定时器
         */
        pauseTimer(key) {
            let handler = this._keyMap.get(key);
            if (handler != null) {
                handler.isPause = true;
            }
        }
        /**
         *恢复定时器
         */
        resumeTimer(key) {
            let handler = this._keyMap.get(key);
            if (handler != null) {
                handler.isPause = false;
            }
        }
    }
    asf.TimeMgr = TimeMgr;
    /**定时处理器*/
    class TimerVo {
        constructor() {
            /**唯一ID */
            this.key = -1;
        }
        /**清理*/
        clear() {
            this.key = -1;
            this.method = null;
            this.thisObj = null;
            this.args = null;
            this.isPause = false;
            this.isDel = false;
        }
    }
    asf.TimerVo = TimerVo;
})(asf);
/**
 * @ArrayUtils.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/23
 */

/**
 * @ArrayUtils.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/23
 */
(function (asf) {
    class ArrayUtils {
        static removeItem(array, item) {
            var i = array.length;
            while (i > 0) {
                i--;
                if (array[i] === item) {
                    array.splice(i, 1);
                    return;
                }
            }
            console.log("removeItem");
        }
        /**新随机数组*/
        static randomAry(arr) {
            var outputArr = arr.slice();
            var i = outputArr.length;
            var temp;
            var indexA;
            var indexB;
            while (i) {
                indexA = i - 1;
                indexB = Math.floor(Math.random() * i);
                i--;
                if (indexA == indexB)
                    continue;
                temp = outputArr[indexA];
                outputArr[indexA] = outputArr[indexB];
                outputArr[indexB] = temp;
            }
            return outputArr;
        }
        /**
         * 拷贝一个字典
         */
        static copyDict(dict, isDeepCopy) {
            if (!dict) {
                return dict;
            }
            var newDict = (dict instanceof Array) ? [] : {};
            for (var i in dict) {
                if (typeof dict[i] == "function") {
                    continue;
                }
                if (isDeepCopy) {
                    newDict[i] = (typeof dict[i] == "object") ? ArrayUtils.copyDict(dict[i]) : dict[i];
                }
                else {
                    newDict[i] = dict[i];
                }
            }
            return newDict;
        }
    }
    asf.ArrayUtils = ArrayUtils;
})(asf);
/**
 * @ClassUtils.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-1-14
 */

/**
 * @ClassUtils.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-1-14
 */
(function (asf) {
    /**
     *
     * @author sodaChen
     * Date:2012-1-14
     */
    class ClassUtils {
        /**
         * 获取到Class的名字（不包含包名）.这个方法要注意，因为打包成min.js的时候，会修改类名，所以到时无法获取真实的类名字了
         * @param clazz 构造函数实例
         * @param firstMin 名字的首字母是否小写。默认是不修改
         * @returns {string} 返回名字
         */
        static getClassName(clazz, firstMin = false) {
            var name = clazz.prototype.constructor.name;
            if (firstMin) {
                //首字母变小写
                return asf.StringUtils.uncapitalize(name);
            }
            return name;
        }
        /**
         * 根据对象的实例返回<code>Class<code>对象
         * @param instance 需要返回class对象的实例
         * @return 实例的<code>构造函数</code>
         */
        static forInstance(instance) {
            if (!instance[this.CONSTRUCTOR_NAME])
                return instance[this.CONSTRUCTOR_NAME];
            return null;
        }
        /**
         * 根据名字获得对应的构造函数对象
         * @param name 构造函数名字
         * @return the 名字的<code>构造函数</code>
         */
        static forName(name) {
            return eval(name);
        }
        /**
         * Creates an instance of the given class and passes the arguments to
         * the constructor.
         *
         * TODO find a generic solution for this. Currently we support constructors
         * with a maximum of 10 arguments.
         *
         * @param clazz the class from which an instance will be created
         * @param args the arguments that need to be passed to the constructor
         */
        static newInstance(clazz, args = null) {
            var result;
            ;
            var a = (args == null) ? [] : args;
            switch (a.length) {
                case 1:
                    result = new clazz(a[0]);
                    break;
                case 2:
                    result = new clazz(a[0], a[1]);
                    break;
                case 3:
                    result = new clazz(a[0], a[1], a[2]);
                    break;
                case 4:
                    result = new clazz(a[0], a[1], a[2], a[3]);
                    break;
                case 5:
                    result = new clazz(a[0], a[1], a[2], a[3], a[4]);
                    break;
                case 6:
                    result = new clazz(a[0], a[1], a[2], a[3], a[4], a[5]);
                    break;
                case 7:
                    result = new clazz(a[0], a[1], a[2], a[3], a[4], a[5], a[6]);
                    break;
                case 8:
                    result = new clazz(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]);
                    break;
                case 9:
                    result = new clazz(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8]);
                    break;
                case 10:
                    result = new clazz(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9]);
                    break;
                default:
                    result = new clazz();
            }
            return result;
        }
    }
    ClassUtils.CONSTRUCTOR_NAME = "constructor";
    asf.ClassUtils = ClassUtils;
})(asf);
/*
    CASA Lib for ActionScript 3.0
    Copyright (c) 2011, Aaron Clinger & Contributors of CASA Lib
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    
    - Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    
    - Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    
    - Neither the name of the CASA Lib nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*/

/*
    CASA Lib for ActionScript 3.0
    Copyright (c) 2011, Aaron Clinger & Contributors of CASA Lib
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    
    - Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    
    - Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    
    - Neither the name of the CASA Lib nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*/
(function (asf) {
    /**
        Provides utility functions for dealing with color.
        @author Aaron Clinger
        @version 03/29/10
    */
    class ColorUtil {
        /**
            Converts a series of individual RGB(A) values to a 32-bit ARGB color value.
            
            @param r: A uint from 0 to 255 representing the red color value.
            @param g: A uint from 0 to 255 representing the green color value.
            @param b: A uint from 0 to 255 representing the blue color value.
            @param a: A uint from 0 to 255 representing the alpha value. Default is <code>255</code>.
            @return Returns a hexidecimal color as a String.
            @example
                <code>
                    var hexColor:string = ColorUtil.getHexStringFromARGB(128, 255, 0, 255);
                    trace(hexColor); // Traces 80FF00FF
                </code>
        */
        static getColor(r, g, b, a = 255) {
            return (a << 24) | (r << 16) | (g << 8) | b;
        }
        /**
            Converts a 32-bit ARGB color value into an ARGB object.
            
            @param color: The 32-bit ARGB color value.
            @return Returns an object with the properties a, r, g, and b defined.
            @example
                <code>
                    var myRGB:Object = ColorUtil.getARGB(0xCCFF00FF);
                    trace("Alpha = " + myRGB.a);
                    trace("Red = " + myRGB.r);
                    trace("Green = " + myRGB.g);
                    trace("Blue = " + myRGB.b);
                </code>
        */
        static getARGB(color) {
            var c = {};
            c.a = color >> 24 & 0xFF;
            c.r = color >> 16 & 0xFF;
            c.g = color >> 8 & 0xFF;
            c.b = color & 0xFF;
            return c;
        }
        /**
            Converts a 24-bit RGB color value into an RGB object.
            
            @param color: The 24-bit RGB color value.
            @return Returns an object with the properties r, g, and b defined.
            @example
                <code>
                    var myRGB:Object = ColorUtil.getRGB(0xFF00FF);
                    trace("Red = " + myRGB.r);
                    trace("Green = " + myRGB.g);
                    trace("Blue = " + myRGB.b);
                </code>
        */
        static getRGB(color) {
            var c = {};
            c.r = color >> 16 & 0xFF;
            c.g = color >> 8 & 0xFF;
            c.b = color & 0xFF;
            return c;
        }
        /**
            Converts a 32-bit ARGB color value into a hexidecimal String representation.
            
            @param a: A uint from 0 to 255 representing the alpha value.
            @param r: A uint from 0 to 255 representing the red color value.
            @param g: A uint from 0 to 255 representing the green color value.
            @param b: A uint from 0 to 255 representing the blue color value.
            @return Returns a hexidecimal color as a String.
            @example
                <code>
                    var hexColor:string = ColorUtil.getHexStringFromARGB(128, 255, 0, 255);
                    trace(hexColor); // Traces 80FF00FF
                </code>
        */
        static getHexStringFromARGB(a, r, g, b) {
            var aa = a.toString(16);
            var rr = r.toString(16);
            var gg = g.toString(16);
            var bb = b.toString(16);
            aa = (aa.length == 1) ? '0' + aa : aa;
            rr = (rr.length == 1) ? '0' + rr : rr;
            gg = (gg.length == 1) ? '0' + gg : gg;
            bb = (bb.length == 1) ? '0' + bb : bb;
            return (aa + rr + gg + bb).toUpperCase();
        }
        /**
            Converts an RGB color value into a hexidecimal String representation.
            
            @param r: A uint from 0 to 255 representing the red color value.
            @param g: A uint from 0 to 255 representing the green color value.
            @param b: A uint from 0 to 255 representing the blue color value.
            @return Returns a hexidecimal color as a String.
            @example
                <code>
                    var hexColor:string = ColorUtil.getHexStringFromRGB(255, 0, 255);
                    trace(hexColor); // Traces FF00FF
                </code>
        */
        static getHexStringFromRGB(r, g, b) {
            var rr = r.toString(16);
            var gg = g.toString(16);
            var bb = b.toString(16);
            rr = (rr.length == 1) ? '0' + rr : rr;
            gg = (gg.length == 1) ? '0' + gg : gg;
            bb = (bb.length == 1) ? '0' + bb : bb;
            return (rr + gg + bb).toUpperCase();
        }
        /**
         * 输入一个颜色,将它拆成三个部分:
         * 红色,绿色和蓝色
         */
        static retrieveRGB(color) {
            var r = color >> 16;
            var g = (color >> 8) & 0xff;
            var b = color & 0xff;
            return [r, g, b];
        }
        /**
         * 红色,绿色和蓝色三色组合
         */
        static generateFromRGB(rgb) {
            if (rgb == null || rgb.length != 3 ||
                rgb[0] < 0 || rgb[0] > 255 ||
                rgb[1] < 0 || rgb[1] > 255 ||
                rgb[2] < 0 || rgb[2] > 255) {
                return 0xFFFFFF;
            }
            return rgb[0] << 16 | rgb[1] << 8 | rgb[2];
        }
        /**
         * color1是浅色,color2是深色,实现渐变
         * steps是指在多大的区域中渐变,
         */
        static generateTransitionalColor(color1, color2, steps) {
            if (steps < 3) {
                return [];
            }
            var color1RGB = this.retrieveRGB(color1);
            var color2RGB = this.retrieveRGB(color2);
            var colors = [];
            colors.push(color1);
            steps = steps - 2;
            var redDiff = color2RGB[0] - color1RGB[0];
            var greenDiff = color2RGB[1] - color1RGB[1];
            var blueDiff = color2RGB[2] - color1RGB[2];
            for (var i = 1; i < steps - 1; i++) {
                var tmpRGB = [
                    color1RGB[0] + redDiff * i / steps,
                    color1RGB[1] + greenDiff * i / steps,
                    color1RGB[2] + blueDiff * i / steps
                ];
                colors.push(this.generateFromRGB(tmpRGB));
            }
            colors.push(color2);
            return colors;
        }
    }
    asf.ColorUtil = ColorUtil;
})(asf);

(function (asf) {
    /**
     * 时间日期工具
     *
     * @export
     * @class DateUtils
     */
    class DateUtils {
        constructor() {
        }
    }
    asf.DateUtils = DateUtils;
})(asf);
/**
 * @DestroyUtils.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 FeiYin.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Game3DAnd2D
 * <br>Date:2013-9-27
 */

/**
 * @DestroyUtils.as
 *
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br>Copyright (C), 2013 FeiYin.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Game3DAnd2D
 * <br>Date:2013-9-27
 */
(function (asf) {
    /**
     * 销毁对象的工具方法，会自动寻找数组以及"destroy","dispose"这个几个因素来销毁对象
     * @author sodaChen
     * #Date:2012-9-27
     */
    class DestroyUtils {
        static destroy(obj) {
            if (obj instanceof Array) {
                for (var i = 0; i < obj.length; i++) {
                    this.destroy(obj[i]);
                }
                return;
            }
            for (var i = 0; i < this.keyNames.length; i++) {
                if (obj[this.keyNames[i]]) {
                    obj[this.keyNames[i]]();
                    return;
                }
            }
        }
    }
    DestroyUtils.keyNames = ["destroy", "dispose"];
    asf.DestroyUtils = DestroyUtils;
})(asf);
/**
 * @FunctionUtils.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-2-1
 */

/**
 * @FunctionUtils.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-2-1
 */
(function (asf) {
    /**
     *
     * @author sodaChen
     * Date:2012-2-1
     */
    class FuncUtils {
        /**
         * 执行回调函数
         * @param fun
         * @param thisObj
         * @param param 可选参数
         */
        static execute(fun, thisObj, param = null) {
            if (fun == null)
                return;
            if (param == null)
                fun.call(thisObj);
            else
                fun.call(thisObj, param);
        }
        /**
         * 执行具有数组参数的回调函数
         * @param fun
         * @param thisObj
         * @param params 可选参数
         */
        static executeAry(fun, thisObj, params = null) {
            if (fun == null)
                return;
            if (params == null) {
                fun.call(thisObj);
                return;
            }
            fun.apply(thisObj, params);
        }
    }
    asf.FuncUtils = FuncUtils;
})(asf);

(function (asf) {
    class JSUtils {
        /**
         * 添加一个js脚本
         * @param {String} js
         * @param {HTMLElement} ele
         */
        static addJS(js, ele) {
            var script = document.createElement("script");
            script.setAttribute("type", "text/javascript");
            script.text = js;
            if (!ele)
                ele = document.body;
            ele.appendChild(script);
            ele.removeChild(script);
        }
    }
    asf.JSUtils = JSUtils;
})(asf);
/**
 * @ObjectUtils.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/18
 */

/**
 * @ObjectUtils.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/1/18
 */
(function (asf) {
    class ObjectUtils {
        static hasProperty(target, property) {
            if (target.hasOwnProperty(property))
                return true;
            //主要是继承来的
            if (target[property] == undefined)
                return false;
            return true;
        }
        /**
         * 字符串的属性不为空
         * @param target
         * @param property
         * @returns {boolean}
         */
        static strNoNull(target, property) {
            if (target[property] == undefined)
                return false;
            if (target[property] == null)
                return false;
            if (target[property] == "")
                return false;
            return true;
        }
        static copyObject(target, obj) {
            if (!obj)
                obj = {};
            for (var key in target) {
                //不管3721，全部给他
                obj[key] = target[key];
            }
            return obj;
        }
    }
    asf.ObjectUtils = ObjectUtils;
})(asf);
/**
 * @RandomUtils.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-1-19
 */

/**
 * @RandomUtils.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-1-19
 */
(function (asf) {
    /**
     * 随机数工具类
     * @author sodaChen
     * Date:2012-1-19
     */
    class RandomUtils {
        /**
         * 返回一个
         * @param minNum
         * @param maxNum
         * @returns {number}
         */
        static random(minNum, maxNum) {
            return Math.random() * (maxNum - minNum);
        }
        /**
         * 随机获得int整数
         * @param minNum:最小范围(0开始)
         * @param maxNum:最大范围
         * @param stepLen:增加范围（整数，默认为1）
         * @return
         *
         */
        static randomInt(minNum, maxNum = 0, stepLen = 1) {
            if (minNum > maxNum) {
                var nTemp = minNum;
                minNum = maxNum;
                maxNum = nTemp;
            }
            var nDeltaRange = (maxNum - minNum) + (1 * stepLen);
            var nRandomNumber = Math.random() * nDeltaRange;
            nRandomNumber += minNum;
            return Math.floor(nRandomNumber / stepLen) * stepLen;
        }
        /**
         * 随机布尔值
         * @return
         *
         */
        static randomBoolean() {
            return this.randomInt(1, 2) == 1;
        }
        /**
         * 取得随机正负波动值(1 / -1)
         * @return
         *
         */
        static randomWave() {
            return this.randomBoolean() ? 1 : -1;
        }
        /**
         * 概率是否成功(100%  1- 100)
         * @param rate:最大值
         * @return 随机生成数是否小于或者等于rate
         */
        static isRateSucced(rate) {
            if (rate <= 0) {
                return false;
            }
            if (rate >= 100) {
                return true;
            }
            rate = rate / 100.0;
            var values = [];
            values[0] = Math.random();
            values[1] = Math.random();
            values[2] = Math.random();
            //随机选取0-2的下标
            if (rate >= values[this.randomInt(0, 2)]) {
                //随机数小于或者等于概率，证明出现在概率范围内
                return true;
            }
            return false;
        }
        static isRateSuccedInt(rate) {
            if (rate <= 0) {
                return false;
            }
            var values = [];
            values[0] = this.randomInt(1, 100);
            values[1] = this.randomInt(1, 100);
            values[2] = this.randomInt(1, 100);
            //随机选取0-2的下标
            if (rate >= values[this.randomInt(0, 2)]) {
                //随机数小于或者等于概率，证明出现在概率范围内
                return true;
            }
            return false;
        }
        /**
         * 判断值是否小于0-10000
         */
        static isRandTrue(rate) {
            var rand = this.randomInt(0, 10000);
            return rand < rate;
        }
    }
    asf.RandomUtils = RandomUtils;
})(asf);

(function (asf) {
    class SortUtil {
        constructor() {
        }
        /**
         *  对数组的进行NUMBER排序
         *  对象数组
         *  re:true升序 false降序
         *  isNew 是否创建新的数组
         */
        static sortBy(_arr1, re = false, isNew = true) {
            var arr = isNew ? _arr1.concat() : _arr1;
            arr.sort(sortFun);
            return arr;
            function sortFun(a, b) {
                if (a < b) {
                    if (re)
                        return -1;
                    return 1;
                }
                if (a == b)
                    return 0;
                if (re)
                    return 1;
                return -1;
            }
        }
        /**
         *  对数组的某个属性排序
         *  _arr1:对象数组
         *  p:属性名
         *  re:false降序 true升序
         *  isNew:是否创建新的数组
         */
        static sortBy2(_arr1, p, re = false, isNew = true) {
            this._p = p;
            this._re = re;
            var arr = isNew ? _arr1.concat() : _arr1;
            arr.sort(this.sortFun);
            return arr;
        }
        static sortFun(a, b) {
            if (a[SortUtil._p] < b[SortUtil._p]) {
                if (SortUtil._re)
                    return -1;
                return 1;
            }
            if (a[SortUtil._p] == b[SortUtil._p])
                return 0;
            if (SortUtil._re)
                return 1;
            return -1;
        }
        /**
         * 对数组的某些属性排序
         * @param _arr1 对象数组
         * @param p 属性名
         * @param re false 升序 true 降序
         * @param isNew 是否创建新的数组
         * @return
         *
         */
        static sortBy3(_arr1, p, re = false, isNew = true) {
            var arr = isNew ? _arr1.concat() : _arr1;
            arr.sort(sortFun);
            return arr;
            function sortFun(a, b) {
                var i = 0;
                while (true) {
                    if (a[p[i]] < b[p[i]]) {
                        if (re)
                            return 1;
                        return -1;
                    }
                    else if (a[p[i]] > b[p[i]]) {
                        if (re)
                            return -1;
                        return 1;
                    }
                    if (a[p[i]] == b[p[i]]) {
                        i++;
                        if (i >= p.length) {
                            break;
                        }
                    }
                }
                return 0;
            }
        }
        /**
         *  对数组的某个属性的属性排序
         *  对象数组
         *  属性名
         *  false 升序 true 降序
         *  isNew 是否创建新的数组
         */
        static sortBy4(_arr1, _arr2, reArr, isNew = true) {
            var arr = isNew ? _arr1.concat() : _arr1;
            if (!arr || arr.length == 0)
                return [];
            arr.sort(sortFun);
            return arr;
            function sortFun(a, b) {
                var i;
                var aa = a;
                var bb = b;
                var re = false;
                for (var j = 0; j < _arr2.length; j++) {
                    a = aa;
                    b = bb;
                    re = reArr[j];
                    for (i = 0; i < _arr2[j].length; i++) {
                        a = a[_arr2[j][i]];
                    }
                    for (i = 0; i < _arr2[j].length; i++) {
                        b = b[_arr2[j][i]];
                    }
                    if (a != b) {
                        continue;
                    }
                }
                if (a < b) {
                    if (re)
                        return -1;
                    return 1;
                }
                if (a == b)
                    return 0;
                if (re)
                    return 1;
                return -1;
            }
        }
        /**
         * 对数组的某些属性排序
         * @param _arr1 对象数组
         * @param p 属性名
         * @param reArr false 升序 true 降序
         * @param isNew 是否创建新的数组
         *
         * 例子:sortBy5(list,["type","id"],[fales,true],false);//对type升序排完再对id降序排
         * @return
         *
         */
        static sortBy5(_arr1, p, reArr = [], isNew = true) {
            var arr = isNew ? _arr1.concat() : _arr1;
            arr.sort(sortFun);
            return arr;
            function sortFun(a, b) {
                var i = 0;
                while (true) {
                    var re = reArr[i];
                    if (a[p[i]] < b[p[i]]) {
                        if (re)
                            return 1;
                        return -1;
                    }
                    else if (a[p[i]] > b[p[i]]) {
                        if (re)
                            return -1;
                        return 1;
                    }
                    if (a[p[i]] == b[p[i]]) {
                        i++;
                        if (i >= p.length) {
                            break;
                        }
                    }
                }
                return 0;
            }
        }
    }
    asf.SortUtil = SortUtil;
})(asf);
/**
 * @StringUtils.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-1-19
 */

/**
 * @StringUtils.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-1-19
 */
(function (asf) {
    /**
     * 字符串工具类
     * @author sodaChen
     * Date:2012-1-19
     */
    class StringUtils {
        /**
         * 字符串有内容
         * @param str
         * @returns {boolean}
         */
        static hasValue(str) {
            if (str && str != "")
                return true;
            return false;
        }
        /**
         * string str1 = "杰卫，这里有{0}个苹果，和{1}个香蕉！{0}个苹果{3} 元，{1}个香蕉{4} 元，一共{2}钱";
         * string str2 = "Hei jave, there are {0} apples，and {1} banana！ {2} dollar all together";
         * Console.WriteLine(string.Format(str1, 5, 10, 20, 7, 13));
         * Console.WriteLine(string.Format(str2, 5, 10, 20));
         * //输出：
         * 杰卫，这里有5个苹果，和10个香蕉！5个苹果7 元，10个香蕉13 元，一共20钱
         * Hei jave, there are 5 apples，and 10 banana！ 20 dollar all together
         * @param str
         * @param args
         * @return
         *
         */
        static formate(str, ...args) {
            for (var i = 0; i < args.length; i++) {
                str = str.replace(new RegExp("\\{" + i + "\\}", "gm"), args[i]);
            }
            return str;
        }
        static formateArys(str, args) {
            for (var i = 0; i < args.length; i++) {
                str = this.formateOne(i, str, args[i]);
            }
            return str;
        }
        static formateOne(index, str, param) {
            return str.replace(new RegExp("\\{" + index + "\\}", "gm"), param);
        }
        /**
         * <p>从字符串的末尾删除一个换行符，如果它在那里，
         *否则孤独。 换行符是“<code> \ n </ code>”
         *“<code> \ r </ code>”或“<code> \ r \ n </ code>”。</ p>
         *
         * <pre>
         * StringUtils.chomp(null)          = null
         * StringUtils.chomp("")            = ""
         * StringUtils.chomp("abc \r")      = "abc "
         * StringUtils.chomp("abc\n")       = "abc"
         * StringUtils.chomp("abc\r\n")     = "abc"
         * StringUtils.chomp("abc\r\n\r\n") = "abc\r\n"
         * StringUtils.chomp("abc\n\r")     = "abc\n"
         * StringUtils.chomp("abc\n\rabc")  = "abc\n\rabc"
         * StringUtils.chomp("\r")          = ""
         * StringUtils.chomp("\n")          = ""
         * StringUtils.chomp("\r\n")        = ""
         * </pre>
         *
         * @param str  the String to chomp a newline from, may be null
         * @return String without newline, <code>null</code> if null String input
         */
        static chomp(str) {
            return this.chompString(str, '(\r\n|\r|\n)');
        }
        /**
         * <p>从末尾除去<code>分隔符</ code>
         * <code> str </ code>如果它在那里，否则孤独。</ p>
         *：
         * <p>现在更接近匹配Perl chomp。
         *对于以前的行为，使用#substringBeforeLast（String，String）。
         *此方法使用#endsWith（String）。</ p>
         *
         * <pre>
         * StringUtils.chompString(null, *)         = null
         * StringUtils.chompString("", *)           = ""
         * StringUtils.chompString("foobar", "bar") = "foo"
         * StringUtils.chompString("foobar", "baz") = "foobar"
         * StringUtils.chompString("foo", "foo")    = ""
         * StringUtils.chompString("foo ", "foo")   = "foo "
         * StringUtils.chompString(" foo", "foo")   = " "
         * StringUtils.chompString("foo", "foooo")  = "foo"
         * StringUtils.chompString("foo", "")       = "foo"
         * StringUtils.chompString("foo", null)     = "foo"
         * </pre>
         *
         * @param str将字符串转换为chomp，可以为null
         * @param分隔符分隔符String，可以为null
         * @return String无尾部分隔符，<code> null </ code>如果为空字符串输入
         */
        static chompString(str, separator) {
            if (this.isEmpty(str) || separator == null) {
                return str;
            }
            return str.replace(new RegExp(separator + '$', ''), '');
        }
        /**
         * <p>从两者中删除控制字符（char <= 32）
         *这个String的结尾，通过返回处理<code> null </ code>
         * <code> null </ code>。</ p>
         *：
         * <p> Trim删除开始和结束字符＆lt; = 32。
         *要剥离空格，请使用#strip（String）。</ p>
         *：
         * <p>要修剪您选择的字符，请使用
         * #strip（String，String）methods。</ p>
         *
         * <pre>
         * StringUtils.trim(null)          = null
         * StringUtils.trim("")            = ""
         * StringUtils.trim("     ")       = ""
         * StringUtils.trim("abc")         = "abc"
         * StringUtils.trim("    abc    ") = "abc"
         * </pre>
         *
         * @param str要修剪的字符串，可以为null
         * @返回修剪后的字符串，<code> null </ code>如果为null String输入
         */
        static trim(str) {
            if (str == null) {
                return null;
            }
            return str.replace(/^\s*/, '').replace(/\s*$/, '');
        }
        /**
         * <p>从字符串中删除所有空格。</p>
         *
         * <pre>
         * StringUtils.deleteWhitespace(null)         = null
         * StringUtils.deleteWhitespace("")           = ""
         * StringUtils.deleteWhitespace("abc")        = "abc"
         * StringUtils.deleteWhitespace("   ab  c  ") = "abc"
         * </pre>
         *
         * @param str要删除空格的String，可以为null
         * @返回无空格的字符串，<code> null </ code>如果为空字符串输入
         */
        static deleteWhitespace(str) {
            return this.deleteFromString(str, /\s/g);
        }
        static deleteFromString(str, pattern) {
            if (this.isEmpty(str)) {
                return str;
            }
            return str.replace(pattern, '');
        }
        /**
         * <p>替换另一个字符串中所有出现的字符串。</ p>
         * ：
         * <p>传递给此方法的<code> null </ code>引用是无操作。</ p>
         *
         * <pre>
         * StringUtils.replace(null, *, *)        = null
         * StringUtils.replace("", *, *)          = ""
         * StringUtils.replace("any", null, *)    = "any"
         * StringUtils.replace("any", *, null)    = "any"
         * StringUtils.replace("any", "", *)      = "any"
         * StringUtils.replace("aba", "a", null)  = "aba"
         * StringUtils.replace("aba", "a", "")    = "b"
         * StringUtils.replace("aba", "a", "z")   = "zbz"
         * </pre>
         *
         * @param文本文本要搜索和替换，可以为null
         * @param pattern要搜索的字符串，可以为null
         * @param repl将String替换为，可以为null
         * @返回带有任何替换处理的文本，
         * <code> null </ code>如果为null String输入
         */
        static replace(text, pattern, repl) {
            if (text == null || this.isEmpty(pattern) || repl == null) {
                return text;
            }
            return text.replace(new RegExp(pattern, 'g'), repl);
        }
        /**
         * <p>返回在String中传递的值，或者返回String
         * empty或<code> null </ code>，<code> defaultStr </ code>的值。</ p>
         *
         * <pre>
         * StringUtils.defaultIfEmpty(null, "NULL")  = "NULL"
         * StringUtils.defaultIfEmpty("", "NULL")    = "NULL"
         * StringUtils.defaultIfEmpty("bat", "NULL") = "bat"
         * </pre>
         *
         * @param str要检查的String，可以为null
         * @param defaultStr返回的默认字符串.如果输入为空（“”）或<code> null </ code>，可以为null
         * @返回传入的String或默认值
         */
        static defaultIfEmpty(str, defaultStr) {
            return this.isEmpty(str) ? defaultStr : str;
        }
        /**
         * <p>检查字符串是否为空（""）或null</p>
         *
         * <pre>
         * StringUtils.isEmpty(null)      = true
         * StringUtils.isEmpty("")        = true
         * StringUtils.isEmpty(" ")       = false
         * StringUtils.isEmpty("bob")     = false
         * StringUtils.isEmpty("  bob  ") = false
         * </pre>
         *
         * <p>注：此方法在Lang 2.0版中更改。
         * 它不再修剪字符串。
         * 该功能在isBlank（）中可用。</ p>
         * ：
         * @param str要检查的String，可以为null
         * @return <code> true </ code>如果String为空或为null
         */
        static isEmpty(str) {
            if (!str) {
                return true;
            }
            return str.length == 0;
        }
        /**
         * <p>将字母改为首字母大写字母大写。
         * 没有其他字母更改。</ p>
         * ：
         * A <code> null </ code> input String返回<code> null </ code>。</ p>
         *
         * <pre>
         * StringUtils.capitalize(null)  = null
         * StringUtils.capitalize("")    = ""
         * StringUtils.capitalize("cat") = "Cat"
         * StringUtils.capitalize("cAt") = "CAt"
         * </pre>
         *
         * @param str将String转换为大写，可以为null
         * @返回大写字符串，<code> null </ code>如果为空字符串输入
         * @see titleize（String）
         * @see #uncapitalize（String）
         */
        static capitalize(str) {
            if (this.isEmpty(str)) {
                return str;
            }
            return str.charAt(0).toUpperCase() + str.substring(1);
        }
        /**
         * <p>取消资格将字符串更改为首字母到标题大小写。没有其他字母更改。</ p>
         *
         * <pre>
         * StringUtils.uncapitalize(null)  = null
         * StringUtils.uncapitalize("")    = ""
         * StringUtils.uncapitalize("Cat") = "cat"
         * StringUtils.uncapitalize("CAT") = "cAT"
         * </pre>
         *
         * @param str将String设置为uncapitalize，可以为null
         * @返回未资本化的字符串，<code> null </ code>如果为空字符串输入
         * @see #capitalize（String）
         */
        static uncapitalize(str) {
            if (this.isEmpty(str)) {
                return str;
            }
            return str.charAt(0).toLowerCase() + str.substring(1);
        }
        /**
         * 替换有颜色而替换对应的字符
         *
         * @static
         * @param {String} str
         * @param {Array} elementArr
         * @param {String} [symbol="&"]
         * @param {String} [color="#E2EAFB"]
         * @returns {String}
         *
         * @memberOf StringUtils
         */
        static replaceStrColorAndStr(str, elementArr, symbol = "&", color = null) {
            return StringUtils.replaceString(StringUtils.replaceStrColor(str, color), elementArr, symbol);
            //			return replaceStrColor(replaceString(str,elementArr,symbol));
        }
        /**
         * 替换字符
         * @param sourceStr 源字符串
         * @param elementArr 要替换的元素
         * @param symbol 分割符号
         * @return
         *
         */
        static replaceString(sourceStr, elementArr, symbol = "&") {
            var sourceArr = sourceStr.split(symbol);
            var str = "";
            for (var i = 0; i < sourceArr.length; i++) {
                if (elementArr && elementArr[i] != null) {
                    str += sourceArr[i] + elementArr[i];
                }
                else {
                    str += sourceArr[i];
                }
            }
            return str;
        }
        /**
         * 替代字符串的颜色，返回html
         * 		注意：使用回默认颜色的都要加回默认颜色值
         */
        static replaceStrColor(str, color = null, fontSize = null, isBold = false) {
            var txtColor = str.match(/#[0-9,a-f,A-F]{6}/g);
            var arr = [];
            var tmp = "";
            if (fontSize) {
                tmp = "'size='" + fontSize;
            }
            var clr;
            if (txtColor) {
                for (clr of txtColor) {
                    if (arr[clr])
                        continue;
                    arr[clr] = true;
                    str = StringUtils.replaceGanN(str);
                    if (isBold) {
                        str = str.split(clr).join("</b></font><font color='" + clr + tmp + "'><b>");
                    }
                    else {
                        str = str.split(clr).join("</font><font color='" + clr + tmp + "'>");
                    }
                }
            }
            str = str.split("#normal").join("</font><font color='#6992CD'>");
            str = str.split("#yellow").join("</font><font color='#F9E167'>");
            str = str.split("#white").join("</font><font color='#FFFFFF'>");
            str = str.split("#green").join("</font><font color='#00CB2B'>");
            str = str.split("#black").join("</font><font color='#000000'>");
            str = str.split("#blue").join("</font><font color='#6992CD'>");
            str = str.split("#purple").join("</font><font color='#C91CCD'>");
            str = str.split("#red").join("</font><font color='#E10000'>");
            str = str.split("#orange").join("</font><font color='#C46A00'>");
            str = str.replace(/\[#\]/g, "</font><font color='" + color + tmp + "'>");
            if (isBold) {
                if (color == null) {
                    return "<font><b>" + str + "<b></font>";
                }
                return "<font color='" + color + tmp + "'><b>" + str + "<b></font>";
            }
            if (color == null) {
                return str;
            }
            return "<font color='" + color + tmp + "'>" + str + "</font>";
        }
        /**
         * 匹配替换\\n(用于xml里的\n)
         * @param str
         *
         */
        static replaceGanN(str) {
            var newStr = str.replace(/\\n/g, "\n");
            return newStr;
        }
        /**获取带颜色的HTML字符串
         * str 字符串
         * color 颜色字符串 如f8d274
         */
        static getColorHtmlString(str, color) {
            return "<font color='#" + color + "'>" + str + "</font>";
        }
        /**获取带颜色的HTML字符串(不带#)
                * str 字符串
                * color 颜色字符串 如f8d274
                */
        static getColorHtmlString2(str, color) {
            return "<font color='" + color + "'>" + str + "</font>";
        }
        /**获取textLink字符串 */
        static getLinkHtmlString(str, event, needUnderline = true) {
            var underlineStr = needUnderline ? "<u>" : "";
            var underlineEndStr = needUnderline ? "</u>" : "";
            return "<a href='event:" + event + "'>" + underlineStr + str + underlineEndStr + "</a>";
        }
        /**数字转成带2位小数+1个中文字的格式
         * 可以是number或者string
         * 可以是带小数点的数值
         */
        static numberToShortString(num, showDot = true) {
            var result = String(num);
            var len;
            var dotLen;
            var dotIndex = result.indexOf(".");
            var dotStr = ""; //小数部分的结果
            var word = ""; //万/亿
            if (dotIndex != -1) { //有小数点情况下
                len = result.slice(0, dotIndex).length;
                dotLen = result.slice(dotIndex).length; //包括小数点 小数点以后的位数
                if (len > 8) {
                    dotStr = result.slice(-8 - dotLen, -6 - dotLen);
                    word = "亿";
                    result = result.slice(0, -8 - dotLen);
                }
                else if (len > 4) {
                    // dotStr = result.slice(-4 - dotLen, -2 - dotLen);
                    word = "万";
                    result = result.slice(0, -4 - dotLen);
                }
            }
            else {
                len = result.length;
                // dotLen = 0;
                if (len > 8) {
                    dotStr = result.slice(-8, -6);
                    word = "亿";
                    result = result.slice(0, -8);
                }
                else if (len > 4) {
                    // dotStr = result.slice(-4, -2);
                    word = "万";
                    result = result.slice(0, -4);
                }
            }
            if (dotStr.length > 0) {
                if (!showDot) {
                    dotStr = "00";
                }
                dotStr = (dotStr == "00" ? "" : "." + dotStr); //00就不显示小数后面2位了
            }
            return result + dotStr + word;
        }
        /**数字转汉字 */
        static numToChn(_num) {
            if (_num == 0)
                return "零";
            var chnNumChar = { "0": "零", "1": "一", "2": "二", "3": "三", "4": "四", "5": "五", "6": "六", "7": "七", "8": "八", "9": "九" };
            var chnNameValue = { "1": "十", "2": "百", "3": "千", "4": "万" };
            var resultStr = "";
            var numString = _num.toString();
            var nameIndex = numString.length;
            var len = numString.length;
            var lastZero = false;
            for (var i = 0; i < len; i++) {
                nameIndex--;
                var char = numString.charAt(i);
                //是否需要零
                if (char == "0" && _num % (Math.pow(10, nameIndex + 1)) == 0) {
                    continue;
                }
                //连着两个零
                if (char == "0" && lastZero)
                    continue;
                if (char == "0")
                    lastZero = true;
                resultStr += chnNumChar[char];
                //是否需要十
                if (char == "0" && nameIndex == 1)
                    continue;
                //是否需要百
                if (char == "0" && nameIndex == 2)
                    continue;
                if (chnNameValue[nameIndex]) {
                    resultStr += chnNameValue[nameIndex];
                }
            }
            //修复“一十” 至 “十”
            if (_num >= 10 && _num < 20) {
                resultStr = resultStr.slice(1);
            }
            return resultStr;
        }
    }
    asf.StringUtils = StringUtils;
})(asf);
// 时间戳工具类 liujianhao createBy 2017/5/24

// 时间戳工具类 liujianhao createBy 2017/5/24
(function (asf) {
    class TimeUtils {
        /**
         * 获取服务器时间
         */
        static getServerTime(nowTime) {
            return Math.floor(this.getServerTimeMill(nowTime) / 1000);
        }
        static getServerTimeMill(nowTime) {
            //DB.getInstance().serverTimeDB.getNowBeiJingTime().getTime()
            return this.ServerTime + (nowTime - this.ServerTimeStamp);
        }
        /**
         * 获取服务器时间的Ymd格式
         */
        static getServerTimeYmd(nowTime) {
            return Number(TimeUtils.dateFormat("yyyyMMdd", this.getServerTime(nowTime)));
        }
        /**
         * 获取服务器时间的Ymd格式
         */
        static getServerTimeYmd2(nowTime) {
            return this.dateFormat("yyyyMMdd-hh:mm:ss", this.getServerTime(nowTime));
        }
        //格式化日期
        static dateFormat(fmt, time) {
            var date = new Date(time * 1000);
            var o = {
                "M+": date.getMonth() + 1,
                "d+": date.getDate(),
                "h+": date.getHours(),
                "m+": date.getMinutes(),
                "s+": date.getSeconds(),
                "q+": Math.floor((date.getMonth() + 3) / 3),
                "S": date.getMilliseconds() //毫秒 
            };
            if (/(y+)/.test(fmt))
                fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
            for (var k in o)
                if (new RegExp("(" + k + ")").test(fmt))
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            return fmt;
        }
        /**
    * 将时间长度格式化
    *
    */
        static diffTimeFormat(fmt, time, type = 1) {
            var day = TimeUtils.number2int(time / 86400);
            var hour = TimeUtils.number2int(time % 86400 / 3600);
            var minutent = TimeUtils.number2int(time % 3600 / 60);
            var seconds = TimeUtils.number2int(time % 60);
            if (!new RegExp("(d+)").test(fmt)) {
                hour += day * 24;
            }
            if (!new RegExp("(h+)").test(fmt)) {
                minutent += hour * 60;
            }
            var o = {
                "d+": day,
                "h+": hour,
                "m+": minutent,
                "s+": seconds,
            };
            for (var k in o)
                if (new RegExp("(" + k + ")").test(fmt)) {
                    //                    debug((("00" + o[k]).substr(("" + o[k]).length)));
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : ("" + o[k]).length == 1 ? "0" + o[k] : o[k]);
                }
            return fmt;
        }
        /**
         * 返回 12:00:00这种格式
         * @param time
         * @returns {string}
         */
        static format1(time) {
            return TimeUtils.diffTimeFormat('hh:mm:ss', time);
        }
        static format2(time) {
            return TimeUtils.diffTimeFormat('hh时mm分ss秒', time);
        }
        static number2int(num) {
            return parseInt(TimeUtils.number2str(num));
        }
        static number2str(num) {
            return num;
        }
        /**
         * 用剩余时间显示 0:00:00格式
         * @param totalsec
         * @param showHour 是否显示小时
         * @param showAsChinese 是否显示成中文时分秒
         * @return
         */
        static getRemainTimeBySeconds(totalsec, showHour = true, showAsChinese = false) {
            var str = "";
            var hour = 0; //小时数
            var min = 0; //分钟数
            var sec = 0; //秒数
            var minR; //去小时数后剩余的秒数
            hour = Math.floor(totalsec / this.secondsPerHour);
            hour = hour < 0 ? 0 : hour;
            minR = Math.floor(totalsec % this.secondsPerHour);
            min = Math.floor(minR / 60);
            min = min < 0 ? 0 : min;
            sec = minR % 60;
            sec = sec < 0 ? 0 : sec;
            if (!showAsChinese && showHour) {
                str = this.formatString(hour) + ":" + this.formatString(min) + ":" + this.formatString(sec);
            }
            else if (!showAsChinese) {
                str = this.formatString(min) + ":" + this.formatString(sec);
            }
            else if (showAsChinese) {
                if (hour > 0) {
                    str += hour + "时";
                }
                if (min > 0) {
                    str += min + "分";
                }
                str += sec + "秒";
            }
            return str;
        }
        static formatString(value) {
            if (value < 0) {
                return "00";
            }
            var str;
            if (value <= 9) {
                str = "0" + value;
            }
            else {
                str = value.toString();
            }
            return str;
        }
        /** 获取当前月份的天数 */
        static getMonthLength(nowTime) {
            //nowTime = DB.getInstance().serverTimeDB.nowServerTime
            //构造当前日期对象
            var date = new Date();
            //把服务器的时间戳传进去
            date.setTime(nowTime);
            //获取年份
            var year = date.getFullYear();
            //获取当前月份
            var mouth = date.getMonth() + 1;
            //定义当月的天数；
            var days;
            //当月份为二月时，根据闰年还是非闰年判断天数
            if (mouth == 2) {
                days = year % 4 == 0 ? 29 : 28;
            }
            else if (mouth == 1 || mouth == 3 || mouth == 5 || mouth == 7 || mouth == 8 || mouth == 10 || mouth == 12) {
                //月份为：1,3,5,7,8,10,12 时，为大月.则天数为31；
                days = 31;
            }
            else {
                //其他月份，天数为：30.
                days = 30;
            }
            return days;
        }
        /**获取传入的时间戳的北京时间
         * time 时间戳(毫秒)
         * return Date
         */
        static getBeiJingTime(time) {
            var BeiJingTime = time + TimeUtils.BeiJingDate.getTimezoneOffset() * 60000 + 28800000; //修正为北京时间:time + TimeUtils.BeiJingDate.getTimezoneOffset() * 60 * 1000 + 8 * 3600 * 1000
            return new Date(BeiJingTime);
        }
        /**
         * 根据星期获取中文字符
         * sunDayIs0 是否周日是0
         */
        static getWeekName(day, sunDayIs0 = true) {
            if (sunDayIs0) {
                switch (day) {
                    case 0:
                        return "星期日";
                    case 1:
                        return "星期一";
                    case 2:
                        return "星期二";
                    case 3:
                        return "星期三";
                    case 4:
                        return "星期四";
                    case 5:
                        return "星期五";
                    case 6:
                        return "星期六";
                }
            }
            else {
                switch (day) {
                    case 0:
                        return "星期一";
                    case 1:
                        return "星期二";
                    case 2:
                        return "星期三";
                    case 3:
                        return "星期四";
                    case 4:
                        return "星期五";
                    case 5:
                        return "星期六";
                    case 6:
                        return "星期日";
                }
            }
            return "";
        }
    }
    TimeUtils.ServerTime = 0; //通过服务器协议返回的时间
    TimeUtils.ServerTimeStamp = 0; //时间标尺
    TimeUtils.BeiJingDate = new Date();
    TimeUtils.secondsPerDay = 60 * 60 * 24;
    TimeUtils.secondsPerHour = 60 * 60;
    asf.TimeUtils = TimeUtils;
})(asf);
/**
 * Created by sodaChen on 2017/3/14.
 */

/**
 * Created by sodaChen on 2017/3/14.
 */
(function (asf) {
    class TypeofUtils {
        static isNumber(value) {
            return typeof value === "number";
        }
        static isString(value) {
            return typeof value === "string";
        }
        static isArray(value) {
            var str = typeof value;
            return str === "array";
        }
        static isBoolean(value) {
            return typeof value === "boolean";
        }
    }
    asf.TypeofUtils = TypeofUtils;
})(asf);
